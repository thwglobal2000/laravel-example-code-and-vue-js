export * from './_common_/endpoints';
export * from './Admin/endpoints';
export * from './ServiceProvider/endpoints';
export * from './Customer/endpoints';
export * from './Participant/endpoints';
export * from './Representative/endpoints';
