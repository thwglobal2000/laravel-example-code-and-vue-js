import axios from 'axios';
import createAuthRefreshInterceptor from '~/includes/expired-session-refetch';

createAuthRefreshInterceptor(axios);

export const getServiceProviderRateList = (params, options) => axios.get(zRoute('rates.index', params), options);
export const storeServiceProviderRate = data => axios.post(zRoute('rates.store'), data);
export const updateServiceProviderRate = data => axios.put(zRoute('rates.update', {rate: data.id}), data);
export const destroyServiceProviderRate = id => axios.delete(zRoute('rates.destroy', {rate: id}));
export const getSpecializationsListForServiceProviderRates = () => axios.get(zRoute('rates.specializations-list'));
export const getCompaniesListForServiceProviderRates = () => axios.get(zRoute('rates.companies-list'));

export const getPersonnelList = (params, options) => axios.get(zRoute('personnel.index', params), options);
export const storePersonnel = data => axios.post(zRoute('personnel.store'), data);
export const updatePersonnel = data => axios.put(zRoute('personnel.update', {personnel: data.id}), data);
export const destroyPersonnel = id => axios.delete(zRoute('personnel.destroy', {personnel: id}));
export const getSpecializationListForPersonnel = () => axios.get(zRoute('personnel.specializations'));

export const confirmBookingRequest = ({code, hash}) => axios.get(zRoute('booking-requests.confirm', {booking: code, providerHash: hash}));
export const denyBookingRequest = ({code, hash}) => axios.get(zRoute('booking-requests.deny', {booking: code, providerHash: hash}));
export const getProviderBookingList = (params, options) => axios.get(zRoute('provider.booking.index', params), options);

export const getMyCompanies = () => axios.get(zRoute('companies.get-my-companies'));
