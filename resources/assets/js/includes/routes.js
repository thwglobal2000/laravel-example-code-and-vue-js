/* Common pages */
import Home from '~/views/Home.vue';
import NotFound from '~/views/_common_/404.vue';
import Login from '~/views/_common_/Login.vue';
import SignUp from '~/views/_common_/SignUp.vue';
import Reset from '~/views/_common_/Reset.vue';
import Response from '~/views/_common_/Response.vue';
import Dashboard from '~/views/_common_/Dashboard.vue';
import AccountSettings from '~/views/_common_/AccountSettings';
import VerifyUser from '~/views/_common_/VerifyUser';

/* Admin section */
import ContactImportModuleCsv from '~/views/Admin/setup/ContactImportModuleCsv.vue';
import Audits from '~/views/Admin/setup/Audits.vue';
import Users from '~/views/Admin/setup/Users.vue';
import Roles from '~/views/Admin/setup/Roles.vue';
import Contacts from '~/views/Admin/Contacts';
import Companies from '~/views/Admin/Companies';
import ServiceTypes from '~/views/Admin/ServiceTypes';
import Specializations from '~/views/Admin/Specializations';
import RateTypes from '~/views/Admin/RateTypes';
import SupportCategories from '~/views/Admin/SupportCategories';
import SupportNames from '~/views/Admin/SupportNames';
import PersonnelSpecializations from '~/views/Admin/PersonnelSpecializations';
import Plans from '~/views/Admin/Plans';

/* Service Provider section */
import Personnel from '~/views/ServiceProvider/Personnel';
import Confirm from '~/views/ServiceProvider/BookingRequest/Confirm';
import Deny from '~/views/ServiceProvider/BookingRequest/Deny';
import Rates from '~/views/ServiceProvider/Rates';
import Bookings from '~/views/ServiceProvider/Bookings/Bookings';

/* Defining routes */
import SubMenu from '~/components/_common_/SubMenu.vue';
import UIStyleGuide from '~/components/_common_/UIStyleGuide';

/* Representative section */
import Verification from '~/views/Representative/Verification';

let dashboard = () => import(/* webpackChunkName: "dashboard" */ '../views/_common_/Dashboard.vue');

let IfAuthRedirectToDashboard = (to, from, next) => {
    if (window.Vue && window.Vue.$auth.check()) {
        next({name: 'Dashboard'});
    } else {
        next();
    }
};

let IfRegistrationEnabled = (to, from, next) => {
    if (window.Vue && window.Vue.$auth.check()) {
        next({name: 'Dashboard'});
    } else if (!window.Laravel.appEnable.registration) {
        next({name: 'Login'});
    } else {
        next();
    }
};

let home = {
    name: 'Dashboard',
    title: 'Home',
};

const breadcrumb = (name, title, name2, title2) => {
    let bc = [home, {name: name, title: title ? title : name}];
    if (name2 || title2) bc.push({name: name2, title: title2 ? title2 : name2});

    return {breadcrumb: bc};
};

let routes = [
    {
        path: '',
        component: Home,
        meta: {auth: true},
        menu: true,
        children: [
            {path: '/', component: () => dashboard(), name: 'Dashboard', iconCls: 'el-icon-fa-th-large'},
            {path: '/account-settings', component: AccountSettings, name: 'Account Settings', hidden: true},
            {path: '/personnel-specializations', component: PersonnelSpecializations, name: 'Personnel Specializations', iconCls: 'el-icon-fa-user-md', meta: {menuName: 'Specializations', auth: ['personnel-specializations.index']}},
            {path: '/contacts', component: Contacts, name: 'Contacts', iconCls: 'el-icon-fa-id-card', meta: {auth: ['contacts.index']}},
            {path: '/companies', component: Companies, name: 'Companies', iconCls: 'el-icon-fa-trademark', meta: {auth: ['companies.index']}},
            {path: '/plans', component: Plans, name: 'Plans', iconCls: 'el-icon-fa-calendar-week', meta: {auth: ['plans.index']}},
            {path: '/rates', component: Rates, name: 'Rates', iconCls: 'el-icon-fa-dollar-sign', meta: {auth: ['rates.index']}},
            {path: '/personnel', component: Personnel, name: 'Personnel', iconCls: 'el-icon-fa-user-md', meta: {auth: ['personnel.index']}},
            {path: '/bookings', component: Bookings, name: 'Service Booking', iconCls: 'el-icon-fa-calendar-check', meta: {auth: ['provider.booking.index']}},
            {
                path: '/',
                component: SubMenu,
                name: 'Admin',
                iconCls: 'el-icon-fa-universal-access ',
                meta: {auth: ['roles.index', 'users.index', 'audits.index']},
                children: [
                    {path: '/roles', component: Roles, name: 'Roles', iconCls: 'el-icon-fa-suitcase', meta: {auth: ['roles.index'], ...breadcrumb('Roles', 'Roles & Permissions')}},
                    {path: '/users', component: Users, name: 'Users', iconCls: 'el-icon-fa-users', meta: {auth: ['users.index'], ...breadcrumb('Users')}},
                    {path: '/audits', component: Audits, name: 'Audits', iconCls: 'el-icon-fa-archive', meta: {auth: ['audits.index'], ...breadcrumb('Audits')}},
                    {path: '/import-contact-csv', component: ContactImportModuleCsv, name: 'Import сsv', iconCls: 'el-icon-fa-users',
                        meta: {auth: ['import.contact-csv'], ...breadcrumb('Import сsv')}
                    },
                ],
            },
            {
                path: '/',
                component: SubMenu,
                name: 'Pick Lists',
                iconCls: 'el-icon-fa-list-ol ',
                meta: {
                    auth: [
                        'rate-types.index',
                        'support-names.index',
                        'support-categories.index',
                        'service-types.index',
                        'specializations.index',
                    ],
                },
                children: [
                    {path: '/rate-types', component: RateTypes, name: 'Rate Types', iconCls: '', meta: {auth: ['rate-types.index']}},
                    {path: '/service-types', component: ServiceTypes, name: 'Service Types', iconCls: '', meta: {auth: ['service-types.index']}},
                    {path: '/specializations', component: Specializations, name: 'Specializations', iconCls: '', meta: {auth: ['specializations.index']}},
                    {path: '/support-categories', component: SupportCategories, name: 'Support Categories', iconCls: '', meta: {auth: ['support-categories.index']}},
                    {path: '/support-names', component: SupportNames, name: 'Support Names', iconCls: '', meta: {auth: ['support-names.index']}},
                ],
            },
        ],
    },
    {path: '/login', component: Login, name: 'Login', hidden: true, beforeEnter: IfAuthRedirectToDashboard},
    {path: '/sign-up', component: SignUp, name: 'Sign Up', hidden: true, beforeEnter: IfRegistrationEnabled},
    {path: '/password-reset', component: Reset, name: 'Reset', hidden: true, beforeEnter: IfAuthRedirectToDashboard},
    {path: '/response', component: Response, name: 'Response', hidden: true},
    {path: '/404', component: NotFound, hidden: true},
    {path: '*', component: NotFound, hidden: true},
    {path: '/ui-style-guide', component: UIStyleGuide, name: 'UI Style Guide', hidden: true, meta: {...breadcrumb('Style Guide')}},
    {path: '/verify/:hash', component: VerifyUser, name: 'Verify User', hidden: true},
    {path: '/verify/legal-representative/:hash', component: Verification, name: 'Representative Verification', hidden: true},
    {path: '/booking-request/:code/:hash/confirm', component: Confirm, name: 'Booking Request Confirm', hidden: true},
    {path: '/booking-request/:code/:hash/deny', component: Deny, name: 'Booking Request Deny', hidden: true},
];

export default routes;
