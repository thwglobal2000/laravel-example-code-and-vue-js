import axios from 'axios';
import createAuthRefreshInterceptor from '~/includes/expired-session-refetch';

createAuthRefreshInterceptor(axios);

export const importContactCsv             = params => axios.post     (zRoute('users.upload-contact-csv'), params);

export const getUserList                  = (p, o) => axios.get      (zRoute('users.index', p), o);
export const getUserCompanyList           = params => axios.get      (zRoute('users.companies', {user: params.id}));
export const getUser                      = params => axios.get      (zRoute('users.read', {user: params.id}));
export const getUserActiveClients         = (p, o) => axios.get      (zRoute('users.active-clients', p), o);
export const addUser                      = params => axios.post     (zRoute('users.store'), params);
export const removeUser                   = id     => axios.delete   (zRoute('users.delete', {user: id}));
export const deleteUserAvatar             = params => axios.delete   (zRoute('users.delete-avatar'), {params});
export const editUser                     = params => axios.post     (zRoute('users.update', {user: params.get('id')}), params);
export const exportUser                   = params => axios.get      (zRoute('users.export', params), {responseType: 'blob'});
export const importUser                   = params => axios.post     (zRoute('users.import'), params);
export const addUserActiveClient          = params => axios.post     (zRoute('users.add-active-clients'), params);
export const removeUserActiveClient       = params => axios.post     (zRoute('users.delete-active-clients'), params);
export const userImageUpload              = params => axios.post     (zRoute('users.image-upload'), params);

export const getRoleList                  = (p, o) => axios.get      (zRoute('roles.index', p), o);
export const getRole                      = params => axios.get      (zRoute('roles.read', {role: params.id}));
export const getRoleAutocomplete          = params => axios.get      (zRoute('roles.autocomplete', params));
export const addRole                      = params => axios.post     (zRoute('roles.create'), params);
export const removeRole                   = id     => axios.delete   (zRoute('roles.delete', {role: id}));
export const editRole                     = params => axios.put      (zRoute('roles.update', {role: params.id}), params);

export const getPermissionList            = params => axios.get      (zRoute('permissions.index', params));
export const getTagAutocomplete             = params => axios.get      (zRoute('tags.autocomplete', params));
export const getAuditList                 = (p, o) => axios.get      (zRoute('audits.index', p), o);

export const getServiceTypeList = (params, options) => axios.get(zRoute('service-types.index', params), options);
export const storeServiceType = data => axios.post(zRoute('service-types.store'), data);
export const updateServiceType = data => axios.put(zRoute('service-types.update', {serviceType: data.id}), data);
export const destroyServiceType = id => axios.delete(zRoute('service-types.destroy', {serviceType: id}));

export const getSpecializationList = (params, options) => axios.get(zRoute('specializations.index', params), options);
export const storeSpecialization = data => axios.post(zRoute('specializations.store'), data);
export const updateSpecialization = data => axios.put(zRoute('specializations.update', {specialization: data.id}), data);
export const destroySpecialization = id => axios.delete(zRoute('specializations.destroy', {specialization: id}));
export const getServiceTypeListForSpecializations = () => axios.get(zRoute('specializations.service-types-list'));

export const getRateTypeList = (params, options) => axios.get(zRoute('rate-types.index', params), options);
export const storeRateType = data => axios.post(zRoute('rate-types.store'), data);
export const updateRateType = data => axios.put(zRoute('rate-types.update', {rateType: data.id}), data);
export const destroyRateType = id => axios.delete(zRoute('rate-types.destroy', {rateType: id}));

export const getSupportCategoryList = (params, options) => axios.get(zRoute('support-categories.index', params), options);
export const storeSupportCategory = data => axios.post(zRoute('support-categories.store'), data);
export const updateSupportCategory = data => axios.put(zRoute('support-categories.update', {supportCategory: data.id}), data);
export const destroySupportCategory = id => axios.delete(zRoute('support-categories.destroy', {supportCategory: id}));

export const getSupportNameList = (params, options) => axios.get(zRoute('support-names.index', params), options);
export const storeSupportName = data => axios.post(zRoute('support-names.store'), data);
export const updateSupportName = data => axios.put(zRoute('support-names.update', {supportName: data.id}), data);
export const destroySupportName = id => axios.delete(zRoute('support-names.destroy', {supportName: id}));

export const getUserSpecializationList = (params, options) => axios.get(zRoute('personnel-specializations.index', params), options);
export const storeUserSpecialization = data => axios.post(zRoute('personnel-specializations.store'), data);
export const updateUserSpecialization = data => axios.put(zRoute('personnel-specializations.update', {specialization: data.id}), data);
export const destroyUserSpecialization = id => axios.delete(zRoute('personnel-specializations.destroy', {specialization: id}));
export const getAvailableSpecializationsList = () => axios.get(zRoute('personnel-specializations.specializations-list'));
export const getContactListForPersonnelSpecializations = () => axios.get(zRoute('personnel-specializations.contacts-list'));

export const getPlanList = (params, options) => axios.get(zRoute('plans.index', params), options);
export const storePlan = data => axios.post(zRoute('plans.store'), data);
export const updatePlan = data => axios.put(zRoute('plans.update', {plan: data.id}), data);
export const destroyPlan = id => axios.delete(zRoute('plans.destroy', {plan: id}));
export const getCompanyListForPlans = () => axios.get(zRoute('plans.companies-list'));
export const getParticipantListForPlans = () => axios.get(zRoute('plans.participants-list'));
