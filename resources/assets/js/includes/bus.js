import Echo from 'laravel-echo';
import push from './push';

// const messaging = firebase.messaging();

export default {
    data: {
        listenChannelName: '',
    },
    created: function() {
        this.$on('userLoggedIn', function(data) {
            setTimeout(() => {
                this.startWebSocketListeners(data);
            }, 300);
            if (push) push.requestPermission();
        });

        this.$on('userLoggedOff', function() {
            this.stopWebSocketListeners();
        });

        this.$on('notification.CommonUserNotification', function(data) {
            const $message = this.$message[data.status];
            if ($message) {
                $message(data.message);
            }
        });

        this.$on('notification.booking.new-request', function(data) {
            // noinspection JSUnresolvedVariable
            this.$notify({
                duration: 50000,
                title: 'New Booking Request',
                message: `Service: <b>${data.service}</b><br><a href="${data.confirmUrl}">Confirm</a> or <a href="${data.denyUrl}">Deny</a>`,
                type: 'info',
                dangerouslyUseHTMLString: true,
            });
        });

        this.$on('notification.booking.failed-request', function(data) {
            this.$notify({
                duration: 50000,
                title: 'Failed booking request',
                message: `Service <b>${data.service}</b> was already booked without your participation`,
                type: 'error',
                dangerouslyUseHTMLString: true,
            });
        });

        this.$on('notification.booking.request-timeout', function(data) {
            this.$notify({
                duration: 50000,
                title: 'Booking request timeout',
                message: `The waiting time for a response to the <b>${data.service}</b> request has expired`,
                type: 'warning',
                dangerouslyUseHTMLString: true,
            });
        });

        this.$on('notification.booking.confirmed', function(data) {
            this.$notify({
                duration: 50000,
                title: 'Booking confirmed',
                message: `Service: <b>${data.service}</b> booked successfully`,
                type: 'error',
                dangerouslyUseHTMLString: true,
            });
        });
    },

    methods: {
        authStateIsChanged: function(val) {
            if (!val) bus.$emit('userLoggedOff');
        },

        availableForUser: function(e) {
            // noinspection JSUnresolvedVariable
            return e.data.role_id === null || e.data.role_id === this.$auth.user().roles[0].id;
        },

        userChanged: function() {
            bus.$emit('userChanged');
        },

        startWebSocketListeners: function(user_data) {
            this.stopWebSocketListeners();
            try {
                // noinspection JSUnresolvedVariable
                window.Echo = window.io ? new Echo({
                    broadcaster: 'socket.io',
                    key: Laravel.pusher.key,
                    host: Laravel.pusher.scheme + '://' + window.location.hostname + ':' + Laravel.pusher.port,
                    auth: {
                        headers: {'Authorization': 'Bearer ' + Vue.$auth.token()},
                    },
                    encrypted: Laravel.pusher.encrypted,
                    // authEndpoint: '/api/broadcasting/auth',
                }) : null;
            }
            catch (err) {
                window.Echo = null;
            }

            if (window.Echo && user_data) {
                this.listenChannelName = 'communication-channel.' + user_data.id;
                // noinspection JSUnresolvedFunction,JSUnresolvedVariable
                window.Echo.private(this.listenChannelName && !!this.$auth.user().notification_status)
                    .listen('EventReminder', (e) => {
                        e && bus.$emit('notification.EventReminder', e);
                    })
                    .listen('MessageReceived', (e) => {
                        e && bus.$emit('notification.MessageReceived', e);
                    })
                    .listen('CommonUserNotification', (e) => {
                        e && bus.$emit('notification.CommonUserNotification', e);
                    })
                    .listen('SubscriptionNotification', (e) => {
                        e && bus.$emit('notification.SubscriptionNotification', e);
                    })
                    .listen('UserRelatedSettingIsUpdated', (e) => {
                        e && bus.$emit('notification.UserRelatedSettingIsUpdated', e);
                    })
                    .listen('AppointmentConfirmed', (e) => {
                        e && bus.$emit('notification.AppointmentConfirmed', e);
                    });

                // noinspection JSUnresolvedFunction
                window.Echo.private(`provider.${user_data.id}`)
                    .listen('booking.new-request', (e) => {
                        e && bus.$emit('notification.booking.new-request', e);
                    })
                    .listen('booking.failed-request', (e) => {
                        e && bus.$emit('notification.booking.failed-request', e);
                    });

                // noinspection JSUnresolvedFunction
                window.Echo.private(`participant.${user_data.id}`)
                    .listen('booking.confirmed', (e) => {
                        e && bus.$emit('notification.booking.confirmed', e);
                    });
            }
        },

        stopWebSocketListeners: function() {
            if (window.Echo) {
                // noinspection JSUnresolvedFunction
                window.Echo.leave(this.listenChannelName);
                // noinspection JSUnresolvedFunction
                window.Echo.disconnect();
                delete window.Echo;
            }
            window.Echo = null;
            this.listenChannelName = '';
        },
    },
};
