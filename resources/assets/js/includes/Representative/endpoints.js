import axios from 'axios';
import createAuthRefreshInterceptor from '~/includes/expired-session-refetch';

createAuthRefreshInterceptor(axios);

export const verifyLegalRepresentative = (code, data) => axios.post(zRoute('legal-representative.verify', {code: code}), data);
