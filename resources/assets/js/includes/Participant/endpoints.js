import axios from 'axios';
import createAuthRefreshInterceptor from '~/includes/expired-session-refetch';

createAuthRefreshInterceptor(axios);

export const storeParticipantContact  = data => axios.post(zRoute('participant.contact.store'), data);
export const getPlans                 = ()   => axios.get(zRoute('participant.subscription.plans'));
export const resyncSubscriptionStatus = id   => axios.get(zRoute('participant.subscription.resync-status', {id}));
export const subscriptionPayment      = data => axios.post(zRoute('participant.subscription.pay'), data);

export const storeBooking = data => axios.post(zRoute('booking.store'), data);

export const getServiceList = (params) => axios.get(zRoute('booking.services', params));

export const getUpcomingService = () => axios.get(zRoute('booking.upcoming'));

export const getCalendarEvents = (data) => axios.post(zRoute('widgets.calendar'), data);
