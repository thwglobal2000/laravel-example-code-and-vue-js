import axios from 'axios';
import createAuthRefreshInterceptor from '~/includes/expired-session-refetch';

createAuthRefreshInterceptor(axios);

export const currentUser = params => axios.get(zRoute('users.current'), params);

export const getBookingList = (params, options) => axios.get(zRoute('booking.index', params), options);

export const getContactList = (params, options) => axios.get(zRoute('contacts.index', params), options);
export const storeContact = data => axios.post(zRoute('contacts.store'), data);
export const updateContact = data => axios.put(zRoute('contacts.update', {contact: data.id}), data);
export const destroyContact = id => axios.delete(zRoute('contacts.destroy', {contact: id}));
export const getUserListForContacts = () => axios.get(zRoute('contacts.users-list'));
export const getCompanyListForContacts = () => axios.get(zRoute('contacts.companies-list'));

export const getCompanyList = (params, options) => axios.get(zRoute('companies.index', params), options);
export const storeCompany = data => axios.post(zRoute('companies.store'), data);
export const updateCompany = data => axios.put(zRoute('companies.update', {company: data.id}), data);
export const destroyCompany = id => axios.delete(zRoute('companies.destroy', {company: id}));
export const getUserListForCompanies = () => axios.get(zRoute('companies.users-list'));
export const getContactListForCompanies = () => axios.get(zRoute('companies.contacts-list'));
export const getContactListServiceProvider = () => axios.get(zRoute('companies.contacts-list-service-provider'));
export const getServiceTypeListForCompanies = () => axios.get(zRoute('companies.service-types-list'));
export const getPlanManagers = () => axios.get(zRoute('companies.plan-managers'));

export const updateUser = data => axios.post(zRoute('account-settings.update'), data);
export const changePassword = data => axios.put(zRoute('account-settings.update'), data);
export const userSpecializations = () => axios.get(zRoute('account-settings.specializations'));
export const updateUserSpecializations = data => axios.post(zRoute('account-settings.edit-specializations'), data);

export const verifyUser = hash => axios.post(zRoute('api.verify-user', {emailVerificationCode: hash}));

export const getDashboardWidgets            = params => axios.get      (zRoute('widgets.index', params));
export const updateLayout                   = params => axios.put      (zRoute('widgets.update-layout'), params);
export const setDashboardWidgetVisibility   = params => axios.put      (zRoute('widgets.visibility'), params);
export const setDashboardWidgetStatic       = params => axios.put      (zRoute('widgets.static'), params);
export const removeDashboardWidget          = params => axios.delete   (zRoute('widgets.delete', {i:params.i}));
