export const pagination = {
    data: () => ({
        pagination: {
            pageSize: 10,
            page: 1,
        },
    }),
    methods: {
        handlePageChange: _.debounce(function(value) {
            this.pagination.page = value;
            this.handlePaginationChanges();
        }, 500),
        handlePageSizeChange(value) {
            this.pagination.pageSize = value;
            this.handlePaginationChanges();
        },
    },
};
