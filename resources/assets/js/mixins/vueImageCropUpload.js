module.exports = {
    data() {
        return {
            cropShow: false,
            cropImageLoading: false,
            cropUploadUrl: zRoute('users.image-upload').url(),
            cropImgDataUrl: '',
            cropHeaders: {
                Authorization: 'Bearer ' + localStorage.default_auth_token,
                'X-Requested-With': 'XMLHttpRequest',
            },
        };
    },

    methods: {
        cropToggleShow() {
            this.cropShow = !this.cropShow;
        },
        cropMyUploadOnInput(a) {
            this.cropShow = a;
        },
        cropSuccess(imgDataUrl, field) {
            this.cropImgDataUrl = imgDataUrl;
        },
        cropUploadSuccess(jsonData, field) {
            this.$set(this.form, 'logo', jsonData);
            this.$set(this.form, 'avatar', jsonData);
        },
        cropUploadFail(status, field) {
        },
        cropOnSrcFileSet(fileName, fileType, fileSize) {
        },
        cropDeleteImage() {
            this.$confirm('This will permanently delete the image. Continue?', 'Warning', {
                type: 'warning',
            }).then(() => {
                this.cropImageLoading = true;
                this.removeCroppedImage();
            }).catch(() => {
            });
        },
    },
};