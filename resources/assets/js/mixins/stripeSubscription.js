import {resyncSubscriptionStatus, subscriptionPayment} from '~/includes/endpoints';
import scss from '@/element-ui-exports.module.scss';

export const stripeSubscription = {
    data() {
        return {
            loading: {
                payment: false,
            },
            refs: {
                elements: 'elements',
                card: 'card',
            },
            stripe: {
                key: Laravel.stripe_publish_key,
                instanceOptions: {
                    locale: 'au',
                },
                elementsOptions: {},
                cardOptions: {
                    value: {},
                    hidePostalCode: true,
                    classes: {
                        base: 'card-base',
                    },
                    style: {
                        base: {
                            lineHeight: scss.input_height,
                            '::placeholder': {
                                color: scss.text_secondary,
                            },
                            display: 'flex',
                            alignItems: 'center',
                        },
                    },
                },
            },
        };
    },
    methods: {
        makePayment(resolve, reject, planId) {

            this.loading.payment = true;

            const stripeInstance = this.$refs[this.refs.elements].instance;
            const cardElement = this.$refs[this.refs.card].stripeElement;

            stripeInstance
                .createPaymentMethod({
                    type: 'card',
                    card: cardElement,
                })
                .then(({error, paymentMethod}) => {
                    if (error) {
                        this.$errors.handle({response: {data: error}});

                        return reject();
                    }

                    subscriptionPayment({
                        plan_id: planId,
                        payment_method: paymentMethod.id,
                    }).then(({data}) => {
                        this.$message.success(data.message);
                        this.$emit('payment-complete');
                        resolve();
                    }).catch(e => {
                        const data = e.response.data;

                        if (!data?.client_secret || !data?.subscription_id) {
                            this.$errors.handle(e);

                            return reject();
                        }

                        stripeInstance.confirmCardPayment(data.client_secret, {
                            payment_method: paymentMethod.id,
                        }).then(({error, paymentIntent}) => {
                            if (error) {
                                this.$errors.handle({response: {data: error}});

                                return reject();
                            }

                            resyncSubscriptionStatus(data.subscription_id).then(({data}) => {
                                this.$message.success(data.message);
                                this.$emit('payment-complete');
                                resolve();
                            });
                        });
                    });
                })
                .catch(e => {
                    this.$errors.handle(e);
                    reject();
                })
                .finally(() => this.loading.payment = false);
        },
    },
};
