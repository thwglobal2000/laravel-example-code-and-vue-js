const link = '<a class="el-link el-link--primary is-underline"><span class="el-link--inner">here</span></a>';
const updateMessage = `New version is available! Click ${link} for start update.`;
const loadingMessage = 'The update process has started, wait for the page to refresh.';

export default {
    mounted() {
        bus.$on('sw-new-version', callback => {
            if (!callback) return;

            const notifyInstance = this.$notify.info({
                title: 'Good news!',
                dangerouslyUseHTMLString: true,
                message: updateMessage,
                duration: 0,
                onClick: () => {
                    notifyInstance.close();
                    callback();

                    this.$notify.info({
                        title: 'Updating...',
                        message: loadingMessage,
                        duration: 0,
                        showClose: false,
                    });
                },
            });
        });
    },
};