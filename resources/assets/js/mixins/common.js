module.exports = {
    data() {
        return {
            input_address: '',
            autocompleteOptions: {
                componentRestrictions: {
                    country: [
                        'au',
                    ],
                },
                language: 'en',
            },
            defaultCenterMap: {
                lat: -37.814,
                lng: 144.96332,
            },
            placesService: null,
        };
    },

    methods: {
        addressSearch(query, callback, mapRef = 'gMap') {
            this.initPlacesService(mapRef);
            if (!this.placesService) return;
            this.placesService.textSearch(
                {query},
                ((result, status) => {
                    if (status !== 'OK' || !result.length) return;
                    this.placesService.getDetails(
                        {placeId: result[0].place_id},
                        ((data, status) => {
                            if (status !== 'OK') return;
                            const params = this.getAddress(data);
                            if (params) {
                                callback(params);
                            }
                        }),
                    );
                }),
            );
        },
        initPlacesService(mapRef) {
            if (!this.placesService && this.$refs[mapRef] && window.google) {
                this.placesService = new window.google.maps.places.PlacesService(this.$refs[mapRef].$mapObject);
            }
        },
        handleCatch(error) {
            if (Laravel.appDebug) {
                console.log(error);
            }

            this.formLoading = false;

            if (error.response && error.response.data) {
                if (error.response.data.errors) {
                    return this.errors.record(error.response.data.errors);
                } else if (error.response.data.message) {
                    return this.$message.error(error.response.data.message);
                }
            }

            this.$message.error('Unknown server error');
        },
        changeAddress(val) {
            let regExp = new RegExp('[0-9]+.+[0-9]');
            let result = regExp.exec(val.target.value);
            if (result) {
                this.input_address = result[0];
            }
        },
        getAddress(data) {
            let params = {};
            if (data.address_components) {
                let _that = this;
                params = {
                    address: '',
                    street_number: '',
                    street_name: '',
                    suburb: '',
                    locality: '',
                    state: '',
                    postcode: '',
                    country: '',
                    lat: '',
                    lng: '',
                    centerMap: '',
                    place: JSON.stringify(data.address_components),
                };

                data.address_components.forEach(function(component) {
                    if (component.types.includes('subpremise')) {
                        params.address += component.short_name + '/';
                    } else if (component.types.includes('street_number')) {
                        params.address += component.short_name + ' ';
                    } else if (component.types.includes('route')) {
                        params.address += component.short_name + ', ';
                        params.street_name = component.short_name;
                    } else if (component.types.includes('locality')) {
                        params.address += component.short_name + ' ';
                        params.locality = params.suburb = component.short_name;
                    } else if (component.types.includes('administrative_area_level_1')) {
                        params.address += component.short_name.toUpperCase() + ' ';
                        params.state = component.short_name.toUpperCase();
                    } else if (component.types.includes('postal_code')) {
                        params.address += component.short_name + ', ';
                        params.postcode = component.short_name;
                    }

                    if (component.types.includes('country')) {
                        params.country = component.short_name;
                    }
                });

                if (data.geometry.location) {
                    params.lat = typeof data.geometry.location.lat === 'number' ? data.geometry.location.lat : data.geometry.location.lat();
                    params.lng = typeof data.geometry.location.lng === 'number' ? data.geometry.location.lng : data.geometry.location.lng();
                    params.centerMap = {lat: params.lat, lng: params.lng};
                }

                params.address += params.country;
                _that.input_address = '';
            } else if (data.name) {
                params['address'] = data.name;
                params.centerMap = this.defaultCenterMap;
            }

            return params;
        },
        resetAddress() {
            let params = {
                address: '',
                location: '',
                place: '',
                lat: '',
                lng: '',
            };
            this.mergeExistingFields(this.form, params);
        },
        cleanMap() {
            this.resetAddress();
            this.centerMap = {lat: -37.814, lng: 144.96332};
            this.formMap = true;
        },
    },
};