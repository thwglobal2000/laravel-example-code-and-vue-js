const CACHE_URLS = [
    '/',
    '/404',
    '/login',
    '/favicon.ico',
    '{{ mix("/js/compiled/manifest.js") }}',
    '{{ mix("/js/compiled/vendor.js") }}',
    '{{ mix("/js/compiled/app.js") }}',
    '{{ mix("/styles/compiled/sass_part.css") }}',
    '/images/avatar-placeholder.png',
    '/images/compiled/header-image.jpg',
    '/images/compiled/Logo_Light.png',
    '/images/compiled/Logo_Dark.png',
    '/fonts/vendor/font-awesome/fontawesome-webfont.woff2',
    '/fonts/vendor/font-awesome/fontawesome-webfont.woff',
    '/fonts/vendor/font-awesome/fontawesome-webfont.ttf',
    '/fonts/vendor/element-ui/lib/theme-chalk/element-icons.woff',
    '/fonts/vendor/element-ui/lib/theme-chalk/element-icons.ttf',
];

const hashFiles = CACHE_URLS.reduce((acc, url) => {
    const urlParts = url.split('?id=');

    if (urlParts && urlParts[1]) {
        acc += urlParts[1];
    }

    return acc;
}, '');

const CACHE_NAME = hashFiles || 'v1';

self.addEventListener('install', function(event) {
    event.waitUntil(
        caches.open(CACHE_NAME)
            .then(function(cache) {
                return cache.addAll(CACHE_URLS);
            })
    );
});

self.addEventListener('activate', event => {
    event.waitUntil(
        caches.keys()
            .then(keys => Promise.all(
                keys.map(key => {
                    if (CACHE_NAME !== key) {
                        return caches.delete(key);
                    }
                })
            ))
    );
});

self.addEventListener('activate', event => {
    event.waitUntil(
        caches.keys()
            .then(keys => Promise.all(
                keys.map(key => {
                    if (CACHE_NAME !== key) {
                        return caches.delete(key);
                    }
                })
            ))
    );
});

self.addEventListener('message', event => {
    if (event.data === 'SKIP_WAITING') skipWaiting();
});

self.addEventListener('fetch', function(event) {
    if (event.request.method === 'POST' || event.request.method === 'PUT') {
        return;
    }
    event.respondWith(
        caches.match(event.request)
            .then(function(response) {
                if (response) {
                    return response;
                }
                return fetch(event.request).catch(function(error) {
                    // `fetch()` throws an exception when the server is unreachable but not
                    // for valid HTTP responses, even `4xx` or `5xx` range.
                    let url = event.request.url.split('?', 2);
                    return caches.match(url[0])
                        .then(function(response) {
                            if (response) {
                                return response;
                            }
                            return caches.open(CACHE_NAME).then(function(cache) {
                                return cache.match('/login');
                            });
                        });
                });
            })
    );
});