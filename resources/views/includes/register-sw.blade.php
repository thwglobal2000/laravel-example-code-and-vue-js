<script>
    let registration;

    function skipWaiting() {
        if (registration && registration.waiting) {
            registration.waiting.postMessage('SKIP_WAITING');
        }
    }

    function emitNewVersion() {
        bus.$emit('sw-new-version', skipWaiting);
    }

    function reloadPage() {
        window.location.reload();
    }

    if (navigator && 'serviceWorker' in navigator) {
        window.addEventListener('load', function () {
            navigator.serviceWorker.register('/service-worker.js')
                .then(reg => {
                    registration = reg;

                    if (reg.waiting) {
                        emitNewVersion();
                    }

                    reg.addEventListener('updatefound', () => {
                        const newWorker = reg.installing;

                        newWorker.addEventListener('statechange', () => {
                            if (newWorker.state === 'installed') emitNewVersion();

                            if (newWorker.state === 'activated') reloadPage();
                        });
                    });
                })
                .catch(err => {
                    if (Laravel.appDebug) {
                        console.log('ServiceWorker registration failed: ', err);
                    }
                });

            navigator.serviceWorker.addEventListener('controllerchange', reloadPage);
        });
    }
</script>