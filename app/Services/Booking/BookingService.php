<?php

namespace App\Services\Booking;

use App\Events\Booking\NotifyParticipantAboutRequest;
use App\Events\Booking\NotifyProviderAboutFailedRequest;
use App\Events\Booking\NotifyProviderAboutRequest;
use App\Jobs\Booking\SendNotificationToProvidersBatch;
use App\Libraries\Utils\Utils;
use App\Mail\Booking\SendConfirmationToParticipant;
use App\Mail\Booking\SendFailedRequestToProvider;
use App\Mail\Booking\SendRequestNotificationToProvider;
use App\Mail\Booking\SendRequestTimeoutToProvider;
use App\Models\Booking\Booking;
use App\Models\BookingRequest\BookingRequest;
use App\Models\Company\Company;
use App\Models\Invoice\Invoice;
use App\Models\ServiceProviderRate\ServiceProviderRate;
use App\Models\ServiceType\ServiceType;
use App\Models\Specialization\Specialization;
use App\Models\User\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use LogicException;
use Throwable;

class BookingService implements BookingInterface
{
    public const MAX_PROVIDERS_FOR_NOTIFICATIONS = 9;
    public const PROVIDERS_IN_BATCH = 3;

    public const TIME_IN_MINUTES_FOR_CONFIRM_REQUEST = 10;

    protected User $participant;
    protected ServiceType $serviceType;
    protected Specialization $specialization;

    private array $bookingPayload;
    private array $frequencyActions = [
        Booking::FREQUENCY_DAILY => 'addDay',
        Booking::FREQUENCY_WEEKLY => 'addWeek',
        Booking::FREQUENCY_FORTNIGHTLY => 'addTwoWeeks',
    ];

    /**
     * BookingService constructor.
     */
    public function __construct()
    {
        //
    }

    /**
     * @param User $participant
     * @return $this
     * @throws Exception
     */
    public function setParticipant(User $participant): BookingService
    {
        if (!$participant->exists || !$participant->hasRole('participant')) {
            throw new Exception('Incorrect participant');
        }
        $this->participant = $participant;

        return $this;
    }

    /**
     * @param ServiceType $serviceType
     * @return $this
     * @throws Exception
     */
    public function setServiceType(ServiceType $serviceType): BookingService
    {
        if (!$serviceType->exists) {
            throw new Exception('Incorrect service type');
        }

        $this->serviceType = $serviceType;

        return $this;
    }

    public function setSpecialization(Specialization $specialization): BookingService
    {
        if (!$specialization->exists) {
            throw new Exception('Incorrect specialization');
        }

        $this->specialization = $specialization;

        return $this;
    }

    /**
     * @inheritDoc
     * @throws Throwable
     */
    public function sendRequest(array $bookingPayload): void
    {
        $this->bookingPayload = $bookingPayload;
        $providers = $this->getSuitableProviders();

        if ($providers->isNotEmpty()) {
            $code = Utils::uuid();

            $bookingPayload['start_date'] = Carbon::createFromFormat(config('date.carbonDate'), $bookingPayload['start_date']);
            $bookingPayload['end_date'] = Carbon::createFromFormat(config('date.carbonDate'), $bookingPayload['end_date']);

            $chunkedProviders = $providers->chunk(self::PROVIDERS_IN_BATCH);

            DB::beginTransaction();

            $booking = Booking::make($bookingPayload);
            $booking->participant_id = Utils::getCurrentUserId();
            $booking->status = Booking::STATUS_PENDING;
            $booking->saveOrFail();

            $request = BookingRequest::create([
                'service_type_id' => $this->serviceType->id,
                'participant_id' => $this->participant->id,
                'booking_id' => $booking->id,
                'code' => $code,
                'next_provider_batches' => $chunkedProviders,
                'current_providers_batch' => [],
                'booking_payload' => $bookingPayload,
            ]);

            SendNotificationToProvidersBatch::dispatch($request);

            DB::commit();
        } else {
            // TODO: Implement notification if providers not found
            throw new LogicException('Sorry, but suitable providers is not found 😟');
        }
    }

    /**
     * @inheritDoc
     */
    public function getSuitableProviders(): Collection
    {
        $types = [
            Company::COMPANY_TYPE_SOLE_TRADER,
            Company::COMPANY_MULTIPLE_SERVICE_TYPES,
        ];

        preg_match('/\b(?P<state>\w+) \d{4}(?:, \w{2})?$/mi', optional(Utils::getCurrentUser()->contact)->address, $state);
        $state = $state['state'] ?? null;

        return Company
            ::whereHas('contacts.specializations', function ($query) {
                $query->where(Specialization::TABLE_NAME. '.id', $this->specialization->id);
            })
            ->whereHas('visitTypes', fn($query) => $query->where('visit_type', $this->bookingPayload['visit_type']))
            ->when($this->bookingPayload['visit_type'] === Company::VISIT_TYPE_CLINIC, function ($query) {
                $query->byRadius($this->bookingPayload['lng'], $this->bookingPayload['lat'], $this->bookingPayload['radius']);
            })
            ->when($this->bookingPayload['visit_type'] === Company::VISIT_TYPE_HOMECARE, fn ($query) =>
                $query->where(fn (Builder $query): Builder => $query
                    ->where(Company::COLUMN_COVERAGE, Company::COVERAGE_NATIONAL)
                    ->orWhere(fn(Builder $query): Builder => $query
                        ->where(Company::COLUMN_COVERAGE, Company::COVERAGE_STATE)
                        ->where(Company::COLUMN_COVERAGE_STATE, $state))
                    ->orWhere(fn(Builder $query): Builder => $query
                        ->where(Company::COLUMN_COVERAGE, Company::COVERAGE_LIMITED)
                        ->whereColumn(
                            Company::COLUMN_COVERAGE_RADIUS,
                            '>',
                            DB::raw("ST_Distance_Sphere(point(lng, lat), point({$this->bookingPayload['lng']}, {$this->bookingPayload['lat']})) * .001")
                        ))))
            ->whereIn('company_type', $types)
            ->orderBy('business_name')
            ->limit(self::MAX_PROVIDERS_FOR_NOTIFICATIONS)
            ->get();
    }

    /**
     * @inheritDoc
     * @throws Throwable
     */
    public function confirmRequest(Company $provider, BookingRequest $request): void
    {
        $currentProvidersBatch = optional($request->current_providers_batch)->keyBy('id');
        if (!optional($currentProvidersBatch)[$provider->id]) {
            throw new Exception('Unable to confirm this booking');
        }

        $personnel = $provider
            ->contacts()
            ->whereHas('specializations', function (Builder $query) use ($request) {
                $query->where(Specialization::TABLE_NAME . '.id', $request->booking->specialization_id);
            })
            ->firstOr(function () {
                throw new Exception('Provider does not have personnel associated with this service type');
            });

        $booking = $request->booking;
        $booking->status = Booking::STATUS_CONFIRMED;
        $booking->serviceProvider()->associate($provider);
        $booking->personnel()->associate($personnel);
        $booking->saveOrFail();

        $this->createInvoice($booking);

        $this->cancelBookingRequest($request);
        $currentProvidersBatch
            ->forget($provider->id)
            ->each(function (array $item) use ($request): void {
                $company = Company::find($item['id']);
                $company && $this->notifyProvidersAboutFailedRequest($company, $request);
            });

        $this->setParticipant(User::findOrFail($booking->participant_id));
        $this->notifyParticipant($booking);
    }

    /**
     * @inheritDoc
     * @throws Exception
     */
    public function denyRequest(Company $provider, BookingRequest $request): void
    {
        $currentProvidersBatch = $request
            ->current_providers_batch
            ->reject(fn(array $company) => $company['id'] === $provider->id);

        $request->update([
            'current_providers_batch' => $currentProvidersBatch,
        ]);
    }

    /**
     * @inheritDoc
     */
    public function notifyProviderAboutRequest(Company $provider, BookingRequest $request): void
    {
        Mail::send(new SendRequestNotificationToProvider($provider, $request));
        $provider->admin->user && NotifyProviderAboutRequest::dispatch($provider, $request);
    }

    /**
     * @inheritDoc
     */
    public function notifyProvidersAboutFailedRequest(Company $provider, BookingRequest $request): void
    {
        Mail::send(new SendFailedRequestToProvider($provider, $request));
        $provider->admin->user && NotifyProviderAboutFailedRequest::dispatch($provider->admin->user, $request);
    }

    /**
     * @inheritDoc
     */
    public function notifyParticipant(Booking $booking): void
    {
        Mail::send(new SendConfirmationToParticipant($this->participant, $booking));
        NotifyParticipantAboutRequest::dispatch($this->participant, $booking);
    }

    /**
     * @inheritDoc
     */
    public function notifyProvidersAboutTimeoutRequest(Company $provider, BookingRequest $request): void
    {
        Mail::send(new SendRequestTimeoutToProvider($provider, $request));
        $provider->admin->user && NotifyProviderAboutFailedRequest::dispatch($provider->admin->user, $request);
    }

    private function cancelBookingRequest(BookingRequest $request): void
    {
        $request->update([
            'next_provider_batches' => [],
            'current_providers_batch' => [],
        ]);
    }

    public function getAppointmentsWithinDates(Collection $bookings, Carbon $startDate, Carbon $endDate): Collection
    {
        return $bookings
            ->map(function (Booking $booking) use ($startDate, $endDate) {
                $action = $this->frequencyActions[$booking->frequency];

                if (!$action) {
                    return [];
                }

                $bookingStart = Carbon::parse($booking->start_date)->startOfDay();
                $bookingEnd = Carbon::parse($booking->end_date)->startOfDay();
                $executions = [];

                while ($startDate->lessThanOrEqualTo($endDate)) {
                    if ($bookingEnd->lessThan($startDate)) {
                        break;
                    }

                    if ($startDate->lessThan($bookingStart)) {
                        $startDate = $bookingStart;
                        continue;
                    }

                    $executions[] = [
                        'booking_id' => $booking->id,
                        'title' => $booking->serviceType->service_name,
                        'start' => (clone $startDate)->setTimeFromTimeString($booking->start_time)->toIso8601String(),
                        'end' => (clone $startDate)->setTimeFromTimeString($booking->end_time)->toIso8601String(),
                    ];

                    $action === 'addTwoWeeks' ? $startDate->addWeeks(2) : $startDate->$action();
                }

                return $executions;
            })
            ->flatten(1);
    }

    private function createInvoice(Booking $booking): void
    {
        $rate = ServiceProviderRate
            ::where(ServiceProviderRate::COLUMN_COMPANY_ID, $booking->service_provider_id)
            ->where(ServiceProviderRate::COLUMN_SPECIALIZATION_ID, $booking->specialization_id)
            ->value(ServiceProviderRate::COLUMN_RATE_AMOUNT);

        $qty = $this->getAppointmentsWithinDates(
            collect([$booking]),
            now(),
            $booking->end_date->setTimeFromTimeString($booking->end_time)
        )->count();

        Invoice::create([
            'company_id' => $booking->service_provider_id,
            'booking_id' => $booking->id,
            'invoice_number' => Utils::encodeId($booking->id),
            'invoice_date' => now(),
            'qty' => $qty,
            'price' => $rate,
            'total_price' => $qty * $rate,
            'gst' => 0,
        ]);
    }
}
