<?php

namespace App\Services\Booking;

use App\Models\Booking\Booking;
use App\Models\BookingRequest\BookingRequest;
use App\Models\Company\Company;
use Illuminate\Support\Collection;

interface BookingInterface
{
    /**
     * Send request to providers
     *
     * @param array $bookingPayload
     * @return void
     */
    public function sendRequest(array $bookingPayload): void;

    /**
     * Prepare and sort Service Providers
     *
     * @return Collection|Company[]
     */
    public function getSuitableProviders(): Collection;

    /**
     * Notify Service Provider about new booking request
     *
     * @param Company $provider
     * @param BookingRequest $request
     *
     * @return void
     */
    public function notifyProviderAboutRequest(Company $provider, BookingRequest $request): void;

    /**
     * Confirm participant request
     *
     * @param Company $provider
     * @param BookingRequest $request
     */
    public function confirmRequest(Company $provider, BookingRequest $request): void;

    /**
     * Deny participant request
     *
     * @param Company $provider
     * @param BookingRequest $request
     */
    public function denyRequest(Company $provider, BookingRequest $request): void;

    /**
     * Notify other providers about failed request
     *
     * @param Company $provider
     * @param BookingRequest $request
     */
    public function notifyProvidersAboutFailedRequest(Company $provider, BookingRequest $request): void;

    /**
     * Notify other providers about request time out
     *
     * @param Company $provider
     * @param BookingRequest $request
     */
    public function notifyProvidersAboutTimeoutRequest(Company $provider, BookingRequest $request): void;

    /**
     * Notify participant about confirmation
     */
    public function notifyParticipant(Booking $booking): void;
}
