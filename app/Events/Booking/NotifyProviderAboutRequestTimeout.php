<?php

namespace App\Events\Booking;

use App\Models\BookingRequest\BookingRequest;
use App\Models\User\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class NotifyProviderAboutRequestTimeout implements ShouldBroadcast
{
    use Dispatchable;
    use InteractsWithSockets;
    use SerializesModels;

    /**
     * Company admin user for sending notification
     *
     * @var User
     */
    protected User $companyAdmin;

    /**
     * Booking request
     *
     * @var BookingRequest
     */
    protected BookingRequest $bookingRequest;

    /**
     * Create a new event instance.
     *
     * @param User $companyAdmin
     * @param BookingRequest $bookingRequest
     */
    public function __construct(User $companyAdmin, BookingRequest $bookingRequest)
    {
        $this->companyAdmin = $companyAdmin;
        $this->bookingRequest = $bookingRequest;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel
     */
    public function broadcastOn()
    {
        return new PrivateChannel("provider.{$this->companyAdmin->id}");
    }

    /**
     * The event's broadcast name.
     *
     * @return string
     */
    public function broadcastAs(): string
    {
        return 'booking.request-timeout';
    }

    /**
     * Get the data to broadcast.
     *
     * @return array
     */
    public function broadcastWith(): array
    {
        return [
            'service' => $this->bookingRequest->serviceType->service_name,
        ];
    }
}
