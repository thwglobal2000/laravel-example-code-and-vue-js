<?php

namespace App\Events\Booking;

use App\Models\Booking\Booking;
use App\Models\User\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class NotifyParticipantAboutRequest implements ShouldBroadcast
{
    use Dispatchable;
    use InteractsWithSockets;
    use SerializesModels;

    /**
     * Participant user
     *
     * @var User
     */
    protected User $participant;

    /**
     * Booking request
     *
     * @var Booking
     */
    protected Booking $booking;

    /**
     * Create a new event instance.
     *
     * @param User $participant
     * @param Booking $booking
     */
    public function __construct(User $participant, Booking $booking)
    {
        $this->participant = $participant;
        $this->booking = $booking;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|PrivateChannel
     */
    public function broadcastOn()
    {
        return new PrivateChannel("participant.{$this->participant->id}");
    }

    /**
     * The event's broadcast name.
     *
     * @return string
     * @noinspection PhpUnused
     */
    public function broadcastAs(): string
    {
        return 'booking.confirmed';
    }

    /**
     * Get the data to broadcast.
     *
     * @return array
     * @noinspection PhpUnused
     */
    public function broadcastWith(): array
    {
        return [
            'service' => $this->booking->serviceType->service_name,
        ];
    }
}
