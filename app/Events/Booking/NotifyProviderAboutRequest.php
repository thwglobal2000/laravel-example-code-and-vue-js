<?php

namespace App\Events\Booking;

use App\Libraries\Utils\Utils;
use App\Models\BookingRequest\BookingRequest;
use App\Models\Company\Company;
use App\Models\User\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class NotifyProviderAboutRequest implements ShouldBroadcast
{
    use Dispatchable;
    use InteractsWithSockets;
    use SerializesModels;

    /**
     * Company admin user for sending notification
     *
     * @var User
     */
    protected Company $company;

    /**
     * Booking request
     *
     * @var BookingRequest
     */
    protected BookingRequest $bookingRequest;

    /**
     * Create a new event instance.
     *
     * @param User $companyAdmin
     * @param BookingRequest $bookingRequest
     */
    public function __construct(Company $company, BookingRequest $bookingRequest)
    {
        $this->company = $company;
        $this->bookingRequest = $bookingRequest;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel
     */
    public function broadcastOn(): Channel
    {
        return new PrivateChannel("provider.{$this->company->admin->user->id}");
    }

    /**
     * The event's broadcast name.
     *
     * @return string
     * @noinspection PhpUnused
     */
    public function broadcastAs(): string
    {
        return 'booking.new-request';
    }

    /**
     * Get the data to broadcast.
     *
     * @return array
     * @noinspection PhpUnused
     */
    public function broadcastWith(): array
    {
        $providerHash = Utils::encodeId($this->company->id);

        return [
            'service' => $this->bookingRequest->serviceType->service_name,
            'confirmUrl' => $this->bookingRequest->getConfirmUrl($providerHash),
            'denyUrl' => $this->bookingRequest->getDenyUrl($providerHash),
        ];
    }
}
