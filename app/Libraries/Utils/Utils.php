<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 13/11/2017
 * Time: 1:28 AM
 */

namespace App\Libraries\Utils;

use App\Models\User\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use JWTAuth;

class Utils
{
    /**
     * @return int|null
     */
    public static function getCurrentUserId()
    {
        $curUser = self::getCurrentUser();

        return $curUser ? $curUser->id : null;
    }

    /**
     * @return User|null
     */
    public static function getCurrentUser()
    {
        $user = null;
        /* First of all try to auth though Token */
        try {
            $user = JWTAuth::parseToken()->authenticate();
        } catch (Exception $e) {
        }

        /* If Token user is not available try to get authorisation through AuthFactory */
        if (!$user) {
            $user = auth()->user();
        }

        return $user;
    }

    /**
     * Generate UUID
     *
     * @return string
     * @throws Exception
     */
    public static function uuid(): string
    {
        $uuid = [
            'time_low'      => 0,
            'time_mid'      => 0,
            'time_hi'       => 0,
            'clock_seq_hi'  => 0,
            'clock_seq_low' => 0,
            'node'          => []
        ];

        $uuid['time_low'] = random_int(0, 0xffff) + (random_int(0, 0xffff) << 16);
        $uuid['time_mid'] = random_int(0, 0xffff);
        $uuid['time_hi']  = (4 << 12) | (random_int(0, 0x1000));
        $uuid['clock_seq_hi']  = (1 << 7) | (random_int(0, 128));
        $uuid['clock_seq_low'] = random_int(0, 255);

        for ($i = 0; $i < 6; $i++) {
            $uuid['node'][$i] = random_int(0, 255);
        }

        return sprintf(
            '%08x-%04x-%04x-%02x%02x-%02x%02x%02x%02x%02x%02x',
            $uuid['time_low'],
            $uuid['time_mid'],
            $uuid['time_hi'],
            $uuid['clock_seq_hi'],
            $uuid['clock_seq_low'],
            $uuid['node'][0],
            $uuid['node'][1],
            $uuid['node'][2],
            $uuid['node'][3],
            $uuid['node'][4],
            $uuid['node'][5]
        );
    }

    /**
     * Function is little bit masking ID
     *
     * @param int|string $id
     *
     * @return string
     */
    public static function encodeId($id)
    {
        return bin2hex(base64_encode($id));
    }

    /**
     * Unmask ID number
     *
     * @param $masked_id
     *
     * @return null|string
     */
    public static function decodeId($masked_id)
    {
        $result = null;
        try {
            $base64 = hex2bin($masked_id);
            if ($base64) {
                $id = base64_decode($base64);
                if ($id) {
                    $result = $id;
                }
            }
        } catch (Exception $e) {
        }

        return $result;
    }

    /**
     * Convert time HH:ii:ss to seconds
     *
     * @param $time
     *
     * @return int
     */
    public static function timeToSeconds($time)
    {
        [$h, $m, $s] = explode(':', $time);

        return ($h * 3600) + ($m * 60) + $s;
    }

    /**
     * Convert seconds to time HH:ii:ss
     *
     * @param $seconds
     *
     * @return string
     */
    public static function secondsToTime($seconds)
    {
        $h = floor($seconds / 3600);
        $m = floor(($seconds % 3600) / 60);
        $s = $seconds - ($h * 3600) - ($m * 60);

        return sprintf('%02d:%02d:%02d', $h, $m, $s);
    }


    /**
     * @param Request $request
     *
     * @return Request
     */
    public static function updateRequestPhone(Request $request)
    {
        try {
            $phone = phone($request->phone, ['AUTO', 'AU'], 'E164');
            $request->merge(['phone' => $phone]);
        } catch (Exception $e) {
        }

        return $request;
    }

    public static function getHumanDateTime($date, $dateOnly = false)
    {
        $time  = Carbon::parse($date);
        $today = Carbon::today();

        if ($time->greaterThan($today) && $dateOnly) {
            return $time->format('g:i A');
        } else {
            return $time->format(' M j');
        }
    }

    public static function getWeekPeriod(int $week = 0)
    {
        $startDate = Carbon::now();
        $endDate = Carbon::now();

        if ($week) {
            if ($week > 0) {
                $startDate->addWeek($week);
                $endDate->addWeek($week);
            } elseif ($week < 0) {
                $startDate->subWeek(abs($week));
                $endDate->subWeek(abs($week));
            }
        }

        $monday = $startDate->startOfWeek();
        $sunday = $endDate->endOfWeek();

        return (object)['monday' => $monday, 'sunday' => $sunday];
    }

    /**
     * @param float $a
     * @param float $b
     *
     * @return int
     */
    public static function getPercentDifference(float $a, float $b): int
    {
        // strict comparison of the value with the float type behaves unusually,
        // to exclude unforeseen situations we bring the type to int
        if ((int)$a === 0) {
            return 100;
        }

        return (int)round((($b / $a) - 1) * 100);
    }

    /**
     * @param array $array
     *
     * @return array
     */
    public static function convertArrayToObject(array $array): array
    {
        return json_decode(json_encode($array));
    }
}
