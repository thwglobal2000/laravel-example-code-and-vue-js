<?php

namespace App\Models\Plan;

use App\Models\Company\Company;
use App\Models\User\User;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;

/**
 * App\Models\Plan\Plan
 *
 * @property int $id
 * @property int $company_id
 * @property int $participant_id
 * @property string $ndis_reference_number
 * @property bool $active
 * @property Carbon $expiration_date
 * @property string $manage_type
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Company $company
 * @property-read User $participant
 * @method static Builder|Plan newModelQuery()
 * @method static Builder|Plan newQuery()
 * @method static Builder|Plan query()
 * @method static Builder|Plan whereActive($value)
 * @method static Builder|Plan whereCompanyId($value)
 * @method static Builder|Plan whereCreatedAt($value)
 * @method static Builder|Plan whereExpirationDate($value)
 * @method static Builder|Plan whereId($value)
 * @method static Builder|Plan whereManageType($value)
 * @method static Builder|Plan whereNdisReferenceNumber($value)
 * @method static Builder|Plan whereParticipantId($value)
 * @method static Builder|Plan whereUpdatedAt($value)
 * @mixin Eloquent
 */
class Plan extends Model
{
    public const PLAN_MANAGED_TYPE = 'plan_managed';
    public const SELF_MANAGED_TYPE = 'self_managed';

    public const MANAGE_TYPES = [
        self::PLAN_MANAGED_TYPE => 'Plan managed',
        self::SELF_MANAGED_TYPE => 'Self managed',
    ];

    protected $casts = [
        'active' => 'bool',
    ];

    protected $dates = [
        'expiration_date',
    ];

    protected $fillable = [
        'company_id',
        'participant_id',
        'ndis_reference_number',
        'active',
        'expiration_date',
        'manage_type',
    ];

    public function participant(): BelongsTo
    {
        return $this->belongsTo(User::class, 'participant_id');
    }

    public function company(): BelongsTo
    {
        return $this->belongsTo(Company::class);
    }
}
