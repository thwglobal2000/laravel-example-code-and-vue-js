<?php
/** @noinspection PhpMissingFieldTypeInspection */

namespace App\Models\Specialization;

use App\Models\ServiceType\ServiceType;
use Database\Factories\Specialization\SpecializationFactory;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;

/**
 * App\Models\Specialization\Specialization
 *
 * @property int $id
 * @property int $service_type_id
 * @property string $name
 * @property string $code
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read ServiceType $serviceType
 * @method static SpecializationFactory factory(...$parameters)
 * @method static Builder|Specialization newModelQuery()
 * @method static Builder|Specialization newQuery()
 * @method static Builder|Specialization query()
 * @method static Builder|Specialization whereCode($value)
 * @method static Builder|Specialization whereCreatedAt($value)
 * @method static Builder|Specialization whereId($value)
 * @method static Builder|Specialization whereName($value)
 * @method static Builder|Specialization whereServiceTypeId($value)
 * @method static Builder|Specialization whereUpdatedAt($value)
 * @mixin Eloquent
 */
class Specialization extends Model
{
    use HasFactory;

    public const TABLE_NAME = 'specializations';

    protected $fillable = [
        'service_type_id',
        'name',
        'code',
    ];

    public function serviceType(): BelongsTo
    {
        return $this->belongsTo(ServiceType::class);
    }
}
