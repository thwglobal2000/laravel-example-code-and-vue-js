<?php

namespace App\Models\CompanyVisitType;

use Database\Factories\CompanyVisitType\CompanyVisitTypeFactory;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Models\CompanyVisitType\CompanyVisitType
 *
 * @property int $id
 * @property string $visit_type
 * @property int $company_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static CompanyVisitTypeFactory factory(...$parameters)
 * @method static Builder|CompanyVisitType newModelQuery()
 * @method static Builder|CompanyVisitType newQuery()
 * @method static Builder|CompanyVisitType query()
 * @method static Builder|CompanyVisitType whereCompanyId($value)
 * @method static Builder|CompanyVisitType whereCreatedAt($value)
 * @method static Builder|CompanyVisitType whereId($value)
 * @method static Builder|CompanyVisitType whereUpdatedAt($value)
 * @method static Builder|CompanyVisitType whereVisitType($value)
 * @mixin Eloquent
 */
class CompanyVisitType extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_id',
        'visit_type',
    ];
}
