<?php

namespace App\Models\BookingRequest;

use App\Models\Booking\Booking;
use App\Models\Company\Company;
use App\Models\ServiceType\ServiceType;
use App\Models\User\User;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

/**
 * App\Models\BookingRequest\BookingRequest
 *
 * @property int $id
 * @property int $booking_id
 * @property int $service_type_id
 * @property int $participant_id
 * @property string $code
 * @property Collection $next_provider_batches
 * @property Collection|null $current_providers_batch
 * @property array $booking_payload
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Booking $booking
 * @property-read User $participant
 * @property-read ServiceType $serviceType
 * @method static Builder|BookingRequest newModelQuery()
 * @method static Builder|BookingRequest newQuery()
 * @method static Builder|BookingRequest query()
 * @method static Builder|BookingRequest whereBookingId($value)
 * @method static Builder|BookingRequest whereBookingPayload($value)
 * @method static Builder|BookingRequest whereCode($value)
 * @method static Builder|BookingRequest whereCreatedAt($value)
 * @method static Builder|BookingRequest whereCurrentProvidersBatch($value)
 * @method static Builder|BookingRequest whereId($value)
 * @method static Builder|BookingRequest whereNextProviderBatches($value)
 * @method static Builder|BookingRequest whereParticipantId($value)
 * @method static Builder|BookingRequest whereServiceTypeId($value)
 * @method static Builder|BookingRequest whereUpdatedAt($value)
 * @mixin Eloquent
 */
class BookingRequest extends Model
{
    public const COLUMN_CURRENT_PROVIDER_BATCH = 'current_providers_batch';

    protected $casts = [
        'next_provider_batches' => 'collection',
        'current_providers_batch' => 'collection',
        'booking_payload' => 'array',
    ];

    protected $fillable = [
        'service_type_id',
        'participant_id',
        'code',
        'next_provider_batches',
        self::COLUMN_CURRENT_PROVIDER_BATCH,
        'booking_id',
        'booking_payload',
    ];

    public function serviceType(): BelongsTo
    {
        return $this->belongsTo(ServiceType::class);
    }

    public function participant(): BelongsTo
    {
        return $this->belongsTo(User::class, 'participant_id');
    }

    public function booking(): BelongsTo
    {
        return $this->belongsTo(Booking::class);
    }

    public function getConfirmUrl($providerHash): ?string
    {
        if (!$this->code) {
            return null;
        }

        return Str::replaceFirst('api/', '', route('booking-requests.confirm', [$this->code, $providerHash]));
    }

    /**
     * @return string
     */
    public function getDenyUrl($providerHash): ?string
    {
        if (!$this->code) {
            return null;
        }

        return Str::replaceFirst('api/', '', route('booking-requests.deny', [$this->code, $providerHash]));
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName(): string
    {
        return 'code';
    }
}
