<?php

namespace App\Models\PersonnelSpecialization;

use App\Models\Contact\Contact;
use App\Models\Specialization\Specialization;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\Pivot;
use Illuminate\Support\Carbon;

/**
 * App\Models\PersonnelSpecialization\PersonnelSpecialization
 *
 * @property int $id
 * @property int $personnel_id
 * @property int $specialization_id
 * @property string $ahpra_registration_number
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Contact $person
 * @property-read Specialization $specialization
 * @method static Builder|PersonnelSpecialization newModelQuery()
 * @method static Builder|PersonnelSpecialization newQuery()
 * @method static Builder|PersonnelSpecialization query()
 * @method static Builder|PersonnelSpecialization whereAhpraRegistrationNumber($value)
 * @method static Builder|PersonnelSpecialization whereCreatedAt($value)
 * @method static Builder|PersonnelSpecialization whereId($value)
 * @method static Builder|PersonnelSpecialization wherePersonnelId($value)
 * @method static Builder|PersonnelSpecialization whereSpecializationId($value)
 * @method static Builder|PersonnelSpecialization whereUpdatedAt($value)
 * @mixin Eloquent
 */
class PersonnelSpecialization extends Pivot
{
    protected $fillable = [
        'personnel_id',
        'specialization_id',
        'ahpra_registration_number',
    ];

    public function person(): BelongsTo
    {
        return $this->belongsTo(Contact::class, 'personnel_id');
    }

    public function specialization(): BelongsTo
    {
        return $this->belongsTo(Specialization::class);
    }
}
