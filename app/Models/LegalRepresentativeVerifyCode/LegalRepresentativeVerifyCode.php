<?php
/** @noinspection PhpMissingFieldTypeInspection */

namespace App\Models\LegalRepresentativeVerifyCode;

use App\Models\User\User;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;

/**
 * App\Models\LegalRepresentativeVerifyCode\LegalRepresentativeVerifyCode
 *
 * @property int $id
 * @property int $representative_id
 * @property string $code
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read User $legalRepresentative
 * @method static Builder|LegalRepresentativeVerifyCode newModelQuery()
 * @method static Builder|LegalRepresentativeVerifyCode newQuery()
 * @method static Builder|LegalRepresentativeVerifyCode query()
 * @method static Builder|LegalRepresentativeVerifyCode whereCode($value)
 * @method static Builder|LegalRepresentativeVerifyCode whereCreatedAt($value)
 * @method static Builder|LegalRepresentativeVerifyCode whereId($value)
 * @method static Builder|LegalRepresentativeVerifyCode whereRepresentativeId($value)
 * @method static Builder|LegalRepresentativeVerifyCode whereUpdatedAt($value)
 * @mixin Eloquent
 */
class LegalRepresentativeVerifyCode extends Model
{
    protected $fillable = [
        'representative_id',
        'code',
    ];

    public function legalRepresentative(): BelongsTo
    {
        return $this->belongsTo(User::class, 'representative_id');
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName(): string
    {
        return 'code';
    }
}
