<?php

namespace App\Models\SupportName;

use App\Models\RateMatrix\RateMatrix;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;

/**
 * App\Models\SupportName\SupportName
 *
 * @property int $id
 * @property string $name
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Collection|RateMatrix[] $rateMatrices
 * @property-read int|null $rate_matrices_count
 * @method static Builder|SupportName newModelQuery()
 * @method static Builder|SupportName newQuery()
 * @method static Builder|SupportName query()
 * @method static Builder|SupportName whereCreatedAt($value)
 * @method static Builder|SupportName whereId($value)
 * @method static Builder|SupportName whereName($value)
 * @method static Builder|SupportName whereUpdatedAt($value)
 * @mixin Eloquent
 */
class SupportName extends Model
{
    protected $fillable = [
        'name',
    ];

    public function rateMatrices(): HasMany
    {
        return $this->hasMany(RateMatrix::class);
    }
}
