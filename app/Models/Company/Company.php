<?php
/** @noinspection PhpMissingFieldTypeInspection */

namespace App\Models\Company;

use App\Models\Booking\Booking;
use App\Models\CompanyVisitType\CompanyVisitType;
use App\Models\Contact\Contact;
use App\Models\Invoice\Invoice;
use App\Models\Plan\Plan;
use App\Models\ServiceProviderRate\ServiceProviderRate;
use App\Models\ServiceType\ServiceType;
use App\Models\Specialization\Specialization;
use App\Traits\HasManySyncTrait;
use Database\Factories\Company\CompanyFactory;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;
use OwenIt\Auditing\Contracts\Auditable;
use OwenIt\Auditing\Models\Audit;
use Spatie\Tags\HasTags;
use Spatie\Tags\Tag;
use Staudenmeir\EloquentHasManyDeep\HasManyDeep;
use Staudenmeir\EloquentHasManyDeep\HasRelationships;

/**
 * App\Models\Company\Company
 *
 * @property int $id
 * @property int|null $admin_id
 * @property string $business_name
 * @property string|null $url
 * @property string $mobile_phone_number
 * @property string|null $phone_number
 * @property string $email
 * @property string $contact_name
 * @property string|null $address
 * @property string|null $lat
 * @property string|null $lng
 * @property string|null $postcode
 * @property string|null $country
 * @property string|null $company_type
 * @property string $coverage
 * @property string|null $coverage_state
 * @property int|null $coverage_radius
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property Carbon|null $deleted_at
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $deleted_by
 * @property-read Contact|null $admin
 * @property-read Collection|Audit[] $audits
 * @property-read int|null $audits_count
 * @property-read Collection|Booking[] $bookings
 * @property-read int|null $bookings_count
 * @property-read Collection|Contact[] $contacts
 * @property-read int|null $contacts_count
 * @property-read Collection|Invoice[] $invoices
 * @property-read int|null $invoices_count
 * @property-read Collection|Plan[] $plans
 * @property-read int|null $plans_count
 * @property-read Collection|ServiceType[] $serviceTypes
 * @property-read int|null $service_types_count
 * @property Collection|Tag[] $tags
 * @property-read Collection|Specialization[] $specializations
 * @property-read int|null $specializations_count
 * @property-read int|null $tags_count
 * @property-read Collection|CompanyVisitType[] $visitTypes
 * @property-read int|null $visit_types_count
 * @method static Builder|Company byRadius(float $lng, float $lat, int $radius)
 * @method static CompanyFactory factory(...$parameters)
 * @method static Builder|Company newModelQuery()
 * @method static Builder|Company newQuery()
 * @method static \Illuminate\Database\Query\Builder|Company onlyTrashed()
 * @method static Builder|Company query()
 * @method static Builder|Company whereAddress($value)
 * @method static Builder|Company whereAdminId($value)
 * @method static Builder|Company whereBusinessName($value)
 * @method static Builder|Company whereCompanyType($value)
 * @method static Builder|Company whereContactName($value)
 * @method static Builder|Company whereCountry($value)
 * @method static Builder|Company whereCoverage($value)
 * @method static Builder|Company whereCoverageRadius($value)
 * @method static Builder|Company whereCoverageState($value)
 * @method static Builder|Company whereCreatedAt($value)
 * @method static Builder|Company whereCreatedBy($value)
 * @method static Builder|Company whereDeletedAt($value)
 * @method static Builder|Company whereDeletedBy($value)
 * @method static Builder|Company whereEmail($value)
 * @method static Builder|Company whereId($value)
 * @method static Builder|Company whereLat($value)
 * @method static Builder|Company whereLng($value)
 * @method static Builder|Company whereMobilePhoneNumber($value)
 * @method static Builder|Company wherePhoneNumber($value)
 * @method static Builder|Company wherePostcode($value)
 * @method static Builder|Company whereUpdatedAt($value)
 * @method static Builder|Company whereUpdatedBy($value)
 * @method static Builder|Company whereUrl($value)
 * @method static Builder|Company withAllTags($tags, ?string $type = null)
 * @method static Builder|Company withAllTagsOfAnyType($tags)
 * @method static Builder|Company withAnyTags($tags, ?string $type = null)
 * @method static Builder|Company withAnyTagsOfAnyType($tags)
 * @method static \Illuminate\Database\Query\Builder|Company withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Company withoutTrashed()
 * @mixin Eloquent
 */
class Company extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    use HasTags;
    use HasRelationships;
    use SoftDeletes;
    use HasManySyncTrait;
    use HasFactory;

    public const COMPANY_TYPE_SOLE_TRADER = 'sole_trader';
    public const COMPANY_MULTIPLE_SERVICE_TYPES = 'multiple_service_types';
    public const COMPANY_PLAN_MANAGER = 'plan_manager';

    public const COMPANY_TYPES = [
        self::COMPANY_TYPE_SOLE_TRADER => 'Sole Trader',
        self::COMPANY_MULTIPLE_SERVICE_TYPES => 'Multiple Service Types',
        self::COMPANY_PLAN_MANAGER => 'Plan Manager',
    ];

    public const VISIT_TYPE_CLINIC = 'clinic';
    public const VISIT_TYPE_HOMECARE = 'homecore';
    public const VISIT_TYPE_TELEHEALTH = 'telehealth';
    public const VISIT_TYPE_VIDEO = 'video';

    public const VISIT_TYPES = [
        self::VISIT_TYPE_CLINIC => 'Clinic',
        self::VISIT_TYPE_HOMECARE => 'Home Care',
        self::VISIT_TYPE_TELEHEALTH => 'TeleHealth',
        self::VISIT_TYPE_VIDEO => 'Video Conferencing',
    ];

    public const COVERAGE_NATIONAL = 'national';
    public const COVERAGE_STATE = 'state';
    public const COVERAGE_LIMITED = 'limited';
    public const COVERAGES = [
        self::COVERAGE_NATIONAL => 'National',
        self::COVERAGE_STATE => 'State',
        self::COVERAGE_LIMITED => 'Limited'
    ];

    public const COLUMN_ADMIN_ID = 'admin_id';
    public const COLUMN_COVERAGE = 'coverage';
    public const COLUMN_COVERAGE_RADIUS = 'coverage_radius';
    public const COLUMN_COVERAGE_STATE = 'coverage_state';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::COLUMN_ADMIN_ID,
        'business_name',
        'url',
        'mobile_phone_number',
        'phone_number',
        'email',
        'contact_name',
        'address',
        'lat',
        'lng',
        'postcode',
        'country',
        'company_type',
        self::COLUMN_COVERAGE,
        self::COLUMN_COVERAGE_RADIUS,
        self::COLUMN_COVERAGE_STATE,
    ];

    /**
     * @param Builder $query
     * @param float $lng
     * @param float $lat
     * @param int $radius
     * @return Builder
     */
    public function scopeByRadius(Builder $query, float $lng, float $lat, int $radius): Builder
    {
        return $query
            ->whereRaw('ST_Distance_Sphere(point(lng, lat), point(?, ?)) * .001 < ?', [
                $lng,
                $lat,
                $radius,
            ]);
    }

    public function admin(): BelongsTo
    {
        return $this->belongsTo(Contact::class, 'admin_id');
    }

    public function plans(): HasMany
    {
        return $this->hasMany(Plan::class);
    }

    public function invoices(): HasMany
    {
        return $this->hasMany(Invoice::class);
    }

    public function bookings(): HasMany
    {
        return $this->hasMany(Booking::class, 'service_provider_id');
    }

    public function users(): HasManyDeep
    {
        return $this->hasManyDeepFromRelations($this->contacts(), (new Contact())->user());
    }

    public function contacts(): BelongsToMany
    {
        return $this->belongsToMany(Contact::class);
    }

    public function serviceTypes(): BelongsToMany
    {
        return $this->belongsToMany(ServiceType::class, 'service_provider_service_type', 'service_provider_id');
    }

    public function visitTypes(): HasMany
    {
        return $this->hasMany(CompanyVisitType::class);
    }

    public function specializations(): belongsToMany
    {
        return $this->belongsToMany(
            Specialization::class,
            ServiceProviderRate::TABLE_NAME,
            ServiceProviderRate::COLUMN_COMPANY_ID,
            ServiceProviderRate::COLUMN_SPECIALIZATION_ID
        )->withPivot([ServiceProviderRate::COLUMN_RATE_AMOUNT]);
    }
}
