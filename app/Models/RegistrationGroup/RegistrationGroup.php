<?php

namespace App\Models\RegistrationGroup;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Models\RegistrationGroup\RegistrationGroup
 *
 * @property int $id
 * @property string $name
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|RegistrationGroup newModelQuery()
 * @method static Builder|RegistrationGroup newQuery()
 * @method static Builder|RegistrationGroup query()
 * @method static Builder|RegistrationGroup whereCreatedAt($value)
 * @method static Builder|RegistrationGroup whereId($value)
 * @method static Builder|RegistrationGroup whereName($value)
 * @method static Builder|RegistrationGroup whereUpdatedAt($value)
 * @mixin Eloquent
 */
class RegistrationGroup extends Model
{
    //
}
