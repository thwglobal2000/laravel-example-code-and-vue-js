<?php

namespace App\Models\ServiceProviderServiceType;

use App\Models\Company\Company;
use App\Models\ServiceProviderRate\ServiceProviderRate;
use App\Models\ServiceType\ServiceType;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\Pivot;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;

/**
 * App\Models\ServiceProviderServiceType\ServiceProviderServiceType
 *
 * @property int $id
 * @property int $service_provider_id
 * @property int $service_type_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property Carbon|null $deleted_at
 * @property int|null $created_by
 * @property int|null $deleted_by
 * @property-read Collection|ServiceProviderRate[] $rates
 * @property-read int|null $rates_count
 * @property-read Company $serviceProvider
 * @property-read ServiceType $serviceType
 * @method static Builder|ServiceProviderServiceType newModelQuery()
 * @method static Builder|ServiceProviderServiceType newQuery()
 * @method static \Illuminate\Database\Query\Builder|ServiceProviderServiceType onlyTrashed()
 * @method static Builder|ServiceProviderServiceType query()
 * @method static Builder|ServiceProviderServiceType whereCreatedAt($value)
 * @method static Builder|ServiceProviderServiceType whereCreatedBy($value)
 * @method static Builder|ServiceProviderServiceType whereDeletedAt($value)
 * @method static Builder|ServiceProviderServiceType whereDeletedBy($value)
 * @method static Builder|ServiceProviderServiceType whereId($value)
 * @method static Builder|ServiceProviderServiceType whereServiceProviderId($value)
 * @method static Builder|ServiceProviderServiceType whereServiceTypeId($value)
 * @method static Builder|ServiceProviderServiceType whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|ServiceProviderServiceType withTrashed()
 * @method static \Illuminate\Database\Query\Builder|ServiceProviderServiceType withoutTrashed()
 * @mixin Eloquent
 */
class ServiceProviderServiceType extends Pivot
{
    use SoftDeletes;

    public function serviceProvider(): BelongsTo
    {
        return $this->belongsTo(Company::class, 'service_provider_id');
    }

    public function serviceType(): BelongsTo
    {
        return $this->belongsTo(ServiceType::class);
    }

    public function rates(): HasMany
    {
        return $this->hasMany(ServiceProviderRate::class, 'service_provider_service_type_id');
    }
}
