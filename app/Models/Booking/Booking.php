<?php

namespace App\Models\Booking;

use App\Libraries\Utils\Utils;
use App\Models\BookingRequest\BookingRequest;
use App\Models\Company\Company;
use App\Models\Contact\Contact;
use App\Models\Invoice\Invoice;
use App\Models\Role\Role;
use App\Models\ServiceType\ServiceType;
use App\Models\Specialization\Specialization;
use App\Models\User\User;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Carbon;

/**
 * App\Models\Booking\Booking
 *
 * @property int $id
 * @property int|null $participant_id
 * @property int|null $service_provider_id
 * @property int|null $personnel_id
 * @property int|null $service_type_id
 * @property int $specialization_id
 * @property string $status
 * @property Carbon $start_date
 * @property Carbon $end_date
 * @property string $start_time
 * @property string $end_time
 * @property string|null $frequency
 * @property string|null $visit_type
 * @property string|null $address
 * @property string|null $lat
 * @property string|null $lng
 * @property int|null $radius
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read BookingRequest|null $bookingRequest
 * @property-read Invoice|null $invoice
 * @property-read User|null $participant
 * @property-read Contact|null $personnel
 * @property-read Company|null $serviceProvider
 * @property-read ServiceType|null $serviceType
 * @property-read Specialization $specialization
 * @method static Builder|Booking confirmed()
 * @method static Builder|Booking newModelQuery()
 * @method static Builder|Booking newQuery()
 * @method static Builder|Booking query()
 * @method static Builder|Booking whereAddress($value)
 * @method static Builder|Booking whereCreatedAt($value)
 * @method static Builder|Booking whereEndDate($value)
 * @method static Builder|Booking whereEndTime($value)
 * @method static Builder|Booking whereFrequency($value)
 * @method static Builder|Booking whereId($value)
 * @method static Builder|Booking whereLat($value)
 * @method static Builder|Booking whereLng($value)
 * @method static Builder|Booking whereParticipantId($value)
 * @method static Builder|Booking wherePersonnelId($value)
 * @method static Builder|Booking whereRadius($value)
 * @method static Builder|Booking whereServiceProviderId($value)
 * @method static Builder|Booking whereServiceTypeId($value)
 * @method static Builder|Booking whereSpecializationId($value)
 * @method static Builder|Booking whereStartDate($value)
 * @method static Builder|Booking whereStartTime($value)
 * @method static Builder|Booking whereStatus($value)
 * @method static Builder|Booking whereUpdatedAt($value)
 * @method static Builder|Booking whereVisitType($value)
 * @mixin Eloquent
 */
class Booking extends Model
{
    public const FREQUENCY_DAILY = 'daily';
    public const FREQUENCY_WEEKLY = 'weekly';
    public const FREQUENCY_FORTNIGHTLY = 'fortnightly';

    public const FREQUENCY_TYPES = [
        self::FREQUENCY_DAILY => 'Daily',
        self::FREQUENCY_WEEKLY => 'Weekly',
        self::FREQUENCY_FORTNIGHTLY => 'Fortnightly',
    ];

    public const STATUS_PENDING = 'pending';
    public const STATUS_CONFIRMED = 'confirmed';
    public const STATUS_COMPLETED = 'completed';

    public const STATUSES = [
        self::STATUS_PENDING => 'Pending',
        self::STATUS_CONFIRMED => 'Confirmed',
        self::STATUS_COMPLETED => 'Completed',
    ];

    public const COLUMN_STATUS = 'status';
    public const COLUMN_PARTICIPANT_ID = 'participant_id';
    public const COLUMN_SERVICE_PROVIDER_ID = 'service_provider_id';
    public const COLUMN_SPECIALIZATION_ID = 'specialization_id';

    public const TABLE_NAME = 'bookings';

    protected $casts = [
        'start_date' => 'datetime',
        'end_date' => 'datetime',
    ];

    protected $fillable = [
        self::COLUMN_PARTICIPANT_ID,
        self::COLUMN_SERVICE_PROVIDER_ID,
        'personnel_id',
        'service_type_id',
        self::COLUMN_SPECIALIZATION_ID,
        self::COLUMN_STATUS,
        'start_date',
        'end_date',
        'start_time',
        'end_time',
        'frequency',
        'visit_type',
        'address',
        'lat',
        'lng',
        'radius',
    ];

    protected static function booted(): void
    {
        self::addGlobalScope('related', function (Builder $builder): void {
            $user = Utils::getCurrentUser();

            if (!$user) {
                return;
            }

            $builder
                ->when(
                    $user->hasRole(Role::ROLE_PARTICIPANT),
                    fn (Builder $query): Builder => $query->where(self::COLUMN_PARTICIPANT_ID, $user->id)
                )
                ->when($user->hasRole(Role::ROLE_SERVICE_PROVIDER), function (Builder $query) use ($user): void {
                    $companies = $user->ownedCompanies->pluck(['id']);
                    $query->where(fn(Builder $query): Builder => $query
                        ->whereIn(self::COLUMN_SERVICE_PROVIDER_ID, $companies)
                        ->orWhereHas('bookingRequest', function (Builder $query) use ($companies): void {
                            $query->whereJsonContains(
                                BookingRequest::COLUMN_CURRENT_PROVIDER_BATCH,
                                ['id' => $companies[0]]
                            );

                            for ($i = 1; $i <= count($companies) - 1; $i++) {
                                $query->orWhereJsonContains(
                                    BookingRequest::COLUMN_CURRENT_PROVIDER_BATCH,
                                    ['id' => $companies[$i]]
                                );
                            }
                        }));
                });
        });
    }

    public function participant(): BelongsTo
    {
        return $this->belongsTo(User::class, 'participant_id');
    }

    public function serviceProvider(): BelongsTo
    {
        return $this->belongsTo(Company::class, 'service_provider_id');
    }

    public function personnel(): BelongsTo
    {
        return $this->belongsTo(Contact::class, 'personnel_id');
    }

    public function serviceType(): BelongsTo
    {
        return $this->belongsTo(ServiceType::class);
    }

    public function specialization(): BelongsTo
    {
        return $this->belongsTo(Specialization::class);
    }

    public function bookingRequest(): HasOne
    {
        return $this->hasOne(BookingRequest::class);
    }

    public function invoice(): HasOne
    {
        return $this->hasOne(Invoice::class);
    }

    public function scopeConfirmed(Builder $query): Builder
    {
        return $query->where(self::COLUMN_STATUS, self::STATUS_CONFIRMED);
    }
}
