<?php
/** @noinspection PhpMissingFieldTypeInspection */

namespace App\Models\Contact;

use App\Models\Company\Company;
use App\Models\PersonnelSpecialization\PersonnelSpecialization;
use App\Models\Specialization\Specialization;
use App\Models\User\User;
use Database\Factories\Contact\ContactFactory;
use DateTime;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;
use OwenIt\Auditing\Contracts\Auditable;
use OwenIt\Auditing\Models\Audit;
use Spatie\Tags\HasTags;
use Spatie\Tags\Tag;

/**
 * App\Models\Contact\Contact
 *
 * @property int $id
 * @property int|null $user_id
 * @property string $first_name
 * @property string $last_name
 * @property mixed $date_of_birth
 * @property string $mobile_phone_number
 * @property string|null $home_phone_number
 * @property string $email
 * @property string|null $address
 * @property string|null $lat
 * @property string|null $lng
 * @property string|null $postcode
 * @property string|null $country
 * @property string|null $ndis_number
 * @property int|null $plan_manager_id Company selected as a plan manager
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property Carbon|null $deleted_at
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $deleted_by
 * @property-read Collection|Audit[] $audits
 * @property-read int|null $audits_count
 * @property-read Collection|Company[] $companies
 * @property-read int|null $companies_count
 * @property-read string $full_name
 * @property-read Collection|Company[] $ownedCompanies
 * @property-read int|null $owned_companies_count
 * @property-read Company|null $planManager
 * @property Collection|Tag[] $tags
 * @property-read Collection|Specialization[] $specializations
 * @property-read int|null $specializations_count
 * @property-read int|null $tags_count
 * @property-read User|null $user
 * @method static ContactFactory factory(...$parameters)
 * @method static Builder|Contact newModelQuery()
 * @method static Builder|Contact newQuery()
 * @method static \Illuminate\Database\Query\Builder|Contact onlyTrashed()
 * @method static Builder|Contact query()
 * @method static Builder|Contact whereAddress($value)
 * @method static Builder|Contact whereCountry($value)
 * @method static Builder|Contact whereCreatedAt($value)
 * @method static Builder|Contact whereCreatedBy($value)
 * @method static Builder|Contact whereDateOfBirth($value)
 * @method static Builder|Contact whereDeletedAt($value)
 * @method static Builder|Contact whereDeletedBy($value)
 * @method static Builder|Contact whereEmail($value)
 * @method static Builder|Contact whereFirstName($value)
 * @method static Builder|Contact whereHomePhoneNumber($value)
 * @method static Builder|Contact whereId($value)
 * @method static Builder|Contact whereLastName($value)
 * @method static Builder|Contact whereLat($value)
 * @method static Builder|Contact whereLng($value)
 * @method static Builder|Contact whereMobilePhoneNumber($value)
 * @method static Builder|Contact whereNdisNumber($value)
 * @method static Builder|Contact wherePlanManagerId($value)
 * @method static Builder|Contact wherePostcode($value)
 * @method static Builder|Contact whereUpdatedAt($value)
 * @method static Builder|Contact whereUpdatedBy($value)
 * @method static Builder|Contact whereUserId($value)
 * @method static Builder|Contact withAllTags($tags, ?string $type = null)
 * @method static Builder|Contact withAllTagsOfAnyType($tags)
 * @method static Builder|Contact withAnyTags($tags, ?string $type = null)
 * @method static Builder|Contact withAnyTagsOfAnyType($tags)
 * @method static \Illuminate\Database\Query\Builder|Contact withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Contact withoutTrashed()
 * @mixin Eloquent
 */
class Contact extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    use HasTags;
    use SoftDeletes;
    use HasFactory;

    protected $appends = [
        'full_name',
    ];

    protected $casts = [
        'date_of_birth' => 'date:'
    ];

    protected $fillable = [
        'user_id',
        'first_name',
        'last_name',
        'date_of_birth',
        'mobile_phone_number',
        'home_phone_number',
        'email',
        'address',
        'lat',
        'lng',
        'postcode',
        'country',
        'ndis_number',
        'plan_manager_id',
    ];

    public function __construct(array $attributes = [])
    {
        $this->casts['date_of_birth'] .= config('date.carbonDate');
        parent::__construct($attributes);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function planManager(): BelongsTo
    {
        return $this->belongsTo(Company::class, 'plan_manager_id');
    }

    public function companies(): BelongsToMany
    {
        return $this->belongsToMany(Company::class);
    }

    public function ownedCompanies(): HasMany
    {
        return $this->hasMany(Company::class, Company::COLUMN_ADMIN_ID);
    }

    public function specializations(): BelongsToMany
    {
        return $this
            ->belongsToMany(Specialization::class, 'personnel_specialization', 'personnel_id')
            ->withPivot('ahpra_registration_number')
            ->using(PersonnelSpecialization::class);
    }

    /**
     * @param $value
     *
     * @noinspection PhpUnused
     */
    public function setDateOfBirthAttribute($value)
    {
        $this->attributes['date_of_birth'] = $value instanceof DateTime
            ? $value
            : Carbon::createFromFormat(config('date.carbonDate'), $value);
    }

    /**
     * @return string
     * @noinspection PhpUnused
     */
    public function getFullNameAttribute(): string
    {
        return "$this->first_name $this->last_name";
    }
}
