<?php
/** @noinspection PhpMissingFieldTypeInspection */

namespace App\Models\ServiceType;

use App\Models\Company\Company;
use App\Models\Specialization\Specialization;
use Database\Factories\ServiceType\ServiceTypeFactory;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;

/**
 * App\Models\ServiceType\ServiceType
 *
 * @property int $id
 * @property string $service_name
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property Carbon|null $deleted_at
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $deleted_by
 * @property-read Collection|Company[] $serviceProviders
 * @property-read int|null $service_providers_count
 * @property-read Collection|Specialization[] $specializations
 * @property-read int|null $specializations_count
 * @method static ServiceTypeFactory factory(...$parameters)
 * @method static Builder|ServiceType newModelQuery()
 * @method static Builder|ServiceType newQuery()
 * @method static \Illuminate\Database\Query\Builder|ServiceType onlyTrashed()
 * @method static Builder|ServiceType query()
 * @method static Builder|ServiceType whereCreatedAt($value)
 * @method static Builder|ServiceType whereCreatedBy($value)
 * @method static Builder|ServiceType whereDeletedAt($value)
 * @method static Builder|ServiceType whereDeletedBy($value)
 * @method static Builder|ServiceType whereId($value)
 * @method static Builder|ServiceType whereServiceName($value)
 * @method static Builder|ServiceType whereUpdatedAt($value)
 * @method static Builder|ServiceType whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|ServiceType withTrashed()
 * @method static \Illuminate\Database\Query\Builder|ServiceType withoutTrashed()
 * @mixin Eloquent
 */
class ServiceType extends Model
{
    use SoftDeletes;
    use HasFactory;

    protected $fillable = [
        'service_name',
    ];

    public function serviceProviders(): BelongsToMany
    {
        return $this->belongsToMany(
            Company::class,
            'service_provider_service_type',
            'service_type_id',
            'service_provider_id'
        );
    }

    public function specializations(): HasMany
    {
        return $this->hasMany(Specialization::class);
    }
}
