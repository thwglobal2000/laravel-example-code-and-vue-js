<?php
/** @noinspection PhpMissingFieldTypeInspection */

namespace App\Models\EmailVerificationCode;

use App\Models\User\User;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;

/**
 * App\Models\EmailVerificationCode\EmailVerificationCode
 *
 * @property int $id
 * @property int $user_id
 * @property string $hash
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read User $user
 * @method static Builder|EmailVerificationCode newModelQuery()
 * @method static Builder|EmailVerificationCode newQuery()
 * @method static Builder|EmailVerificationCode query()
 * @method static Builder|EmailVerificationCode whereCreatedAt($value)
 * @method static Builder|EmailVerificationCode whereHash($value)
 * @method static Builder|EmailVerificationCode whereId($value)
 * @method static Builder|EmailVerificationCode whereUpdatedAt($value)
 * @method static Builder|EmailVerificationCode whereUserId($value)
 * @mixin Eloquent
 */
class EmailVerificationCode extends Model
{
    protected $fillable = [
        'user_id',
        'hash',
    ];

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName(): string
    {
        return 'hash';
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
