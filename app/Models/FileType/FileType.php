<?php

namespace App\Models\FileType;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FileType extends Model
{
    use HasFactory;
    protected $guarded = [];
    public $timestamps = false;
}
