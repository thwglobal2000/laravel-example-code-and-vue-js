<?php

namespace App\Models\Widget;

use App\Models\User\User;
use App\Models\WidgetSetting\WidgetSetting;
use Eloquent;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Carbon;

/**
 * App\Models\Widget\Widget
 *
 * @property int $id
 * @property string $component
 * @property string|null $header
 * @property string $permission
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property Carbon|null $deleted_at
 * @property-read WidgetSetting|null $settings
 * @property-read User $user
 * @method static \Illuminate\Database\Eloquent\Builder|Widget newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Widget newQuery()
 * @method static Builder|Widget onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Widget query()
 * @method static \Illuminate\Database\Eloquent\Builder|Widget whereComponent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Widget whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Widget whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Widget whereHeader($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Widget whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Widget wherePermission($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Widget whereUpdatedAt($value)
 * @method static Builder|Widget withTrashed()
 * @method static Builder|Widget withoutTrashed()
 * @mixin Eloquent
 */
class Widget extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'component',
        'header',
        'permission'
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function settings()
    {
        return $this->hasOne(WidgetSetting::class);
    }
}
