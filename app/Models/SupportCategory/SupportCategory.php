<?php

namespace App\Models\SupportCategory;

use App\Models\RateMatrix\RateMatrix;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;

/**
 * App\Models\SupportCategory\SupportCategory
 *
 * @property int $id
 * @property string $name
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Collection|RateMatrix[] $rateMatrices
 * @property-read int|null $rate_matrices_count
 * @method static Builder|SupportCategory newModelQuery()
 * @method static Builder|SupportCategory newQuery()
 * @method static Builder|SupportCategory query()
 * @method static Builder|SupportCategory whereCreatedAt($value)
 * @method static Builder|SupportCategory whereId($value)
 * @method static Builder|SupportCategory whereName($value)
 * @method static Builder|SupportCategory whereUpdatedAt($value)
 * @mixin Eloquent
 */
class SupportCategory extends Model
{
    protected $fillable = [
        'name',
    ];

    public function rateMatrices(): HasMany
    {
        return $this->hasMany(RateMatrix::class);
    }
}
