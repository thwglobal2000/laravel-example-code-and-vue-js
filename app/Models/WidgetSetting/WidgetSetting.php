<?php
namespace App\Models\WidgetSetting;

use App\Models\Widget\Widget;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Models\WidgetSetting\WidgetSetting
 *
 * @property int $id
 * @property int|null $user_id
 * @property string $widget_name
 * @property string|null $widget_header
 * @property string|null $widget_permission
 * @property int $i
 * @property int $x
 * @property int $y
 * @property int $w
 * @property int $h
 * @property int $is_visible
 * @property int $is_deleted
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Widget $settings
 * @method static Builder|WidgetSetting newModelQuery()
 * @method static Builder|WidgetSetting newQuery()
 * @method static Builder|WidgetSetting query()
 * @method static Builder|WidgetSetting whereCreatedAt($value)
 * @method static Builder|WidgetSetting whereH($value)
 * @method static Builder|WidgetSetting whereI($value)
 * @method static Builder|WidgetSetting whereId($value)
 * @method static Builder|WidgetSetting whereIsDeleted($value)
 * @method static Builder|WidgetSetting whereIsVisible($value)
 * @method static Builder|WidgetSetting whereUpdatedAt($value)
 * @method static Builder|WidgetSetting whereUserId($value)
 * @method static Builder|WidgetSetting whereW($value)
 * @method static Builder|WidgetSetting whereWidgetHeader($value)
 * @method static Builder|WidgetSetting whereWidgetName($value)
 * @method static Builder|WidgetSetting whereWidgetPermission($value)
 * @method static Builder|WidgetSetting whereX($value)
 * @method static Builder|WidgetSetting whereY($value)
 * @mixin Eloquent
 */
class WidgetSetting extends Model
{
    public $table = 'widgets_settings';

    protected $fillable = [
        'widget_name',
        'user_id',
        'i',
        'x',
        'y',
        'w',
        'h',
        'is_visible',
        'is_deleted',
        'widget_permission',
        'widget_header'
    ];

    public function settings()
    {
        return $this->belongsTo(Widget::class);
    }
}
