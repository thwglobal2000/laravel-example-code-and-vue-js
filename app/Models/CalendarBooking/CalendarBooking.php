<?php

namespace App\Models\CalendarBooking;

use App\Models\Booking\Booking;
use App\Models\User\User;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;

/**
 * App\Models\CalendarBooking\CalendarBooking
 *
 * @property int $id
 * @property int $booking_id
 * @property int $personnel_id
 * @property string $date
 * @property string $start_time
 * @property string $end_time
 * @property string $status
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Booking $booking
 * @property-read User $personnel
 * @method static Builder|CalendarBooking newModelQuery()
 * @method static Builder|CalendarBooking newQuery()
 * @method static Builder|CalendarBooking query()
 * @method static Builder|CalendarBooking whereBookingId($value)
 * @method static Builder|CalendarBooking whereCreatedAt($value)
 * @method static Builder|CalendarBooking whereDate($value)
 * @method static Builder|CalendarBooking whereEndTime($value)
 * @method static Builder|CalendarBooking whereId($value)
 * @method static Builder|CalendarBooking wherePersonnelId($value)
 * @method static Builder|CalendarBooking whereStartTime($value)
 * @method static Builder|CalendarBooking whereStatus($value)
 * @method static Builder|CalendarBooking whereUpdatedAt($value)
 * @mixin Eloquent
 */
class CalendarBooking extends Model
{
    public function booking(): BelongsTo
    {
        return $this->belongsTo(Booking::class);
    }

    public function personnel(): BelongsTo
    {
        return $this->belongsTo(User::class, 'personnel_id');
    }
}
