<?php

namespace App\Models\CompanyContact;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\Pivot;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;

/**
 * App\Models\CompanyContact\CompanyContact
 *
 * @property int $company_id
 * @property int $contact_id
 * @property Carbon|null $deleted_at
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $deleted_by
 * @method static Builder|CompanyContact newModelQuery()
 * @method static Builder|CompanyContact newQuery()
 * @method static \Illuminate\Database\Query\Builder|CompanyContact onlyTrashed()
 * @method static Builder|CompanyContact query()
 * @method static Builder|CompanyContact whereCompanyId($value)
 * @method static Builder|CompanyContact whereContactId($value)
 * @method static Builder|CompanyContact whereCreatedBy($value)
 * @method static Builder|CompanyContact whereDeletedAt($value)
 * @method static Builder|CompanyContact whereDeletedBy($value)
 * @method static Builder|CompanyContact whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|CompanyContact withTrashed()
 * @method static \Illuminate\Database\Query\Builder|CompanyContact withoutTrashed()
 * @mixin Eloquent
 */
class CompanyContact extends Pivot
{
    use SoftDeletes;

    protected $guarded = [];
    public $timestamps = false;
}
