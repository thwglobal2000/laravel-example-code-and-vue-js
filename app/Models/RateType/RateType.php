<?php

namespace App\Models\RateType;

use App\Models\RateMatrix\RateMatrix;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;

/**
 * App\Models\RateType\RateType
 *
 * @property int $id
 * @property string $name
 * @property int|null $mm_value
 * @property string|null $state
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Collection|RateMatrix[] $rateMatrices
 * @property-read int|null $rate_matrices_count
 * @method static Builder|RateType newModelQuery()
 * @method static Builder|RateType newQuery()
 * @method static Builder|RateType query()
 * @method static Builder|RateType whereCreatedAt($value)
 * @method static Builder|RateType whereId($value)
 * @method static Builder|RateType whereMmValue($value)
 * @method static Builder|RateType whereName($value)
 * @method static Builder|RateType whereState($value)
 * @method static Builder|RateType whereUpdatedAt($value)
 * @mixin Eloquent
 */
class RateType extends Model
{
    public const REMOTE_RATE_TYPES = [
        ['mm_value' => 6, 'name' => 'Remote'],
        ['mm_value' => 7, 'name' => 'Very remote'],
    ];

    protected $fillable = [
        'name',
        'state',
        'mm_value',
    ];

    public function rateMatrices(): HasMany
    {
        return $this->hasMany(RateMatrix::class);
    }
}
