<?php
/** @noinspection PhpMissingFieldTypeInspection */

namespace App\Models\ServiceProviderRate;

use App\Models\Company\Company;
use App\Models\Specialization\Specialization;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;

/**
 * App\Models\ServiceProviderRate\ServiceProviderRate
 *
 * @property int $id
 * @property int $company_id
 * @property int $specialization_id
 * @property float $rate_amount
 * @property Carbon $rate_date
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property Carbon|null $deleted_at
 * @property-read Company $company
 * @property-read string $rate_date_human
 * @property-read Specialization $specialization
 * @method static Builder|ServiceProviderRate newModelQuery()
 * @method static Builder|ServiceProviderRate newQuery()
 * @method static \Illuminate\Database\Query\Builder|ServiceProviderRate onlyTrashed()
 * @method static Builder|ServiceProviderRate query()
 * @method static Builder|ServiceProviderRate whereCompanyId($value)
 * @method static Builder|ServiceProviderRate whereCreatedAt($value)
 * @method static Builder|ServiceProviderRate whereDeletedAt($value)
 * @method static Builder|ServiceProviderRate whereId($value)
 * @method static Builder|ServiceProviderRate whereRateAmount($value)
 * @method static Builder|ServiceProviderRate whereRateDate($value)
 * @method static Builder|ServiceProviderRate whereSpecializationId($value)
 * @method static Builder|ServiceProviderRate whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|ServiceProviderRate withTrashed()
 * @method static \Illuminate\Database\Query\Builder|ServiceProviderRate withoutTrashed()
 * @mixin Eloquent
 */
class ServiceProviderRate extends Model
{
    use SoftDeletes;

    public const TABLE_NAME = 'service_provider_rates';
    public const COLUMN_COMPANY_ID = 'company_id';
    public const COLUMN_SPECIALIZATION_ID = 'specialization_id';
    public const COLUMN_RATE_AMOUNT = 'rate_amount';

    protected $appends = [
        'rate_date_human'
    ];

    protected $dates = [
        'rate_date',
    ];

    protected $fillable = [
        self::COLUMN_COMPANY_ID,
        self::COLUMN_SPECIALIZATION_ID,
        self::COLUMN_RATE_AMOUNT,
        'rate_date',
    ];

    protected $casts = [
        self::COLUMN_RATE_AMOUNT => 'float'
    ];

    public function company(): BelongsTo
    {
        return $this->belongsTo(Company::class);
    }

    public function specialization(): BelongsTo
    {
        return $this->belongsTo(Specialization::class);
    }

    /**
     * @return string
     * @noinspection PhpUnused
     */
    public function getRateDateHumanAttribute(): string
    {
        return $this->rate_date->format(config('date.carbonDate'));
    }
}
