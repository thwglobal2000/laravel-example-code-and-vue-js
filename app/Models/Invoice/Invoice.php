<?php

namespace App\Models\Invoice;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Models\Invoice\Invoice
 *
 * @property int $id
 * @property int $company_id
 * @property int|null $booking_id
 * @property int|null $rate_matrix_id
 * @property string $invoice_number
 * @property string|null $stripe_invoice_id
 * @property string $invoice_date
 * @property int|null $qty
 * @property string|null $price
 * @property float $total_price
 * @property string $gst
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|Invoice newModelQuery()
 * @method static Builder|Invoice newQuery()
 * @method static Builder|Invoice query()
 * @method static Builder|Invoice whereBookingId($value)
 * @method static Builder|Invoice whereCompanyId($value)
 * @method static Builder|Invoice whereCreatedAt($value)
 * @method static Builder|Invoice whereGst($value)
 * @method static Builder|Invoice whereId($value)
 * @method static Builder|Invoice whereInvoiceDate($value)
 * @method static Builder|Invoice whereInvoiceNumber($value)
 * @method static Builder|Invoice wherePrice($value)
 * @method static Builder|Invoice whereQty($value)
 * @method static Builder|Invoice whereRateMatrixId($value)
 * @method static Builder|Invoice whereStripeInvoiceId($value)
 * @method static Builder|Invoice whereTotalPrice($value)
 * @method static Builder|Invoice whereUpdatedAt($value)
 * @mixin Eloquent
 */
class Invoice extends Model
{
    public const TABLE_NAME = 'invoices';

    public const COLUMN_PRICE = 'price';
    public const COLUMN_BOOKING_ID = 'booking_id';

    protected $fillable = [
        'company_id',
        self::COLUMN_BOOKING_ID,
        'rate_matrix_id',
        'invoice_number',
        'invoice_date',
        'qty',
        self::COLUMN_PRICE,
        'stripe_invoice_id',
        'total_price',
        'gst',
    ];
}
