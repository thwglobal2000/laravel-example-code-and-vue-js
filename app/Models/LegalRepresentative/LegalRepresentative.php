<?php
/** @noinspection PhpMissingFieldTypeInspection */

namespace App\Models\LegalRepresentative;

use App\Models\User\User;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;

/**
 * App\Models\LegalRepresentative\LegalRepresentative
 *
 * @property int $id
 * @property int $participant_id
 * @property int $representative_id
 * @property string|null $representative_type
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read User $participant
 * @property-read User $representative
 * @method static Builder|LegalRepresentative newModelQuery()
 * @method static Builder|LegalRepresentative newQuery()
 * @method static Builder|LegalRepresentative query()
 * @method static Builder|LegalRepresentative whereCreatedAt($value)
 * @method static Builder|LegalRepresentative whereId($value)
 * @method static Builder|LegalRepresentative whereParticipantId($value)
 * @method static Builder|LegalRepresentative whereRepresentativeId($value)
 * @method static Builder|LegalRepresentative whereRepresentativeType($value)
 * @method static Builder|LegalRepresentative whereUpdatedAt($value)
 * @mixin Eloquent
 */
class LegalRepresentative extends Model
{
    public const SUPPORT_COORDINATOR_TYPE = 'Support Coordinator';
    public const LEGAL_GUARDIAN_TYPE = 'Legal Guardian';

    public const REPRESENTATIVE_TYPES = [
        self::LEGAL_GUARDIAN_TYPE => 'Legal Guardian',
        self::SUPPORT_COORDINATOR_TYPE => 'Support Coordinator',
    ];

    protected $fillable = [
        'participant_id',
        'representative_id',
        'representative_type',
    ];

    public function participant(): BelongsTo
    {
        return $this->belongsTo(User::class, 'participant_id');
    }

    public function representative(): BelongsTo
    {
        return $this->belongsTo(User::class, 'representative_id');
    }
}
