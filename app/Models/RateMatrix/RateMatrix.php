<?php

namespace App\Models\RateMatrix;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Models\RateMatrix\RateMatrix
 *
 * @property int $id
 * @property int $rate_type_id
 * @property int $support_name_id
 * @property int $support_category_id
 * @property int $specialization_id
 * @property int $service_type_id
 * @property string $timeframe
 * @property string $status
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|RateMatrix newModelQuery()
 * @method static Builder|RateMatrix newQuery()
 * @method static Builder|RateMatrix query()
 * @method static Builder|RateMatrix whereCreatedAt($value)
 * @method static Builder|RateMatrix whereId($value)
 * @method static Builder|RateMatrix whereRateTypeId($value)
 * @method static Builder|RateMatrix whereServiceTypeId($value)
 * @method static Builder|RateMatrix whereSpecializationId($value)
 * @method static Builder|RateMatrix whereStatus($value)
 * @method static Builder|RateMatrix whereSupportCategoryId($value)
 * @method static Builder|RateMatrix whereSupportNameId($value)
 * @method static Builder|RateMatrix whereTimeframe($value)
 * @method static Builder|RateMatrix whereUpdatedAt($value)
 * @mixin Eloquent
 */
class RateMatrix extends Model
{
    //
}
