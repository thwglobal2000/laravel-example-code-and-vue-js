<?php

namespace App\Console\Commands;

use App\Models\Permission\Permission;
use App\Models\Role\Role;
use Eloquent;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class PermissionsUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'permissions:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update roles and permission due to config';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Truncating Role and Permission tables');
        $this->truncateLaratrustTables();

        $roles         = config('laratrust_seeder.role_structure');
        $role_user     = config('laratrust.tables.role_user');
        $user_models   = config('laratrust.user_models.users');
        $access_levels = config('laratrust_seeder.access_levels');

        Eloquent::unguard();
        foreach ($roles as $roleName => $perm) {
            $roleDisplayName = ucwords(str_replace(['_'], [' '], $roleName));

            $role = Role::updateOrCreate(
                ['name' => $roleName],
                [
                    'display_name' => $roleDisplayName,
                    'description'  => $roleDisplayName,
                    'level'        => $access_levels[$roleName]
                ]
            );

            $permissions = [];
            $this->info("Updating permissions for $roleName", 1);
            foreach ($perm as $permission) {
                $title = ucfirst(str_replace(['.'], [' '], $permission));

                $permissions[] = Permission::firstOrCreate(
                    ['name' => $permission],
                    [
                        'display_name' => $title,
                        'description'  => $title
                    ]
                )->id;

                $this->info("Creating Permission to $permission for $roleName", 'v');
            }

            // Attach all permissions to the role
            $role->permissions()->sync($permissions);

            // Update roles models bindings
            $this->info("Updating user roles model bindings to $user_models", 'v');
            DB::table($role_user)->update([
                'user_type' => $user_models
            ]);
        }

        return 0;
    }

    public function truncateLaratrustTables()
    {
        $roleNames = array_keys(config('laratrust_seeder.role_structure'));
        $roles     = DB::table('roles')->whereIn('name', $roleNames)->get();
        $roleIDs   = $roles->pluck('id')->toArray();
        Schema::disableForeignKeyConstraints();
        DB::table('permission_role')->whereIn('role_id', $roleIDs)->delete();
        Schema::enableForeignKeyConstraints();
    }
}
