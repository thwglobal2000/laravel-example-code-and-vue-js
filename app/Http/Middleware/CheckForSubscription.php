<?php

namespace App\Http\Middleware;

use App\Libraries\Utils\Utils;
use App\Modules\Participant\Subscriptions\Constants\SubscriptionsConstants;
use Closure;
use Illuminate\Http\Request;
use Laratrust;
use Symfony\Component\HttpFoundation\Response;

class CheckForSubscription
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     *
     * @return Closure
     */
    public function handle(Request $request, Closure $next): Closure
    {
        if (!$user = Utils::getCurrentUser()) {
            abort(Response::HTTP_UNPROCESSABLE_ENTITY, 'Current user is not determined');
        }

        if (!(Laratrust::isAbleTo(SubscriptionsConstants::SUPPRESS_CHECK_SUBSCRIPTION) || $user->isSubscribed($user))) {
            abort(Response::HTTP_FORBIDDEN, 'Subscription is inactive. Access denied');
        }

        return $next($request);
    }
}
