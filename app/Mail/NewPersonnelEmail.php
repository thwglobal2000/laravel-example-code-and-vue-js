<?php

namespace App\Mail;

use App\Models\Company\Company;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Sichikawa\LaravelSendgridDriver\SendGrid;

class NewPersonnelEmail extends Mailable
{
    use Queueable;
    use SerializesModels;
    use SendGrid;

    private array $credentials;
    private string $company_names;

    /**
     * Create a new message instance.
     *
     * @param string $company_names
     * @param array $credentials
     */
    public function __construct(string $company_names, array $credentials)
    {
        $this->company_names = $company_names;
        $this->credentials = $credentials;
    }

    /**
     * Build the message.
     *
     * @return $this
     * @noinspection PhpParamsInspection
     */
    public function build(): NewPersonnelEmail
    {
        $senderEmail = config('mail.from.address');
        $senderName = config('mail.from.name');

        return $this
            ->view([])
            ->to($this->credentials['email'], $this->credentials['name'])
            ->from($senderEmail, $senderName)
            ->replyTo($senderEmail, $senderName)
            ->subject('subject') // this gets overwritten by the template
            ->sendgrid([
                'personalizations' => [
                        [
                            'dynamic_template_data' => [
                                'App_Name' => config('app.name'),
                                'User_Name' => $this->credentials['name'],
                                'Company_Name' => $this->company_names,
                                'User_Email' => $this->credentials['email'],
                                'User_Password' => $this->credentials['password'],
                            ],
                        ],
                    ],
                    'template_id' => config('services.sendgrid.templates.new_personnel_email'),
            ]);
    }
}
