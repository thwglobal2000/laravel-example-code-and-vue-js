<?php

namespace App\Mail;

use App\Models\User\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Sichikawa\LaravelSendgridDriver\SendGrid;

class UserRegistered extends Mailable
{
    use Queueable;
    use SerializesModels;
    use SendGrid;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    private User $user;
    private string $password;
    private string $verificationCode;

    public function __construct(User $user, string $password, string $verificationCode)
    {
        $this->user = $user;
        $this->password = $password;
        $this->verificationCode = $verificationCode;
    }

    /**
     * Build the message.
     *
     * @return $this
     * @noinspection PhpParamsInspection
     */
    public function build(): UserRegistered
    {
        $senderEmail = config('mail.from.address');
        $senderName = config('mail.from.name');
        $verificationUrl = config('app.url') . "/verify/{$this->verificationCode}";

        return $this
            ->view([])
            ->to($this->user->email, $this->user->name)
            ->from($senderEmail, $senderName)
            ->replyTo($senderEmail, $senderName)
            ->subject('subject') // this gets overwritten by the template
            ->sendgrid([
                'personalizations' => [
                    [
                        'dynamic_template_data' => [
                            'userName' => $this->user->name,
                            'userEmail' => $this->user->email,
                            'userPassword' => $this->password,
                            'verificationUrl' => $verificationUrl,
                        ],
                    ],
                ],
                'template_id' => config('services.sendgrid.templates.user_registered_email'),
            ]);
    }
}
