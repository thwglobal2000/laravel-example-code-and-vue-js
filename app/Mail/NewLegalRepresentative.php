<?php

namespace App\Mail;

use App\Models\User\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Sichikawa\LaravelSendgridDriver\SendGrid;

class NewLegalRepresentative extends Mailable
{
    use Queueable;
    use SerializesModels;
    use SendGrid;

    private User $legalRepresentative;
    private string $verificationCode;

    /**
     * Create a new message instance.
     *
     * @param User $legalRepresentative
     * @param string $verificationCode
     */
    public function __construct(User $legalRepresentative, string $verificationCode)
    {
        $this->legalRepresentative = $legalRepresentative;
        $this->verificationCode = $verificationCode;
    }

    /**
     * Build the message.
     *
     * @return $this
     * @noinspection PhpParamsInspection
     */
    public function build(): NewLegalRepresentative
    {
        $senderEmail = config('mail.from.address');
        $senderName = config('mail.from.name');

        $verificationUrl = config('app.url') . "/verify/legal-representative/{$this->verificationCode}";

        return $this
            ->view([])
            ->to($this->legalRepresentative->email, $this->legalRepresentative->name)
            ->from($senderEmail, $senderName)
            ->replyTo($senderEmail, $senderName)
            ->subject('subject')
            ->sendgrid([
                'personalizations' => [
                    [
                        'dynamic_template_data' => [
                            'url' => $verificationUrl,
                            'userName' => $this->legalRepresentative->name,
                            'userEmail' => $this->legalRepresentative->email,
                        ],
                    ],
                ],
                'template_id' => config('services.sendgrid.templates.new_legal_representative'),
            ]);
    }
}
