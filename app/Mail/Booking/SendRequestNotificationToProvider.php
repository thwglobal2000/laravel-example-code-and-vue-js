<?php

namespace App\Mail\Booking;

use App\Libraries\Utils\Utils;
use App\Models\BookingRequest\BookingRequest;
use App\Models\Company\Company;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Sichikawa\LaravelSendgridDriver\SendGrid;

class SendRequestNotificationToProvider extends Mailable
{
    use Queueable;
    use SerializesModels;
    use SendGrid;

    protected BookingRequest $bookingRequest;
    protected Company $company;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Company $company, BookingRequest $bookingRequest)
    {
        $this->company = $company;
        $this->bookingRequest = $bookingRequest;
    }

    /**
     * Build the message.
     *
     * @return $this
     * @noinspection PhpParamsInspection
     */
    public function build(): self
    {
        $senderEmail = config('mail.from.address');
        $senderName = config('mail.from.name');

        $providerHash = Utils::encodeId($this->company->id);

        return $this
            ->view([])
            ->to($this->company->email, $this->company->business_name)
            ->from($senderEmail, $senderName)
            ->replyTo($senderEmail, $senderName)
            ->subject('subject') // this gets overwritten by the template
            ->sendgrid([
                'personalizations' => [
                    [
                        'dynamic_template_data' => [
                            'service' => $this->bookingRequest->serviceType->service_name,
                            'confirmUrl' => $this->bookingRequest->getConfirmUrl($providerHash),
                            'denyUrl' => $this->bookingRequest->getDenyUrl($providerHash),
                        ],
                    ],
                ],
                'template_id' => config('services.sendgrid.templates.booking.request_to_provider'),
            ]);
    }
}
