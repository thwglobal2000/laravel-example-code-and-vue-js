<?php

namespace App\Mail\Booking;

use App\Models\Booking\Booking;
use App\Models\User\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Sichikawa\LaravelSendgridDriver\SendGrid;

class SendConfirmationToParticipant extends Mailable
{
    use Queueable;
    use SerializesModels;
    use SendGrid;

    protected Booking $booking;
    protected User $participant;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $participant, Booking $booking)
    {
        $this->participant = $participant;
        $this->booking = $booking;
    }

    /**
     * Build the message.
     *
     * @return $this
     * @noinspection PhpParamsInspection
     */
    public function build(): self
    {
        $senderEmail = config('mail.from.address');
        $senderName = config('mail.from.name');

        return $this
            ->view([])
            ->to($this->participant->email, $this->participant->name)
            ->from($senderEmail, $senderName)
            ->replyTo($senderEmail, $senderName)
            ->subject('subject') // this gets overwritten by the template
            ->sendgrid([
                'personalizations' => [
                    [
                        'dynamic_template_data' => [
                            'service' => $this->booking->serviceType->service_name,
                        ],
                    ],
                ],
                'template_id' => config('services.sendgrid.templates.booking.confirmation_to_participant'),
            ]);
    }
}
