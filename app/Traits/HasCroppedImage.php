<?php


namespace App\Traits;

use Illuminate\Support\Facades\Storage;

trait HasCroppedImage
{

    /**
     * @param string $imgPath
     * @param string $storagePublicSubPath
     * @param int $id
     *
     * @return string
     */
    public function getCroppedImagePath(string $imgPath, string $storagePublicSubPath, int $id): string
    {
        $isTempPath = strpos($imgPath, 'temp');
        if ($isTempPath) {
            $name = explode('temp', $imgPath)[1];
            Storage::move('public/temp' . $name, 'public/' . $storagePublicSubPath . '/' . $id . '' . $name);
            $imgPath = '/storage/' . $storagePublicSubPath . '/' . $id . '' . $name;
        }

        return $imgPath;
    }

    /**
     * @param $imagePath
     *
     * @return int
     */
    public function deleteCroppedImage($imagePath): int
    {
        $path = explode('/storage/', $imagePath);

        if (isset($path[1]) && Storage::exists("public/{$path[1]}")) {
            return Storage::delete('public/' . $path[1]);
        }

        return 1;
    }
}
