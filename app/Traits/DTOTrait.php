<?php

namespace App\Traits;

use InvalidArgumentException;

trait DTOTrait
{

    /**
     * Make DTO from keyed array
     *
     * @param $arrayData
     *
     * @return mixed
     */
    public static function make($arrayData)
    {
        return (new self())->fill($arrayData);
    }

    /**
     * Fill DTO via assoc array
     * Array keys must be equal property names
     *
     * @param array $arrayData
     *
     * @return $this
     */
    public function fill(array $arrayData)
    {
        foreach ($arrayData as $key => $value) {
            $methodName = 'set' . $key;
            if (method_exists($this, $methodName)) {
                $this->$methodName($value);
            }
        }

        return $this;
    }

    /**
     * Restrict dynamic properties
     *
     * @param $name
     * @param $value
     */
    public function __set($name, $value)
    {
        throw new InvalidArgumentException(trans('common.restrict_dynamic_properties', ['class' => __CLASS__]));
    }

    /**
     * @return false|string
     */
    public function __toString()
    {
        return json_encode(get_object_vars($this));
    }
}