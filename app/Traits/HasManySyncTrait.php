<?php

namespace App\Traits;

trait HasManySyncTrait
{

    public function syncModels($relationship, $field, $values)
    {
        $this->$relationship()->each(function ($record) use (&$values, $field) {
            if (in_array($record->$field, $values)) {
                if (($key = array_search($record->$field, $values)) !== false) {
                    unset($values[$key]);
                }
            } else {
                $record->delete();
            }
        });
        foreach ($values as $value) {
            $this->$relationship()->create([
                $field => $value
            ]);
        }
    }
}
