<?php

namespace App\Jobs\Booking;

use App\Models\BookingRequest\BookingRequest;
use App\Models\Company\Company;
use App\Services\Booking\BookingService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Collection;

class SendNotificationToProvidersBatch implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    /**
     * Provider ids to be notified about request
     *
     * @var Collection
     */
    protected ?Collection $currentProviderIds;

    /**
     * Provider ids to be notified about failed request
     *
     * @var Collection
     */
    protected Collection $previousProviderIds;

    /**
     * Created booking request with common info and provider ids
     *
     * @var BookingRequest
     */
    protected BookingRequest $bookingRequest;

    /**
     * Booking service for sending notifications
     *
     * @var BookingService
     */
    protected BookingService $bookingService;

    /**
     * Create a new job instance.
     *
     * @param BookingRequest $bookingRequest
     *
     * @throws BindingResolutionException
     */
    public function __construct(BookingRequest $bookingRequest)
    {
        $this->bookingRequest = $bookingRequest->refresh();
        $this->bookingService = app()->make(BookingService::class);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(): void
    {
        $nextBatches  = $this->bookingRequest->next_provider_batches;
        $currentChunk = $nextBatches->shift();

        $this->currentProviderIds  = collect($currentChunk)->pluck('id');
        $this->previousProviderIds = $this->bookingRequest->current_providers_batch->pluck('id') ?? collect();

        $this->bookingRequest->update([
            'next_provider_batches'   => $nextBatches,
            'current_providers_batch' => $currentChunk,
        ]);


        $this->denyAndNotifyPreviousProvidersIfExists();

        if ($this->currentProviderIds->isEmpty()) {
            return;
        }
        $this->notifyNewProviders();
        $this->scheduleNextBatchNotifications();
    }

    /**
     * Notify previous batch of providers about time out
     */
    private function denyAndNotifyPreviousProvidersIfExists(): void
    {
        foreach ($this->previousProviderIds as $providerId) {
            $provider = Company::find($providerId);
            if (!$provider) {
                continue;
            }

            $this->bookingService->notifyProvidersAboutTimeoutRequest($provider, $this->bookingRequest);
        }
    }

    /**
     * Notify current batch about request
     */
    private function notifyNewProviders(): void
    {
        foreach ($this->currentProviderIds as $providerId) {
            $provider = Company::find($providerId);
            if (!$provider) {
                continue;
            }

            $this->notifyProvider($provider);
        }
    }

    /**
     * Notify provider about new request
     *
     * @param Company $provider
     */
    private function notifyProvider(Company $provider): void
    {
        $this->bookingService->notifyProviderAboutRequest($provider, $this->bookingRequest);
    }

    /**
     * Dispatch this job for next providers with interval
     */
    private function scheduleNextBatchNotifications(): void
    {
        if (!($this->bookingRequest->next_provider_batches || $this->bookingRequest->current_providers_batch)) {
            return;
        }

        $confirmThreshold = now()->addMinutes(BookingService::TIME_IN_MINUTES_FOR_CONFIRM_REQUEST);
        static::dispatch($this->bookingRequest)->delay($confirmThreshold);
    }
}
