<?php

namespace App\Modules\Representative\Verification\Controllers;

use App\Http\Controllers\Controller;
use App\Models\LegalRepresentativeVerifyCode\LegalRepresentativeVerifyCode;
use App\Modules\Representative\Verification\Requests\VerificationRequest;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Throwable;

class VerificationController extends Controller
{
    /**
     * @param LegalRepresentativeVerifyCode $code
     * @param VerificationRequest $request
     * @return JsonResponse
     * @throws Throwable
     * @noinspection PhpUndefinedFieldInspection
     */
    public function __invoke(LegalRepresentativeVerifyCode $code, VerificationRequest $request): JsonResponse
    {
        $user = $code->legalRepresentative;
        if ($user->email != $request->email) {
            throw new Exception('Invalid verification code');
        }

        DB::transaction(function () use ($user, $request, $code) {
            $user->update(['password' => $request->password]);
            $code->forceDelete();
        });

        return response()->json(['message' => 'Successfully saved. Please, sign in']);
    }
}
