<?php

Route::prefix('legal-representative/verify/{code}')->middleware(['guest'])->group(function () {
    Route::post('/', 'VerificationController')->name('legal-representative.verify');
});
