<?php

namespace App\Modules\ServiceProvider\Bookings\Controllers;

use App\Http\Controllers\Controller;
use App\Libraries\Utils\Utils;
use App\Models\Booking\Booking;
use App\Models\Invoice\Invoice;
use App\Models\ServiceProviderRate\ServiceProviderRate;
use App\Services\Booking\BookingService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class BookingController extends Controller
{
    protected BookingService $booking;

    public function index(Request $request): JsonResponse
    {
        [$column, $order] = explode(',', $request->input('sortBy', 'id,desc'));
        $companies = Utils::getCurrentUser()->ownedCompanies()->pluck('companies.id');

        $query = Booking
            ::with(['serviceType', 'invoice:id,booking_id']);

        $confirmed = $query
            ->clone()
            ->whereNotNull(Booking::COLUMN_SERVICE_PROVIDER_ID)
            ->addSelect([
                'rate' => Invoice
                    ::select(Invoice::COLUMN_PRICE)
                    ->whereColumn(Invoice::COLUMN_BOOKING_ID, Booking::TABLE_NAME . '.id')
                    ->orderBy(Invoice::TABLE_NAME . '.id')
                    ->limit(1)
            ]);

        $columnRate = ServiceProviderRate::COLUMN_RATE_AMOUNT;

        $pending = $query
            ->clone()
            ->whereNull(Booking::COLUMN_SERVICE_PROVIDER_ID)
            ->addSelect([
                'rate' => ServiceProviderRate
                    ::selectRaw(
                        "IF (count(DISTINCT $columnRate) = 1, MAX($columnRate), CONCAT(MIN($columnRate), ' - ', MAX($columnRate)))"
                    )
                    ->whereIn(ServiceProviderRate::COLUMN_COMPANY_ID, $companies)
                    ->whereColumn(
                        ServiceProviderRate::COLUMN_SPECIALIZATION_ID,
                        Booking::TABLE_NAME . '.' . Booking::COLUMN_SPECIALIZATION_ID
                    ),
            ]);

        $query = $confirmed->union($pending)->orderBy($column, $order);

        return response()->json(
            $request->pageSize ? $query->paginate($request->pageSize) : $query->get()
        );
    }
}
