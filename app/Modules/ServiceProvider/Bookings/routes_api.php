<?php

use App\Modules\ServiceProvider\Bookings\Controllers\BookingController;

Route::group(['prefix' => 'provider/bookings', 'middleware' => ['jwt', 'access.control']], function () {
    Route::get('/', [BookingController::class, 'index'])->name('provider.booking.index');
});
