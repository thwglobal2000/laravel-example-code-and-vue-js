<?php

namespace App\Modules\ServiceProvider\Personnel\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Specialization\Specialization;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class SpecializationController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param Request $request
     * @return Collection
     */
    public function __invoke(Request $request): Collection
    {
        return Specialization::with('serviceType:id,service_name')
            ->orderBy('specializations.name')
            ->get()
            ->map(function (Specialization $specialization) {
                return [
                    'id' => $specialization->id,
                    'name' => $specialization->name,
                    'service_type' => $specialization->serviceType->service_name ?? '',
                ];
            });
    }
}
