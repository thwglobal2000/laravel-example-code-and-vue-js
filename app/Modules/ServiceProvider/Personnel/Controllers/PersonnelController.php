<?php

namespace App\Modules\ServiceProvider\Personnel\Controllers;

use App\Http\Controllers\Controller;
use App\Libraries\Utils\Utils;
use App\Mail\NewPersonnelEmail;
use App\Models\Contact\Contact;
use App\Models\PersonnelSpecialization\PersonnelSpecialization;
use App\Models\User\User;
use App\Modules\ServiceProvider\Personnel\Requests\PersonnelIndexRequest;
use App\Modules\ServiceProvider\Personnel\Requests\PersonnelRequest;
use Exception;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Throwable;

class PersonnelController extends Controller
{
    /**
     * @param PersonnelIndexRequest $request
     *
     * @return LengthAwarePaginator
     * @noinspection PhpUndefinedFieldInspection
     */
    public function index(PersonnelIndexRequest $request): LengthAwarePaginator
    {
        $page = $request->input('pagination.page', 1);
        $pageSize = $request->input('pagination.pageSize', 10);
        $term = $request->term;

        $serviceProviderId = Utils::getCurrentUser()->contact->id;

        $personnel = Contact
            ::with(
                'companies:id',
                'user:id,email',
                'user.roles:id,display_name',
                'specializations:id,name',
            )
            ->whereHas('companies', function (Builder $query) use ($serviceProviderId) {
                $query->where('admin_id', $serviceProviderId);
            })
            ->where('id', '!=', $serviceProviderId)
            ->when(
                $request->sort ?? false,
                fn($query, $value) => $query->orderBy($value['column'], $value['direction'])
            )
            ->when(
                $term,
                fn($query, $value) => $query
                    ->where('first_name', 'like', "%$value%")
                    ->orWhere('last_name', 'like', "%$value%")
                    ->orWhere('email', 'like', "%$value%")
                    ->orWhere('mobile_phone_number', 'like', "%$value%")
            )
            ->paginate($pageSize, ['*'], 'page', $page);

        $personnel
            ->getCollection()
            ->transform(function (Contact $item) {
                return [
                    'companies' => $item->companies->pluck('id')
                ] + $item->toArray();
            });

        return $personnel;
    }

    /**
     * @param PersonnelRequest $request
     * @return JsonResponse
     * @throws Throwable
     * @noinspection PhpUndefinedFieldInspection
     */
    public function store(PersonnelRequest $request): JsonResponse
    {
        DB::transaction(function () use ($request) {

            $specializations = $request->specializations;
            $contactData = $request->except('login_email', 'login_password', 'specializations');
            $userData = [
                'name' => $request->first_name . ' ' . $request->last_name,
                'email' => $request->login_email,
                'password' => $request->login_password,
                'phone' => $request->mobile_phone_number,
            ];

            // 1 create user
            $user = User::create($userData);
            // 2 give rights to the user
            $user->attachRole('service-provider');
            $contactData['user_id'] = $user->id;
            // 3 create a contact for this user
            $contact = Contact::create($contactData);
            // 4 add companies to this user
            $contact->companies()->attach($request->companies);
            // 5 add specialization to this user
            foreach ($specializations as $specialization) {
                PersonnelSpecialization::create([
                    'personnel_id' => $contact->id,
                    'specialization_id' => $specialization['id'],
                    'ahpra_registration_number' => $specialization['ahpra_registration_number']
                ]);
            }
            // 6 send a message to the address of this user
            $company_names = implode(', ', $contact->companies()->pluck('business_name')->toArray());
            Mail::to($user->email)
                ->send(new NewPersonnelEmail($company_names, [
                    'name' => $user->name,
                    'email' => $user->email,
                    'password' => $request->login_password,
                ]));
        });

        return response()->json([], Response::HTTP_CREATED);
    }

    /**
     * @param Contact $personnel
     * @param PersonnelRequest $request
     * @return JsonResponse
     * @throws Throwable
     * @noinspection PhpUndefinedFieldInspection
     */
    public function update(Contact $personnel, PersonnelRequest $request): JsonResponse
    {
        DB::transaction(function () use ($personnel, $request) {
            $user = $personnel->user;

            $specializations = $request->specializations;
            $contactData = $request->only([
                'first_name',
                'last_name',
                'date_of_birth',
                'mobile_phone_number',
                'home_phone_number',
                'email',
                'unit_suit_number',
                'street_type',
                'street_number',
                'street_name',
                'state',
                'postcode',
                'country',
                'ndis_number',
            ]);
            $userData = [
                'name' => $request->first_name . ' ' . $request->last_name,
                'email' => $request->login_email,
                'phone' => $request->mobile_phone_number,
            ];

            if ($request->filled('login_password')) {
                $userData['password'] = $request->login_password;
            }

            $personnel->update($contactData);
            $user->update($userData);

            // update company personnel
            $personnel->companies()->sync($request->companies);

            $personnel->specializations()->sync([], true);
            foreach ($specializations as $specialization) {
                PersonnelSpecialization::create([
                    'personnel_id' => $personnel->id,
                    'specialization_id' => $specialization['id'],
                    'ahpra_registration_number' => $specialization['ahpra_registration_number']
                ]);
            }
        });

        return response()->json([]);
    }

    /**
     * @param Contact $personnel
     * @return JsonResponse
     * @throws Exception
     */
    public function destroy(Contact $personnel): JsonResponse
    {
        $user = $personnel->user;
        $user->delete();
        $personnel->delete();

        return response()->json(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * Return companies where is the current contact is an admin
     *
     * @return Response
     */
    public function getMyCompanies(): Response
    {
        $companies = Utils::getCurrentUser()->contact
            ->ownedCompanies()
            ->orderBy('business_name')
            ->get(['id', 'business_name']);

        return response($companies);
    }
}
