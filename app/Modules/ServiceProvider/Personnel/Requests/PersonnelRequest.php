<?php

namespace App\Modules\ServiceProvider\Personnel\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class PersonnelRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     * @noinspection PhpUndefinedFieldInspection
     */
    public function rules(): array
    {
        return [
            'first_name' => ['required', 'string'],
            'last_name' => ['required', 'string'],
            'date_of_birth' => ['required', 'date_format:' . config('date.carbonDate'), 'before_or_equal:today'],
            'mobile_phone_number' => ['required', 'phone:AU', Rule::unique('contacts')->ignore($this->id)],
            'home_phone_number' => ['nullable', 'phone:AU', Rule::unique('contacts')->ignore($this->id)],
            'email' => ['required', 'email', Rule::unique('contacts')->ignore($this->id)],
            'address' => ['nullable', 'string'],
            'postcode' => ['nullable', 'string'],
            'country' => ['nullable', 'string'],
            'ndis_number' => ['nullable', 'string'],

            'login_email' => ['required', 'email', Rule::unique('users', 'email')->ignore($this->user_id)],
            'login_password' => [Rule::requiredIf(!$this->id), 'min:6'],

            'specializations' => ['required', 'array', 'min:1'],
            'specializations.*.id' => ['required', 'distinct', 'exists:specializations,id'],
            'specializations.*.ahpra_registration_number' => ['required', 'string'],
            'companies' => ['required', 'array', 'min:1'],
        ];
    }

    public function attributes(): array
    {
        return [
            'specializations.*.id' => 'Specialization',
            'specializations.*.ahpra_registration_number' => 'AHPRA registration number',
        ];
    }
}
