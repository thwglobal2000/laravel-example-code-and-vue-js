<?php

Route::prefix('personnel')->middleware(['jwt', 'access.control'])->group(function () {
    Route::get('/', 'PersonnelController@index')->name('personnel.index');
    Route::post('/', 'PersonnelController@store')->name('personnel.store');
    Route::put('/{personnel}', 'PersonnelController@update')->name('personnel.update');
    Route::delete('/{personnel}', 'PersonnelController@destroy')->name('personnel.destroy');
    Route::get('/specializations', 'SpecializationController')->name('personnel.specializations');
    Route::get('/get-my-companies', 'PersonnelController@getMyCompanies')->name('companies.get-my-companies');
});
