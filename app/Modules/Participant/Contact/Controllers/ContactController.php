<?php

namespace App\Modules\Participant\Contact\Controllers;

use App\Http\Controllers\Controller;
use App\Libraries\Utils\Utils;
use App\Mail\NewLegalRepresentative;
use App\Models\Company\Company;
use App\Models\Contact\Contact;
use App\Models\Invoice\Invoice;
use App\Models\LegalRepresentative\LegalRepresentative;
use App\Models\LegalRepresentativeVerifyCode\LegalRepresentativeVerifyCode;
use App\Models\User\User;
use App\Modules\Participant\Contact\Requests\ContactRequest;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Laravel\Cashier\Cashier;
use Laravel\Cashier\Exceptions\IncompletePayment;
use Stripe\Exception\ApiErrorException;
use Stripe\Invoice as StripeInvoice;
use Stripe\Subscription;
use Symfony\Component\HttpFoundation\Response;
use Throwable;

class ContactController extends Controller
{
    private const STRIPE_INVOICE_DAYS_UNTIL_DUE = 5;

    /**
     * @param ContactRequest $request
     * @return JsonResponse
     * @noinspection PhpUndefinedFieldInspection
     * @throws Throwable
     */
    public function store(ContactRequest $request): JsonResponse
    {
        $contact = DB::transaction(function () use ($request) {
            $user = Utils::getCurrentUser();
            $data['user_id'] = $user->id;
            $data['mobile_phone_number'] = $user->phone;
            $data += $request->except(['legal_representative']);

            $contact = Contact::create($data);

            if ($request->filled('legal_representative')) {
                $legalRepresentativeData = $request->legal_representative;
                $legalRepresentativeUser = User::create([
                    'name' => "{$legalRepresentativeData['first_name']} {$legalRepresentativeData['last_name']}",
                    'email' => $legalRepresentativeData['email'],
                    'email_verified_at' => now(),
                    'phone' => $legalRepresentativeData['mobile_phone_number'],
                    'password' => Str::random(),
                ]);
                $legalRepresentativeUser->syncRoles(['representative']);

                LegalRepresentative::create([
                    'participant_id' => $user->id,
                    'representative_id' => $legalRepresentativeUser->id,
                    'representative_type' => $legalRepresentativeData['type'],
                ]);

                $legalRepresentativeVerifyCode = LegalRepresentativeVerifyCode::create([
                    'representative_id' => $legalRepresentativeUser->id,
                    'code' => Str::random(32),
                ]);

                Mail::to($legalRepresentativeUser->email)
                    ->send(new NewLegalRepresentative(
                        $legalRepresentativeUser,
                        $legalRepresentativeVerifyCode->code
                    ));
            }

            if ($contact->planManager) {
                $this->sendInvoiceToPlanManager($contact->planManager, $request->plan_id);
            }

            return $contact;
        });

        return response()->json(['contact' => $contact]);
    }

    /**
     * @param Company $plan_manager
     * @param string $plan_id
     *
     * @throws ApiErrorException
     * @throws IncompletePayment
     */
    private function sendInvoiceToPlanManager(Company $plan_manager, string $plan_id): void
    {
        if (!$user = Utils::getCurrentUser()) {
            abort(Response::HTTP_UNPROCESSABLE_ENTITY, 'The current user is not determined');
        }

        $customerDescription = "Participant: $user->name, $user->email";

        $user->createOrGetStripeCustomer();
        $user->updateStripeCustomer([
            'email' => $plan_manager->email,
            'name' => $plan_manager->business_name,
            'address' => [
                'line1' => str_replace(["\r\n", "\n", "\r"], ' ', $plan_manager->address)
            ],
            'phone' => $plan_manager->phone_number,
            'description' => $customerDescription,
        ]);

        $metadata = [
            'participant_id' => $user->id,
            'participant_name' => $user->name,
            'participant_email' => $user->email,
        ];

        $subscription = $user
            ->newSubscription('default', $plan_id)
            ->create(null, [], [
                'collection_method' => StripeInvoice::BILLING_SEND_INVOICE,
                'payment_behavior' => Subscription::PAYMENT_BEHAVIOR_ALLOW_INCOMPLETE,
                'days_until_due' => self::STRIPE_INVOICE_DAYS_UNTIL_DUE,
                'metadata' => $metadata,
            ]);

        if ($invoice = $subscription->latestInvoice()) {
            Cashier::stripe()->invoices->update($invoice->id, [
                'metadata' => $metadata,
                'description' => $customerDescription,
            ]);
            $invoice->send();

            Invoice::create([
                'company_id' => $plan_manager->id,
                'invoice_number' => $invoice->number,
                'stripe_invoice_id' => $invoice->id,
                'invoice_date' => Carbon::parse($invoice->created),
                'total_price' => $invoice->total,
                'gst' => $invoice->tax ?? 0,
            ]);
        }
    }
}
