<?php

namespace App\Modules\Participant\Contact\Requests;

use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     * @noinspection PhpUndefinedFieldInspection
     */
    public function rules(): array
    {
        $dateOfBirth = Carbon::createFromFormat(config('date.carbonDate'), $this->date_of_birth);
        $isParticipantChild = $dateOfBirth->diffInYears(now()) < 18;

        return [
            'first_name' => ['required', 'string'],
            'last_name' => ['required', 'string'],
            'email' => ['required', 'email'],
            'ndis_number' => ['required', 'string'],
            'plan_type' => ['required', 'in:0,1,2'],
            'plan_manager_id' => ['required_if:plan_type,0'],
            'address' => ['nullable', 'string'],
            'lat' => ['nullable', 'numeric'],
            'lng' => ['nullable', 'numeric'],
            'postcode' => ['nullable', 'string'],
            'country' => ['nullable', 'string'],
            'date_of_birth' => ['required', 'date_format:' . config('date.carbonDate'), 'before_or_equal:today'],

            'legal_representative' => [Rule::requiredIf($isParticipantChild), 'array'],
            'legal_representative.first_name' => [Rule::requiredIf($this->filled('legal_representative')), 'string'],
            'legal_representative.last_name' => [Rule::requiredIf($this->filled('legal_representative')), 'string'],
            'legal_representative.email' => [
                Rule::requiredIf($this->filled('legal_representative')),
                'email',
                'unique:users,email'
            ],
            'legal_representative.mobile_phone_number' => [
                Rule::requiredIf($this->filled('legal_representative')),
                'phone:AU',
                'unique:users,phone'
            ],
            'legal_representative.type' => [
                Rule::requiredIf($this->filled('legal_representative')),
                'string'
            ],
        ];
    }

    public function messages(): array
    {
        return [
            'user_id.exists' => 'The user is not exists',
            'date_of_birth.date_format' => 'Wrong date format',
            'plan_manager_id.required_if' => 'Plan manager required'
        ];
    }
}
