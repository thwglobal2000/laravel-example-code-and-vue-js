<?php

Route::prefix('participant/contact')->middleware(['jwt', 'access.control'])->group(function () {
    Route::post('/', 'ContactController@store')->name('participant.contact.store');
});
