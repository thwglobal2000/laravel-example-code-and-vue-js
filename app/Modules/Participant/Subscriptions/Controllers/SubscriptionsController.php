<?php

namespace App\Modules\Participant\Subscriptions\Controllers;

use App\Http\Controllers\Controller;
use App\Libraries\Utils\Utils;
use App\Modules\Participant\Subscriptions\Constants\SubscriptionsConstants;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;
use Laravel\Cashier\Cashier;
use Laravel\Cashier\Exceptions\IncompletePayment;
use Symfony\Component\HttpFoundation\Response;

class SubscriptionsController extends Controller
{
    private const PLANS_CACHE_NAME = 'stripe_plans';
    private const PLANS_CACHE_TTL = 5 * 60; //5 minute

    /**
     * Proceed REST request of getting available plans from Stripe
     *
     * @return JsonResponse
     */
    public function getPlans(): JsonResponse
    {
        try {
            $plans = $this->fetchStripePlans();
        } catch (Exception $e) {
            Log::error($e);
            abort(Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
        }

        return response()->json($plans);
    }

    /**
     * Fetching plans using Cache for the production mode
     *
     * @return Collection
     */
    private function fetchStripePlans(): Collection
    {
        /* Make negative TTL in case of non-production mode, this will avoid caching */
        $ttl = app()->environment('production') ? self::PLANS_CACHE_TTL : -1;

        return Cache::remember(self::PLANS_CACHE_NAME, $ttl, function () {
            $plans = Cashier::stripe()->plans->all(['active' => true])->toArray();

            return collect($plans['data'] ?? [])
                ->sortBy('product')
                ->sortBy('amount')
                ->values();
        });
    }

    /**
     * Sync user subscriptions for the giving subscription id
     *
     * @param $id
     *
     * @return JsonResponse
     */
    public function resyncSubscriptionStatus($id): JsonResponse
    {
        if (!$user = Utils::getCurrentUser()) {
            abort(Response::HTTP_UNPROCESSABLE_ENTITY, 'The current user is not determined');
        }

        if (!$subscription = $user->subscriptions()->find($id)) {
            abort(Response::HTTP_UNPROCESSABLE_ENTITY, 'User subscription not found');
        }
        $subscription->syncStripeStatus();

        return response()->json(['message' => 'Subscription status successfully updated']);
    }

    /**
     * Proceed user subscription. In case of 3DS enabled card, it will raise Exception
     * which will force frontend to confirm entered card
     *
     * @throws ValidationException
     */
    public function pay(Request $request): JsonResponse
    {
        $this->validate($request, [
            'plan_id'        => 'required|string',
            'payment_method' => 'required|string',
        ]);

        if (!$user = Utils::getCurrentUser()) {
            abort(Response::HTTP_UNPROCESSABLE_ENTITY, 'The current user is not determined');
        }

        $user->createOrGetStripeCustomer();

        try {
            $user
                ->newSubscription(SubscriptionsConstants::SUBSCRIPTION_NAME, $request->plan_id)
                ->create($request->payment_method);
        } catch (IncompletePayment $e) {
            if (!$last_subscription = $user->subscriptions()->latest()->first()) {
                abort(Response::HTTP_UNPROCESSABLE_ENTITY, 'User subscription not found');
            }

            return response()->json([
                'client_secret'   => $e->payment->clientSecret(),
                'subscription_id' => $last_subscription->id,
                'message'         => $e->getMessage(),
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        } catch (Exception $e) {
            Log::error($e);
            abort(Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
        }

        return response()->json([
            'message'    => 'Subscribed successfully',
            'subscribed' => $user->subscribed(SubscriptionsConstants::SUBSCRIPTION_NAME),
        ]);
    }
}
