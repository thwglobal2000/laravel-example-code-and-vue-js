<?php

namespace App\Modules\Participant\Subscriptions\Constants;

class SubscriptionsConstants
{
    public const SUBSCRIPTION_NAME = 'default';
    public const SUPPRESS_CHECK_SUBSCRIPTION = 'suppress-check-subscription';
}
