<?php

Route::prefix('participant/subscriptions')->middleware(['jwt', 'access.control'])->group(function () {
    Route::get('/plans', 'SubscriptionsController@getPlans')->name('participant.subscription.plans');
    Route::get('/resync-status/{id}', 'SubscriptionsController@resyncSubscriptionStatus')->name('participant.subscription.resync-status');
    Route::post('/pay', 'SubscriptionsController@pay')->name('participant.subscription.pay');
});
