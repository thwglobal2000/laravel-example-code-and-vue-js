<?php

namespace App\Modules\_common_\AccountSettings\Controllers;

use App\Http\Controllers\Controller;
use App\Models\PersonnelSpecialization\PersonnelSpecialization;
use App\Models\Specialization\Specialization;
use App\Modules\_common_\AccountSettings\Requests\ChangePasswordRequest;
use App\Modules\_common_\AccountSettings\Requests\UpdateUserRequest;
use App\Modules\_common_\AccountSettings\Requests\UpdateUserSpecializationsRequest;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class AccountSettingsController extends Controller
{
    public function index()
    {
    }

    /**
     * @param UpdateUserRequest $request
     * @return JsonResponse
     */
    public function update(UpdateUserRequest $request): JsonResponse
    {
        $user = Auth::user();
        $data = $request->except('file', '_method');

        if ($request->hasFile('file')) {
            $avatar = $request->file('file');
            $data['avatar'] = Storage::url($avatar->storePublicly("public/users/{$user->id}"));
        }

        $user->update($data);

        return response()->json([
            'message' => 'Successfully saved',
        ]);
    }

    /**
     * @param ChangePasswordRequest $request
     * @return JsonResponse
     * @noinspection PhpUndefinedFieldInspection
     * @throws Exception
     */
    public function changePassword(ChangePasswordRequest $request): JsonResponse
    {
        $user = Auth::user();

        $newPassword = $request->password;
        $user->update(['password' => $newPassword]);

        return response()->json([
            'message' => 'Password successfully updated',
        ]);
    }

    /**
     * @return JsonResponse
     */
    public function specializations(): JsonResponse
    {
        $user = Auth::user();

        $userSpecializations = PersonnelSpecialization::with('specialization')
            ->where('personnel_id', $user->id)
            ->get();

        $specializationsList = Specialization::orderBy('name')
            ->pluck('name', 'id');

        return response()->json([
            'specializationsList' => $specializationsList,
            'userSpecializations' => $userSpecializations,
        ]);
    }

    /**
     * @param UpdateUserSpecializationsRequest $request
     * @return JsonResponse
     * @noinspection PhpUndefinedFieldInspection
     * @throws Exception
     */
    public function editSpecializations(UpdateUserSpecializationsRequest $request): JsonResponse
    {
        $user = Auth::user();
        $contact = $user->contact;

        if (!$contact) {
            throw new Exception('You do not have a contact yet. Create it firstly');
        }

        $data = $request->specializations;

        $user->specializations()->sync([]);

        foreach ($data as $datum) {
            PersonnelSpecialization::create([
                'personnel_id' => $contact->id,
                'specialization_id' => $datum['specialization_id'],
                'license_number' => $datum['license_number'],
            ]);
        }

        return response()->json([
            'data' => PersonnelSpecialization::with('specialization')
                ->where('personnel_id', $user->id)
                ->get(),
        ]);
    }
}
