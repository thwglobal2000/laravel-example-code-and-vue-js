<?php

namespace App\Modules\_common_\AccountSettings\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => ['required', 'string'],
            'email' => ['required', 'email', Rule::unique('users')->ignore(Auth::id())],
            'phone' => ['required', 'phone:AU', Rule::unique('users')->ignore(Auth::id())],
            'file' => ['sometimes', 'image', 'max:2048'],
        ];
    }
}
