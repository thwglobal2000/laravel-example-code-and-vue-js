<?php

namespace App\Modules\_common_\AccountSettings\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserSpecializationsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'specializations' => ['required', 'array'],
            'specializations.*.specialization_id' => ['required', 'exists:specializations,id'],
            'specializations.*.license_number' => ['required'],
        ];
    }
}
