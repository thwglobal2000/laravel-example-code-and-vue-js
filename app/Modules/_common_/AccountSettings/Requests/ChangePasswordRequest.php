<?php

namespace App\Modules\_common_\AccountSettings\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ChangePasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'current_password' => ['required', 'password'],
            'password' => ['required', 'confirmed', 'min:6']
        ];
    }

    public function messages(): array
    {
        return [
            'current_password.password' => 'Current password is invalid',
        ];
    }
}
