<?php

Route::prefix('account-settings')->middleware(['jwt', 'access.control'])->group(function () {
    Route::get('/', 'AccountSettingsController@index')->name('account-settings.index');
    Route::post('/change-password', 'AccountSettingsController@changePassword')
        ->name('account-settings.change-password');
    Route::put('/update', 'AccountSettingsController@update')->name('account-settings.update');
    Route::post('/edit-specializations', 'AccountSettingsController@editSpecializations')
        ->name('account-settings.edit-specializations');
    Route::get('/specializations', 'AccountSettingsController@specializations')
        ->name('account-settings.specializations');
});
