<?php

namespace App\Modules\_common_\Booking\Controllers;

use App\Http\Controllers\Controller;
use App\Libraries\Utils\Utils;
use App\Models\BookingRequest\BookingRequest;
use App\Models\Company\Company;
use App\Services\Booking\BookingService;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class BookingRequestController
 *
 * Accept or deny booking by service provider
 *
 * @package App\Modules\_common_\Booking\Controllers
 */
class BookingRequestController extends Controller
{
    /**
     * Booking service for sending notifications
     *
     * @var BookingService
     */
    protected BookingService $booking;

    /**
     * Create a new job instance.
     *
     * @throws BindingResolutionException
     */
    public function __construct()
    {
        $this->booking = app()->make(BookingService::class);
    }

    /**
     * @param BookingRequest $booking
     * @return JsonResponse
     */
    public function store(BookingRequest $booking, string $providerHash): JsonResponse
    {
        $provider = Company::findOrFail(Utils::decodeId($providerHash));

        abort_unless(
            Utils::getCurrentUser()->contact->id === $provider->admin_id,
            Response::HTTP_UNPROCESSABLE_ENTITY,
            'Current user is not associated with the company'
        );

        $this->booking->confirmRequest($provider, $booking);

        return response()->json([
            'message' => 'Booking confirmed',
        ]);
    }

    /**
     * @param BookingRequest $booking
     * @return JsonResponse
     */
    public function destroy(BookingRequest $booking, string $providerHash): JsonResponse
    {
        $provider = Company::findOrFail(Utils::decodeId($providerHash));

        abort_unless(
            Utils::getCurrentUser()->contact->id === $provider->admin_id,
            Response::HTTP_UNPROCESSABLE_ENTITY,
            'Current user is not associated with the company'
        );

        $this->booking->denyRequest($provider, $booking);

        return response()->json([
            'message' => 'Booking denied',
        ]);
    }
}
