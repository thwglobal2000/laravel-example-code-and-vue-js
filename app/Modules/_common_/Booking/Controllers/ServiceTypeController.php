<?php

namespace App\Modules\_common_\Booking\Controllers;

use App\Http\Controllers\Controller;
use App\Libraries\Utils\Utils;
use App\Models\ServiceType\ServiceType;
use App\Services\Booking\BookingService;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class ServiceTypeController extends Controller
{
    private const UPCOMING_SERVICES_CHECK_PERIOD_IN_DAYS = 15;

    public function index(Request $request): JsonResponse
    {
        $services = ServiceType
            ::when($request->relations, fn (Builder $query): Builder => $query->with($request->relations))
            ->orderBy('service_name')
            ->get();

        return Response::json([
            'services' => $services,
        ]);
    }

    public function show(): JsonResponse
    {
        $bookings = Utils
            ::getCurrentUser()
            ->bookings()
            ->confirmed()
            ->with('serviceType')
            ->where('end_date', '>=', today())
            ->get();

        $now = now();

        $service = (new BookingService)
            ->getAppointmentsWithinDates($bookings, $now, $now->clone()->addDays(self::UPCOMING_SERVICES_CHECK_PERIOD_IN_DAYS))
            ->sortBy(fn(array $appointment): string => $appointment['start'])
            ->first(fn(array $appointment): bool => Carbon
                ::parse($appointment['start'])
                ->greaterThan(now())
            );

        return Response::json([
            'service' => $service ?? null,
        ]);
    }
}
