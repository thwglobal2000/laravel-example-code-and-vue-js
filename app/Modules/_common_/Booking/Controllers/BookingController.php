<?php

namespace App\Modules\_common_\Booking\Controllers;

use App\Http\Controllers\Controller;
use App\Libraries\Utils\Utils;
use App\Models\Booking\Booking;
use App\Models\Company\Company;
use App\Models\ServiceType\ServiceType;
use App\Models\Specialization\Specialization;
use App\Modules\_common_\Booking\Requests\BookingRequest;
use App\Services\Booking\BookingService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Throwable;

class BookingController extends Controller
{
    protected BookingService $booking;

    public function index(Request $request): JsonResponse
    {
        [$column, $order] = explode(',', $request->input('sortBy', 'id,desc'));

        $query = Booking
            ::with(['serviceType'])
            ->orderBy($column, $order);

        return response()->json(
            $request->pageSize ? $query->paginate($request->pageSize) : $query->get()
        );
    }

    /**
     * @param BookingRequest $request
     * @return JsonResponse
     * @throws Throwable
     */
    public function store(BookingRequest $request, BookingService $booking): JsonResponse
    {
        $booking
            ->setParticipant(Utils::getCurrentUser())
            ->setServiceType(ServiceType::whereId($request->service_type_id)->firstOrFail())
            ->setSpecialization(Specialization::whereId($request->specialization_id)->firstOrFail());

        $this->booking = $booking;

        $bookingPayload = $request->validated();

        try {
            if (in_array($bookingPayload['visit_type'], [Company::VISIT_TYPE_CLINIC, Company::VISIT_TYPE_HOMECARE], true)) {
                if ($participant = Utils::getCurrentUser()) {
                    $bookingPayload['lat'] = $bookingPayload['lat'] ?? $participant->contact->lat;
                    $bookingPayload['lng'] = $bookingPayload['lng'] ?? $participant->contact->lng;
                }

                if (!$bookingPayload['lng'] || !$participant->contact->address) {
                    throw new Exception('You need to set your address correctly to book this type of visit');
                }
            }

            $this->booking->sendRequest($bookingPayload);
        } catch (Exception $exception) {
            return response()->json(
                ['message' => $exception->getMessage()],
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }

        return response()->json([
            'message' => 'Booking request created',
        ]);
    }
}
