<?php

namespace App\Modules\_common_\Booking\Requests;

use App\Models\Booking\Booking;
use App\Models\Company\Company;
use App\Models\Specialization\Specialization;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class BookingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     * @noinspection PhpUndefinedFieldInspection
     */
    public function rules(): array
    {
        return [
            'service_type_id' => ['required', 'exists:service_types,id'],
            'specialization_id' => ['required', 'exists:' . Specialization::TABLE_NAME . ',id'],
            'start_date' => ['required', 'date_format:' . config('date.carbonDate'), 'after_or_equal:today'],
            'end_date' => ['required', 'date_format:' . config('date.carbonDate'), 'after:start_date'],
            'start_time' => ['required', 'date_format:H:i'],
            'end_time' => ['required', 'date_format:H:i'],
            'frequency' => ['required', Rule::in(array_keys(Booking::FREQUENCY_TYPES))],
            'visit_type' => ['required', Rule::in(array_keys(Company::VISIT_TYPES))],
            'address' => ['sometimes', 'string', 'nullable'],
            'lat' => [Rule::requiredIf((bool)$this->address), 'numeric', 'nullable'],
            'lng' => [Rule::requiredIf((bool)$this->address), 'numeric', 'nullable'],
            'radius' => [Rule::requiredIf($this->visit_type === Company::VISIT_TYPE_CLINIC), 'integer', 'nullable'],
        ];
    }
}
