<?php

Route::group(['prefix' => 'booking-request', 'middleware' => ['jwt', 'access.control']], function () {
    Route::get('/{booking}/{providerHash}/confirm', 'BookingRequestController@store')->name('booking-requests.confirm');
    Route::get('/{booking}/{providerHash}/deny', 'BookingRequestController@destroy')->name('booking-requests.deny');
});

Route::group(['prefix' => 'booking', 'middleware' => ['jwt', 'access.control']], function () {
    Route::get('/', 'BookingController@index')->name('booking.index');
    Route::post('/', 'BookingController@store')->name('booking.store');

    Route::get('/services', 'ServiceTypeController@index')->name('booking.services');
    Route::get('/upcoming', 'ServiceTypeController@show')->name('booking.upcoming');
});
