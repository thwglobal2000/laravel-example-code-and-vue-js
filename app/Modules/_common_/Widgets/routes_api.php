<?php

Route::group(['prefix' => 'widgets', 'middleware' => ['jwt', 'access.control']], function () {
    Route::get('/', 'WidgetsController@index')->name('widgets.index');
    Route::put('/update-layout', 'WidgetsController@updateLayout')->name('widgets.update-layout');
    Route::put('/visibility', 'WidgetsController@setVisibility')->name('widgets.visibility');
    Route::put('/static', 'WidgetsController@setStatic')->name('widgets.static');
    Route::delete('destroy', 'WidgetsController@destroy')->name('widgets.delete');

    Route::post('/calendar', 'CalendarController@index')->name('widgets.calendar');
});
