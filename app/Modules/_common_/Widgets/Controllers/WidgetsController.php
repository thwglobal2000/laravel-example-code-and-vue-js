<?php

namespace App\Modules\_common_\Widgets\Controllers;

use App\Http\Controllers\Controller;
use App\Libraries\Utils\Utils;
use App\Models\WidgetSetting\WidgetSetting;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

class WidgetsController extends Controller
{
    /**
     * @return JsonResponse
     * @throws \Exception
     */
    public function index()
    {
        $config_widgets = config('widgets.dashboard_widgets.draggable');

        $widgets = collect($config_widgets)->whereIn('permission', $this->getPermissions());

        $current_user = Utils::getCurrentUserId();

        $query = WidgetSetting::where('user_id', $current_user)->get();

        if (count($widgets) > count($query)) {
            $defaultWidgetNames = $widgets->keys()->toArray();

            $userWidgetNames = $query->pluck('widget_name')->toArray();
            $diffWidgetNames = array_diff($defaultWidgetNames, $userWidgetNames);
            foreach ($diffWidgetNames as $name) {
                $w = $widgets[$name];

                $settings = new WidgetSetting();
                $settings->fill([
                    'i'                 => $w['i'],
                    'x'                 => $w['x'],
                    'y'                 => $w['y'],
                    'w'                 => $w['w'],
                    'h'                 => $w['h'],
                    'widget_name'       => $name,
                    'widget_header'     => $w['header'],
                    'widget_permission' => $w['permission'],
                    'user_id'           => $current_user
                ])->save();
            }
        } elseif (count($widgets) < count($query)) {
            $defaultWidgetNames = $widgets->keys()->toArray();

            $userWidgetNames = $query->pluck('widget_name')->toArray();
            $diffWidgetNames = array_diff($userWidgetNames, $defaultWidgetNames);
            WidgetSetting
                ::where('user_id', $current_user)
                ->whereIn('widget_name', $diffWidgetNames)
                ->delete();
        }

        [$hiddenWidgets, $visibleWidgets, $staticWidgets] = $this->getWidgets();

        return response()->json(compact('visibleWidgets', 'hiddenWidgets', 'staticWidgets'));
    }

    /**
     * @return Collection
     */
    public function getPermissions()
    {
        return collect(Utils::getCurrentUser()->allPermissions())
            ->filter(function ($permission) {
                return Str::startsWith($permission->name, 'widgets');
            })
            ->pluck('name');
    }

    /**
     * @return array
     */
    public function getWidgets()
    {
        $config_widgets = config('widgets.dashboard_widgets.draggable');
        $staticWidgets  = collect(config('widgets.dashboard_widgets.static'))
            ->whereIn('permission', $this->getPermissions())
            ->values();

        $widgets = collect($config_widgets)->whereIn('permission', $this->getPermissions());

        $widgets = WidgetSetting
            ::where('user_id', Utils::getCurrentUserId())
            ->whereIn('widget_name', $widgets->keys())
            ->where('is_deleted', 0)
            ->get();

        $hiddenWidgets = $widgets->filter(function ($value) {
            return !$value->is_visible;
        })->values();

        $visibleWidgets = $widgets->filter(function ($value) {
            return (bool)$value->is_visible;
        })->values();

        return [$hiddenWidgets, $visibleWidgets, $staticWidgets];
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function updateLayout(Request $request)
    {
        $items = $request->all();
        foreach ($items as $item) {
            $widget = WidgetSetting
                ::where('i', $item['i'])
                ->where('user_id', Utils::getCurrentUserId())
                ->first();

            $widget->fill([
                'i' => $item['i'],
                'x' => $item['x'],
                'y' => $item['y'],
                'w' => $item['w'],
                'h' => $item['h'],
            ])->save();
        }

        [$hiddenWidgets, $visibleWidgets, $staticWidgets] = $this->getWidgets();

        return response()->json(compact('visibleWidgets', 'hiddenWidgets', 'staticWidgets'));
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function setVisibility(Request $request)
    {
        $widget = WidgetSetting
            ::where('i', $request->i)
            ->where('user_id', Utils::getCurrentUserId())
            ->first();

        $widget->is_visible = !(boolean)$widget->is_visible;
        $widget->save();

        [$hiddenWidgets, $visibleWidgets, $staticWidgets] = $this->getWidgets();

        return response()->json(compact('visibleWidgets', 'hiddenWidgets', 'staticWidgets'));
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function setStatic(Request $request)
    {
        $widget = WidgetSetting::where('i', $request->i)->first();

        $widget->is_static = !(boolean)$widget->is_static;
        $widget->save();

        [$hiddenWidgets, $visibleWidgets, $staticWidgets] = $this->getWidgets();

        return response()->json(compact('visibleWidgets', 'hiddenWidgets', 'staticWidgets'));
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function destroy(Request $request)
    {
        WidgetSetting
            ::where('i', $request->i)
            ->where('user_id', Utils::getCurrentUserId())
            ->update(['is_deleted' => true]);

        [$hiddenWidgets, $visibleWidgets, $staticWidgets] = $this->getWidgets();

        return response()->json(compact('visibleWidgets', 'hiddenWidgets', 'staticWidgets'));
    }
}
