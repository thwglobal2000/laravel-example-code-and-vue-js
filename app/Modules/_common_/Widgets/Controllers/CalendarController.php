<?php

namespace App\Modules\_common_\Widgets\Controllers;

use App\Http\Controllers\Controller;
use App\Libraries\Utils\Utils;
use App\Models\Booking\Booking;
use App\Modules\_common_\Booking\Requests\CalendarRequest;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\JsonResponse;

class CalendarController extends Controller
{
    /**
     * @param CalendarRequest $request
     * @return JsonResponse
     * @throws Exception
     * @noinspection PhpUndefinedFieldInspection
     */
    public function index(CalendarRequest $request): JsonResponse
    {
        $startDate = $request->start;
        $endDate = $request->end;

        if (!isset($startDate, $endDate)) {
            throw new Exception('Start/end date are invalid');
        }

        $startDate = Carbon::parse($startDate)->tz(config('app.timezone'))->startOfDay();
        $endDate = Carbon::parse($endDate)->tz(config('app.timezone'))->startOfDay();

        $events = Utils
            ::getCurrentUser()
            ->bookings()
            ->confirmed()
            ->with('serviceType:id,service_name')
            ->get()
            ->map(function (Booking $booking) use ($startDate, $endDate) {
                if ($booking->frequency == Booking::FREQUENCY_DAILY) {
                    $action = 'addDay';
                } elseif ($booking->frequency == Booking::FREQUENCY_WEEKLY) {
                    $action = 'addWeek';
                } elseif ($booking->frequency == Booking::FREQUENCY_FORTNIGHTLY) {
                    $action = 'addTwoWeeks';
                } else {
                    return [];
                }

                return $this->executionsBetweenDates($booking, $startDate, $endDate, $action);
            })
            ->flatten(1);


        return JsonResponse::create([
            'events' => $events,
        ]);
    }

    /**
     * @param Booking $booking
     * @param Carbon $start
     * @param Carbon $end
     * @param string $action
     * @return array
     */
    private function executionsBetweenDates(Booking $booking, Carbon $start, Carbon $end, string $action = 'addDay'): array
    {
        $bookingStart = $booking->start_date->startOfDay();
        $bookingEnd = $booking->end_date->startOfDay();
        $executions = [];

        while ($start->lessThanOrEqualTo($end)) {
            if ($bookingEnd->lessThan($start)) {
                break;
            } elseif ($start->lessThan($bookingStart)) {
                $start = $bookingStart;
                continue;
            }

            $executions[] = [
                'title' => $booking->serviceType->service_name,
                'start' => (clone $start)->setTimeFromTimeString($booking->start_time)->toIso8601String(),
                'end' => (clone $start)->setTimeFromTimeString($booking->end_time)->toIso8601String(),
            ];

            if ($action === 'addTwoWeeks') {
                $start->addWeeks(2);
            } else {
                $start->$action();
            }
        }

        return $executions;
    }
}
