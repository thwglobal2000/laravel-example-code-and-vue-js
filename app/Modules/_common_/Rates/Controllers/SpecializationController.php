<?php

namespace App\Modules\_common_\Rates\Controllers;

use App\Http\Controllers\Controller;
use App\Libraries\Utils\Utils;
use App\Models\Company\Company;
use App\Models\Specialization\Specialization;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Collection;

class SpecializationController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param Request $request
     *
     * @return ResponseFactory|JsonResponse|Response
     */
    public function __invoke(Request $request)
    {
        $user = Utils::getCurrentUser();

        $isSuperAdmin = $user->user_access_level === 0;

        if ($isSuperAdmin) {
            $specializations = Specialization::with('serviceType')->get();
        } else {
            $company = adminCompany();

            $companyContacts = $company
                ->contacts()
                ->with('specializations.serviceType')
                ->get();

            $specializations = $companyContacts
                ->pluck('specializations')
                ->flatten(1)
                ->unique('id')
                ->sortBy('name');
        }

        /** @var Collection $specializations */
        $specializations = $specializations
            ->map(fn(Specialization $specialization) => [
                'id' => $specialization->id,
                'name' => $specialization->name,
                'service_type' => [
                    'name' => $specialization->serviceType->service_name ?? 'Unknown service type',
                ],
            ])
            ->sortBy('name')
            ->sortBy('service_type.name');

        return response([
            'data' => $specializations->values()
        ]);
    }
}
