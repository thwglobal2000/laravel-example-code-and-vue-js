<?php

namespace App\Modules\_common_\Rates\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Company\Company;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CompanyController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param Request $request
     *
     * @return ResponseFactory|JsonResponse|Response
     */
    public function __invoke(Request $request)
    {
        $companies = Company::orderBy('business_name')->get(['id', 'business_name']);

        return response([
            'data' => $companies,
        ]);
    }
}
