<?php

namespace App\Modules\_common_\Rates\Controllers;

use App\Http\Controllers\Controller;
use App\Libraries\Utils\Utils;
use App\Models\ServiceProviderRate\ServiceProviderRate;
use App\Modules\_common_\Rates\Requests\RateIndexRequest;
use App\Modules\_common_\Rates\Requests\RateRequest;
use Carbon\Carbon;
use Exception;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class RateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param RateIndexRequest $request
     * @return LengthAwarePaginator
     * @noinspection PhpUndefinedFieldInspection
     */
    public function index(RateIndexRequest $request): LengthAwarePaginator
    {
        $term = $request->term;
        $orderBy = $request->input('sort.column', 'id');
        $direction = $request->input('sort.column', 'desc');
        $page = $request->input('pagination.page', 1);
        $pageSize = $request->input('pagination.pageSize', 10);

        $isSuperAdmin = Utils::getCurrentUser()->user_access_level === 0;

        return ServiceProviderRate
            ::with('specialization.serviceType')
            ->when(!$isSuperAdmin, fn(Builder $q) => $q->where('company_id', adminCompany()->id))
            ->when($isSuperAdmin, fn(Builder $q) => $q->with('company'))
            ->when($term, function (Builder $q, $value) {
                return $q->whereHas('specialization', function (Builder $q) use ($value) {
                    return $q
                        ->where('name', 'like', "%$value%")
                        ->orWhereHas('serviceType', fn(Builder $q) => $q->where('service_name', 'like', "%$value%"));
                });
            })
            ->orderBy($orderBy, $direction)
            ->paginate($pageSize, ['*'], 'page', $page);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param RateRequest $request
     * @return JsonResponse
     * @noinspection PhpUndefinedFieldInspection
     * @noinspection DuplicatedCode
     */
    public function store(RateRequest $request): JsonResponse
    {
        $data = $request->only('specialization_id', 'rate_date', 'rate_amount');
        $data['rate_date'] = Carbon::createFromFormat(config('date.carbonDate'), $data['rate_date']);

        $user = Utils::getCurrentUser();

        if ($user->user_access_level === 0) {
            $data['company_id'] = $request->company_id;
        } else {
            $company = adminCompany();
            $data['company_id'] = $company->id;
        }

        $rate = ServiceProviderRate::create($data);

        return response()->json(['rate' => $rate], Response::HTTP_CREATED);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ServiceProviderRate $rate
     * @param RateRequest $request
     * @return JsonResponse
     * @noinspection PhpUndefinedFieldInspection
     * @noinspection DuplicatedCode
     */
    public function update(ServiceProviderRate $rate, RateRequest $request): JsonResponse
    {
        $data = $request->only('specialization_id', 'rate_date', 'rate_amount');
        $data['rate_date'] = Carbon::createFromFormat(config('date.carbonDate'), $data['rate_date']);

        $user = Utils::getCurrentUser();

        if ($user->user_access_level === 0) {
            $data['company_id'] = $request->company_id;
        } else {
            $company = adminCompany();
            $data['company_id'] = $company->id;
        }

        $rate->update($data);

        return response()->json(['rate' => $rate]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param ServiceProviderRate $rate
     * @return JsonResponse
     * @throws Exception
     */
    public function destroy(ServiceProviderRate $rate): JsonResponse
    {
        $rate->delete();

        return response()->json(null, Response::HTTP_NO_CONTENT);
    }
}
