<?php

namespace App\Modules\_common_\Rates\Requests;

use App\Libraries\Utils\Utils;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $user = Utils::getCurrentUser();
        return [
            'company_id' => [Rule::requiredIf($user->user_access_level === 0), 'exists:companies,id'],
            'specialization_id' => ['required', 'exists:specializations,id'],
            'rate_amount' => ['required', 'numeric', 'min:0'],
            'rate_date' => ['required', 'date_format:' . config('date.carbonDate')],
        ];
    }
}
