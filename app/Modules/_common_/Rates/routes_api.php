<?php

Route::prefix('rates')->middleware(['jwt', 'access.control'])->group(function () {
    Route::get('/', 'RateController@index')->name('rates.index');
    Route::post('/', 'RateController@store')->name('rates.store');
    Route::put('/{rate}', 'RateController@update')->name('rates.update');
    Route::delete('/{rate}', 'RateController@destroy')->name('rates.destroy');
    Route::get('/specializations', 'SpecializationController')->name('rates.specializations-list');
    Route::get('/companies', 'CompanyController')->name('rates.companies-list');
});
