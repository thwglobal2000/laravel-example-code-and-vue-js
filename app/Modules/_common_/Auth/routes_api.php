<?php

Route::post('login', 'LoginController@login')->name('api.login');
Route::post('register', 'RegisterController@register')->name('api.register');
Route::post('send-reset-email', 'ForgotPasswordController@sendResetEmail')->name('api.send-reset-email');
Route::post('password-reset', 'ResetPasswordController@passwordReset')->name('api.password-reset');
Route::post('verify/{emailVerificationCode}', 'RegisterController@verifyUser')->name('api.verify-user');
Route::post('legal-representative/verify/{code}', 'RegisterController@verifyLegalRepresentative')
    ->name('api.verify-legal-representative');

Route::group(['middleware' => ['jwt']], function () {
    Route::post('logout', 'LoginController@logout')->name('api.logout');
    Route::get('refresh', 'LoginController@refresh')->name('api.refresh');
});
