<?php

namespace App\Modules\_common_\Auth\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Auth::guest();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $registerRoles = [
            'participant',
            'service-provider',
        ];

        return [
            'name' => ['required'],
            'email' => ['required', 'email', 'unique:users'],
            'password' => ['required', 'confirmed', 'min:6'],
            'phone' => ['phone:AUTO,AU', 'unique:users'],
            'agreement' => ['required', 'accepted'],
            'role' => ['required', Rule::in($registerRoles)],
        ];
    }
}
