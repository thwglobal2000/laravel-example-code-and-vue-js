<?php

namespace App\Modules\_common_\Auth\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class VerifyUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Auth::guest();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'first_name' => ['required', 'string'],
            'last_name' => ['required', 'string'],
            'ndis_number' => ['required', 'string'],
            'plan_type' => ['required', 'string'],
            'plan_manager' => ['required_if:plan_type,plan_managed'],
            'unit_suit_number' => ['nullable', 'string'],
            'street_type' => ['nullable', 'string'],
            'street_number' => ['nullable', 'string'],
            'street_name' => ['nullable', 'string'],
            'state' => ['nullable', 'string'],
            'postcode' => ['nullable', 'string'],
            'country' => ['nullable', 'string'],
            'date_of_birth' => ['required', 'date_format:' . config('date.carbonDate')],
            'is_legal_representative_exists' => ['required', 'boolean'],

            'legal_representative' => ['sometimes'],
            'legal_representative.first_name' => ['required', 'string'],
            'legal_representative.last_name' => ['required', 'string'],
            'legal_representative.email' => ['required', 'email'],
            'legal_representative.phone' => ['required', 'phone:AU'],
            'legal_representative.type' => ['required'],
        ];
    }
}
