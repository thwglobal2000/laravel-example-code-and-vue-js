<?php

namespace App\Modules\_common_\Auth\Controllers;

use App\Http\Controllers\Controller;
use App\Mail\UserRegistered;
use App\Models\EmailVerificationCode\EmailVerificationCode;
use App\Models\LegalRepresentativeVerifyCode\LegalRepresentativeVerifyCode;
use App\Models\User\User;
use App\Modules\_common_\Auth\Requests\RegisterRequest;
use App\Modules\_common_\Auth\Requests\VerifyLegalRepresentativeRequest;
use Exception;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Mail;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Throwable;

class RegisterController extends Controller
{
    use RegistersUsers;

    /**
     * Create a new controller instance.
     *
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * @param RegisterRequest $request
     * @return JsonResponse
     * @throws Throwable
     * @noinspection PhpUndefinedFieldInspection
     */
    public function register(RegisterRequest $request): JsonResponse
    {
        if (!config('app_custom.enable.registration')) {
            throw new HttpException(Response::HTTP_FORBIDDEN, 'Registration not available');
        }

        $role = $request->role;

        if ($role == 'participant') {
            DB::transaction(function () use ($request) {
                $data = $request->except('role', 'agreement', 'password_confirmation');

                $user = User::create($data);
                $user->syncRoles(['participant']);

                $emailVerificationCode = EmailVerificationCode::create([
                    'user_id' => $user->id,
                    'hash' => Str::random(32),
                ]);

                Mail::send(new UserRegistered($user, $data['password'], $emailVerificationCode->hash));
            });
        } else {
            throw new HttpException(Response::HTTP_BAD_REQUEST, 'Unknown role type');
        }

        return response()->json([
            'message' => 'Successfully registered. Please, check your email for the next step'
        ]);
    }

    /**
     * @param EmailVerificationCode $emailVerificationCode
     * @return JsonResponse
     * @throws Exception
     * @noinspection PhpUnused
     */
    public function verifyUser(EmailVerificationCode $emailVerificationCode): JsonResponse
    {
        $user = $emailVerificationCode->user;

        $user->markEmailAsVerified();
        $emailVerificationCode->delete();

        return response()->json([
            'message' => 'Successfully verified',
        ]);
    }

    /**
     * @param LegalRepresentativeVerifyCode $code
     * @param VerifyLegalRepresentativeRequest $request
     * @return JsonResponse
     * @noinspection PhpUnused
     * @noinspection PhpUndefinedFieldInspection
     */
    public function verifyLegalRepresentative(
        LegalRepresentativeVerifyCode $code,
        VerifyLegalRepresentativeRequest $request
    ): JsonResponse {
        $legalRepresentative = $code->legalRepresentative;

        if ($legalRepresentative->email != $request->email) {
            throw new HttpException(Response::HTTP_BAD_REQUEST, 'Invalid email');
        }

        $legalRepresentative->update(['password' => $request->password]);
        $code->forceDelete();

        return response()->json(['message' => 'Password changed successfully']);
    }
}
