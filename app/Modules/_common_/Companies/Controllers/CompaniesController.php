<?php

namespace App\Modules\_common_\Companies\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Company\Company;
use Illuminate\Database\Eloquent\Collection;

class CompaniesController extends Controller
{
    public function getPlanManagers(): Collection
    {
        return Company
            ::where('company_type', Company::COMPANY_PLAN_MANAGER)
            ->orderBy('contact_name')
            ->get(['id', 'business_name', 'contact_name']);
    }
}
