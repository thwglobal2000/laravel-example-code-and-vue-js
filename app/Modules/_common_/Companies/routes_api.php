<?php

Route::prefix('companies')->middleware(['jwt', 'access.control'])->group(function () {
    Route::get('/plan-managers', 'CompaniesController@getPlanManagers')->name('companies.plan-managers');
});
