<?php

namespace App\Modules\Admin\RateTypes\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RateTypeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     * @noinspection PhpUndefinedFieldInspection
     */
    public function rules(): array
    {
        return [
            'name' => ['required', 'string', 'max:191', Rule::unique('rate_types')->ignore($this->id)],
            'mm_value' => ['sometimes', 'required_without_all:state', 'in:1,2,3,4,5,6,7', 'nullable'],
            'state' => ['sometimes', 'required_without_all:mm_value', 'string', 'nullable'],
        ];
    }

    public function messages(): array
    {
        return [
            'mm_value.required_without_all' => 'The MM field is required when none of State are present',
            'state.required_without_all' => 'The State field is required when none of MM are present',
        ];
    }
}
