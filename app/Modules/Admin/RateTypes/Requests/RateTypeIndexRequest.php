<?php

namespace App\Modules\Admin\RateTypes\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RateTypeIndexRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'term' => ['string', 'nullable', 'min:1'],

            'sort' => ['array'],
            'sort.column' => [Rule::requiredIf($this->has('sort')), 'string'],
            'sort.direction' => [Rule::requiredIf($this->has('sort')), 'in:asc,desc'],

            'pagination' => ['array'],
            'pagination.page' => [Rule::requiredIf($this->has('pagination')), 'integer', 'min:1'],
            'pagination.pageSize' => [Rule::requiredIf($this->has('pagination')), 'integer', 'min:1'],
        ];
    }
}
