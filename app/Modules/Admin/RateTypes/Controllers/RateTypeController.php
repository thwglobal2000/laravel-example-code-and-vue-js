<?php

namespace App\Modules\Admin\RateTypes\Controllers;

use App\Http\Controllers\Controller;
use App\Models\RateType\RateType;
use App\Modules\Admin\RateTypes\Requests\RateTypeIndexRequest;
use App\Modules\Admin\RateTypes\Requests\RateTypeRequest;
use Exception;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class RateTypeController extends Controller
{
    /**
     * @param RateTypeIndexRequest $request
     * @return LengthAwarePaginator
     * @noinspection PhpUndefinedFieldInspection
     */
    public function index(RateTypeIndexRequest $request): LengthAwarePaginator
    {
        $page = $request->input('pagination.page', 1);
        $pageSize = $request->input('pagination.pageSize', 10);
        $term = $request->term;

        return RateType
            ::when(
                $request->filled('sort'),
                fn($query) => $query->orderBy($request->input('sort.column'), $request->input('sort.direction'))
            )
            ->when($term, fn($query, $value) => $query->where('name', 'like', "%$value%"))
            ->paginate($pageSize, ['*'], 'page', $page);
    }

    /**
     * @param RateTypeRequest $request
     * @return JsonResponse
     */
    public function store(RateTypeRequest $request): JsonResponse
    {
        $rateType = RateType::create($request->validated());

        return response()->json(['rateType' => $rateType]);
    }

    /**
     * @param RateType $rateType
     * @param RateTypeRequest $request
     * @return JsonResponse
     */
    public function update(RateType $rateType, RateTypeRequest $request): JsonResponse
    {
        $rateType->update($request->validated());

        return response()->json(['rateType' => $rateType]);
    }

    /**
     * @param RateType $rateType
     * @return JsonResponse
     * @throws Exception
     */
    public function destroy(RateType $rateType): JsonResponse
    {
        $rateType->delete();

        return response()->json(null, Response::HTTP_NO_CONTENT);
    }
}
