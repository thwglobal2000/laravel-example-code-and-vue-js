<?php

Route::prefix('rate-types')->middleware(['jwt', 'access.control'])->group(function () {
    Route::get('/', 'RateTypeController@index')->name('rate-types.index');
    Route::post('/', 'RateTypeController@store')->name('rate-types.store');
    Route::put('/{rateType}', 'RateTypeController@update')->name('rate-types.update');
    Route::delete('/{rateType}', 'RateTypeController@destroy')->name('rate-types.destroy');
});
