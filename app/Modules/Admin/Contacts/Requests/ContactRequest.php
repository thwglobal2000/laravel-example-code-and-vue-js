<?php

namespace App\Modules\Admin\Contacts\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'user_id' => ['nullable', 'exists:users,id', Rule::unique('contacts')->ignore($this->id)],
            'first_name' => ['required', 'string'],
            'last_name' => ['required', 'string'],
            'date_of_birth' => ['required', 'date_format:' . config('date.carbonDate'), 'before_or_equal:today'],
            'mobile_phone_number' => ['required', 'phone:AU'],
            'home_phone_number' => ['nullable', 'phone:AU'],
            'email' => ['required', 'email'],
            'address' => ['nullable', 'string'],
            'postcode' => ['nullable', 'string'],
            'country' => ['nullable', 'string'],
            'ndis_number' => ['nullable', 'string'],

            'companies' => ['array'],
            'companies.*' => ['exists:companies,id'],
        ];
    }

    public function messages(): array
    {
        return [
            'user_id.exists' => 'The user is not exists',
            'user_id.unique' => 'The user already has a contact',
            'date_of_birth.date_format' => 'Wrong date format',
        ];
    }
}
