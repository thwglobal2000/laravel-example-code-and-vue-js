<?php

namespace App\Modules\Admin\Contacts\Controllers;

use App\Http\Controllers\Controller;
use App\Models\User\User;
use Illuminate\Http\JsonResponse;

class UserController extends Controller
{
    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $users = User
            ::doesntHave('contact')
            ->orderBy('name')
            ->get(['id', 'name']);

        return response()->json(['data' => $users]);
    }

    /**
     * @return JsonResponse
     */
    public function getLegalRepresentatives(): JsonResponse
    {
        $legalRepresentatives = User::whereRoleIs('representative')->get(['id', 'name']);

        return response()->json(['data' => $legalRepresentatives]);
    }
}
