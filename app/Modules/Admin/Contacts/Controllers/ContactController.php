<?php

namespace App\Modules\Admin\Contacts\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Contact\Contact;
use App\Modules\Admin\Contacts\Requests\ContactIndexRequest;
use App\Modules\Admin\Contacts\Requests\ContactRequest;
use Exception;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class ContactController extends Controller
{
    /**
     * @param ContactIndexRequest $request
     * @return LengthAwarePaginator
     * @noinspection PhpUndefinedFieldInspection
     */
    public function index(ContactIndexRequest $request): LengthAwarePaginator
    {
        $page = $request->input('pagination.page', 1);
        $pageSize = $request->input('pagination.pageSize', 10);
        $term = $request->term;

        return Contact
            ::with(['companies:id', 'user:id,name'])
            ->orderBy('contacts.first_name')
            ->orderBy('contacts.last_name')
            ->when(
                $request->filled('sort'),
                fn($query) => $query->orderBy($request->input('sort.column'), $request->input('sort.direction'))
            )
            ->when(
                $term,
                fn($query) => $query->where(fn($query) => $query
                    ->where('first_name', 'like', "%$term%")
                    ->orWhere('last_name', 'like', "%$term%")
                    ->orWhere('email', 'like', "%$term%"))
            )
            ->paginate($pageSize, ['*'], 'page', $page);
    }

    /**
     * @param ContactRequest $request
     * @return JsonResponse
     * @noinspection PhpUndefinedFieldInspection
     */
    public function store(ContactRequest $request): JsonResponse
    {
        $contact = Contact::create($request->except('companies'));
        $contact->companies()->sync($request->companies);

        return response()->json(['contact' => $contact]);
    }

    /**
     * @param Contact $contact
     * @param ContactRequest $request
     * @return JsonResponse
     * @noinspection PhpUndefinedFieldInspection
     */
    public function update(Contact $contact, ContactRequest $request): JsonResponse
    {
        $contact->update($request->except('companies'));
        $contact->companies()->sync($request->companies);
        $contact->load('companies:id');

        return response()->json(['contact' => $contact]);
    }

    /**
     * @param Contact $contact
     * @return JsonResponse
     * @throws Exception
     */
    public function destroy(Contact $contact): JsonResponse
    {
        $contact->delete();

        return response()->json(null, Response::HTTP_NO_CONTENT);
    }
}
