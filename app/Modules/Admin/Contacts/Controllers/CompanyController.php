<?php

namespace App\Modules\Admin\Contacts\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Company\Company;
use Illuminate\Http\JsonResponse;

class CompanyController extends Controller
{
    public function index(): JsonResponse
    {
        $companies = Company::orderBy('business_name')->get(['id', 'business_name']);

        return response()->json(['data' => $companies]);
    }
}
