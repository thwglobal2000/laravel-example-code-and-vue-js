<?php

Route::group(['prefix' => 'contacts', 'middleware' => ['jwt', 'access.control']], function () {
    Route::get('/', 'ContactController@index')->name('contacts.index');
    Route::post('/', 'ContactController@store')->name('contacts.store');
    Route::put('/{contact}', 'ContactController@update')->name('contacts.update');
    Route::delete('/{contact}', 'ContactController@destroy')->name('contacts.destroy');
    Route::get('/users', 'UserController@index')->name('contacts.users-list');
    Route::get('/companies', 'CompanyController@index')->name('contacts.companies-list');
    Route::get('/legal-representatives', 'CompanyController@index')->name('contacts.legal-representatives-list');
});
