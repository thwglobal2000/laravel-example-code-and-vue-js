<?php

Route::prefix('plans')->middleware(['jwt', 'access.control'])->group(function () {
    Route::get('/', 'PlanController@index')->name('plans.index');
    Route::post('/', 'PlanController@store')->name('plans.store');
    Route::put('/{plan}', 'PlanController@update')->name('plans.update');
    Route::delete('/{plan}', 'PlanController@destroy')->name('plans.destroy');
    Route::get('/companies', 'CompanyController@index')->name('plans.companies-list');
    Route::get('/participants', 'ParticipantController@index')->name('plans.participants-list');
});
