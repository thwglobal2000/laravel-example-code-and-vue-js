<?php

namespace App\Modules\Admin\Plans\Requests;

use App\Models\Plan\Plan;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class PlanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     * @noinspection PhpUndefinedFieldInspection
     */
    public function rules(): array
    {
        return [
            'company_id' => ['required', 'exists:companies,id'],
            'participant_id' => ['required', 'exists:users,id'],
            'ndis_reference_number' => ['required', Rule::unique('plans')->ignore($this->id)],
            'active' => ['required', 'boolean'],
            'expiration_date' => ['required', 'date_format:Y-m-d', 'after_or_equal:' . today()->format('Y-m-d')],
            'manage_type' => ['required', Rule::in(array_keys(Plan::MANAGE_TYPES))],
        ];
    }

    public function messages(): array
    {
        return [
            'company_id.required' => 'Company is required',
            'company_id.exists' => 'Company does not exist',
            'participant_id.required' => 'Company is required',
            'participant_id.exists' => 'Company does not exist',
        ];
    }
}
