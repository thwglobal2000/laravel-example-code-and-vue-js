<?php

namespace App\Modules\Admin\Plans\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Plan\Plan;
use App\Modules\Admin\Plans\Requests\PlanIndexRequest;
use App\Modules\Admin\Plans\Requests\PlanRequest;
use Exception;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class PlanController extends Controller
{
    /**
     * @param PlanIndexRequest $request
     * @return LengthAwarePaginator
     * @noinspection PhpUndefinedFieldInspection
     */
    public function index(PlanIndexRequest $request): LengthAwarePaginator
    {
        $page = $request->input('pagination.page', 1);
        $pageSize = $request->input('pagination.pageSize', 10);
        $term = $request->term;

        return Plan
            ::when(
                $request->filled('sort'),
                fn($query) => $query->orderBy($request->input('sort.column'), $request->input('sort.direction'))
            )
            ->when($term, fn($query, $value) => $query->where('ndis_reference_number', 'like', "%$value%"))
            ->paginate($pageSize, ['*'], 'page', $page);
    }

    /**
     * @param PlanRequest $request
     * @return JsonResponse
     */
    public function store(PlanRequest $request): JsonResponse
    {
        $plan = Plan::create($request->validated());

        return response()->json(['plan' => $plan]);
    }

    /**
     * @param Plan $plan
     * @param PlanRequest $request
     * @return JsonResponse
     */
    public function update(Plan $plan, PlanRequest $request): JsonResponse
    {
        $plan->update($request->validated());

        return response()->json(['plan' => $plan]);
    }

    /**
     * @param Plan $plan
     * @return JsonResponse
     * @throws Exception
     */
    public function destroy(Plan $plan): JsonResponse
    {
        $plan->delete();

        return response()->json(null, Response::HTTP_NO_CONTENT);
    }
}
