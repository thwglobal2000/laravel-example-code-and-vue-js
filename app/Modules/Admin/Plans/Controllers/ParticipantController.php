<?php

namespace App\Modules\Admin\Plans\Controllers;

use App\Http\Controllers\Controller;
use App\Models\User\User;
use Illuminate\Http\JsonResponse;

class ParticipantController extends Controller
{
    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $participants = User::whereRoleIs('participant')->get(['id', 'name']);

        return response()->json(['data' => $participants]);
    }
}
