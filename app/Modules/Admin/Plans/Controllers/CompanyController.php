<?php

namespace App\Modules\Admin\Plans\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Company\Company;
use Illuminate\Http\JsonResponse;

class CompanyController extends Controller
{
    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $companies = Company::get(['id', 'business_name']);

        return response()->json(['data' => $companies]);
    }
}
