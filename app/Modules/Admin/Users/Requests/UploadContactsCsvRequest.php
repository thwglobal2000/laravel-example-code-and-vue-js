<?php

namespace App\Modules\Admin\Users\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UploadContactsCsvRequest extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'file' => 'required|file|mimes:csv,txt',
        ];
    }
}
