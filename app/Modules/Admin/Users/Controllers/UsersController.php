<?php

namespace App\Modules\Admin\Users\Controllers;

use App\Http\Controllers\Controller;
use App\Libraries\Utils\Utils;
use App\Mail\UserRegistered;
use App\Models\Role\Role;
use App\Models\User\User;
use App\Modules\_common_\Auth\Requests\RegisterRequest;
use App\Modules\Admin\Users\Requests\UploadContactsCsvRequest;
use App\Traits\HasCroppedImage;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Validation\ValidationException;
use Mail;
use Storage;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;
use Throwable;
use Tymon\JWTAuth\Facades\JWTAuth;


class UsersController extends Controller
{
    use HasCroppedImage;

    public function index(Request $request): LengthAwarePaginator
    {
        $curUserLevel = Utils::getCurrentUser()->user_access_level;

        $query = $this->buildQuery($request);

        $users = $query
            ->get()
            ->filter(function (User $userToShow) use ($curUserLevel) {
                return $curUserLevel <= $userToShow->user_access_level;
            });

        foreach ($users as &$user) {
            $role = $user->roles->first();
            $user['role'] = $role['id'] ?? 0;
            $user['roleDisplayName'] = $role['display_name'] ?? 'Role not selected';
            $user = $this->attachTags($user);
        }

        $page     = $request->input('page', 1);
        $pageSize = (int)$request->input('pageSize', 10);

        return new LengthAwarePaginator(
            array_values($users->forPage($page, $pageSize)->toArray()),
            $users->count(),
            $pageSize,
            $page,
            ['path' => config('app.url')]
        );
    }

    private function buildQuery(Request $request): Builder
    {
        [$column, $order] = explode(',', $request->input('sortBy', 'id,asc'));
        $search = $request->input('search', '');

        return User
            ::when($search, function ($query) use ($search) {
                return $query
                    ->where('name', 'like', "%$search%")
                    ->orWhere('email', 'like', "%$search%")
                    ->orWhere('phone', 'like', "%$search%");
            })
            ->with(['tags', 'roles'])
            ->orderBy($column, $order);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     * @throws Exception
     * @throws Throwable
     */
    public function store(Request $request): JsonResponse
    {
        $request       = Utils::updateRequestPhone($request);
        $rules         = (new RegisterRequest)->rules();
        $rules['role'] = 'required';
        $rules['agreement'] = '';

        $this->validate($request, $rules);
        $data = $request->all();

        $this->roleIsAvailableForCurrentUser($request->role);

        $user = User::create($data);
        $user->syncRoles([$request->input('role')]);

        $user->save();

        if ($request->get('avatar')) {
            $user->avatar = $this->getCroppedImagePath($request->get('avatar'), 'users', $user->id);
            $user->save();
        }

        Mail::send(new UserRegistered($user, $data['password'], false));

        return response()->json([
            'message' => 'Successfully created'
        ]);
    }

    /**
     * Check that requested role is available for callable user
     *
     * @param int|null $roleId
     *
     * @return bool
     * @throws Exception
     */
    private function roleIsAvailableForCurrentUser($roleId)
    {
        $user          = Utils::getCurrentUser();
        $access_levels = config('laratrust_seeder.access_levels');
        $role          = Role::find($roleId);

        if ($user && $role) {
            if ($role->initial) {
                $requestedRoleLevel = $access_levels[$role->name] ?? null;
                if ($requestedRoleLevel) {
                    if ($requestedRoleLevel >= $user->user_access_level) {
                        return true;
                    }
                } else {
                    return true;
                }
            } else {
                return true;
            }
        }

        throw new UnprocessableEntityHttpException("Current user doesn't meet required permissions to set selected role.");
    }

    /**
     * @param Request $request
     * @param User $user
     *
     * @return JsonResponse
     * @throws Exception
     * @throws Throwable
     */
    public function update(Request $request, User $user): JsonResponse
    {
        $request = Utils::updateRequestPhone($request);
        $rules   = [
            'name'  => 'sometimes|required',
            'email' => 'sometimes|required|email|unique:users,email,' . $user->id,
            'role'  => 'sometimes|required',
            'phone' => 'sometimes|phone:AUTO,AU|unique:users,phone,' . $user->id,
        ];
        if ($request->password || $request->password_confirmation) {
            $rules['password'] = 'required|min:4|confirmed';
        }
        $this->validate($request, $rules);

        $data = $request->all();

        if ($request->role) {
            $this->roleIsAvailableForCurrentUser($request->role);
        }

        $user->fill($data);

        if (isset($data['tags']) && is_array($data['tags'])) {
            $user->syncTags($data['tags']);
        }

        // to change role this user
        if ($request->input('role')) {
            $user->syncRoles([$request->input('role')]);
        }


        if ($request->get('avatar')) {
            $user->avatar = $this->getCroppedImagePath($request->get('avatar'), 'users', $user->id);
        }

        $user->save();

        return response()->json([
            'message' => 'Successfully updated'
        ]);
    }

    /**
     * @param User $user
     *
     * @return JsonResponse
     * @throws Exception
     */
    public function destroy(User $user): JsonResponse
    {
        try {
            $higher_priority_role = $user
                ::select('roles.id')
                ->leftJoin('role_user', 'role_user.user_id', 'users.id')
                ->leftJoin('roles', 'role_user.role_id', 'roles.id')
                ->where('users.id', $user->id)
                ->orderBy('level', 'asc')
                ->first();

            if ($higher_priority_role) {
                $roleIsSuitable = $this->roleIsAvailableForCurrentUser($higher_priority_role->id);
            } else {
                //user has no role
                $roleIsSuitable = true;
            }
            if (!$roleIsSuitable) {
                throw new Exception(); // Just initiate to raise Not enough permissions exception
            }
        } catch (Exception $exception) {
            throw new UnprocessableEntityHttpException("Current user doesn't have enough permissions to delete selected user.");
        }

        try {
            Storage::deleteDirectory('public/users/' . $user->id);
        } catch (Exception $e) {
        }

        $user->syncRoles([])->delete();

        return response()->json([
            'message' => 'The user has been successfully deleted'
        ], Response::HTTP_ACCEPTED);
    }

    public function customers(): JsonResponse
    {
        $customers = User::byRoleAndOrdered('customer')->get();

        return response()->json($customers);
    }

    /**
     * @param User $user
     *
     * @return JsonResponse
     */
    public function get(User $user): JsonResponse
    {
        return response()->json($user);
    }

    public function getAutocomplete(Request $request): JsonResponse
    {
        $search = $request->input('search');
        $search = is_string($search) ? $search : '';
        $users  = [];
        if ($search != '') {
            $users = User
                ::where('name', 'like', "%$search%")
                ->orWhere('email', 'like', "%$search%")
                ->orWhere('phone', 'like', "%$search%")
                ->limit(10)->get();
        }

        return response()->json($users);
    }

    public function loginAs(Request $request): JsonResponse
    {
        try {
            $user  = User::findOrFail($request->user_id);
            $token = JWTAuth::fromUser($user);
            JWTAuth::setToken($token);

            $showMessages = $user->isAbleTo('system-messages.list');

            return response()
                ->json([
                    'token' => "Bearer $token",
                    'system_messages' => $showMessages
                ])
                ->header('Authorization', $token);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    /**
     * @param $user
     *
     * @return mixed
     */
    private function attachTags($user)
    {
        $tags = [];
        if ($user->tags->count()) {
            $tags = $user->tags->map(function ($tag) {
                return $tag->name;
            });
        }
        unset($user->tags);
        $user['tagList'] = $tags;
        return $user;
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     * @throws ValidationException
     */
    public function imageUpload(Request $request)
    {
        $this->validate($request, [
            "img" => "image|mimes:jpeg,jpg,png|max:6000",
        ]);

        $path = \Storage::url($request->file('img')->storePublicly('public/temp'));

        return response()->json($path);
    }

    public function uploadContactCsv(UploadContactsCsvRequest $request)
    {
        $name = $this->appendDateTimeToFileName('file.csv');
        $path = '/uploads/csv';
        $path = \Storage::disk('public')->putFileAs($path, $request->file, $name);

        if (Storage::disk('public')->exists($path)) {
            return [ $this->csvReaderFile("storage/$path") ];
        }

        return [222];
    }

    private function csvReaderFile($path)
    {
        $reader = new \SpreadsheetReader($path);
        // format the data array from the file and work with the data
        foreach ($reader as $row) {
            return $row;
        }

        return $reader;
    }

    private function appendDateTimeToFileName($fileName)
    {
        $appended = date('_Y_m_d_H_i_s');
        $dotCount = substr_count($fileName, '.');
        if (!$dotCount) {
            return $fileName . $appended;
        }
        $extension = pathinfo($fileName, PATHINFO_EXTENSION);
        $fileName = pathinfo($fileName, PATHINFO_FILENAME);
        return $fileName . $appended . '.' . $extension;
    }
}
