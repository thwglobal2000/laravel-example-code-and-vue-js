<?php

Route::prefix('support-names')->middleware(['jwt', 'access.control'])->group(function () {
    Route::get('/', 'SupportNameController@index')->name('support-names.index');
    Route::post('/', 'SupportNameController@store')->name('support-names.store');
    Route::put('/{supportName}', 'SupportNameController@update')->name('support-names.update');
    Route::delete('/{supportName}', 'SupportNameController@destroy')->name('support-names.destroy');
});
