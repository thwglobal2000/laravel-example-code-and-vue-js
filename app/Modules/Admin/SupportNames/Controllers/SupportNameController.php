<?php

namespace App\Modules\Admin\SupportNames\Controllers;

use App\Http\Controllers\Controller;
use App\Models\SupportName\SupportName;
use App\Modules\Admin\SupportNames\Requests\SupportNameIndexRequest;
use App\Modules\Admin\SupportNames\Requests\SupportNameRequest;
use Exception;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class SupportNameController extends Controller
{
    /**
     * @param SupportNameIndexRequest $request
     * @return LengthAwarePaginator
     * @noinspection PhpUndefinedFieldInspection
     * @noinspection DuplicatedCode
     */
    public function index(SupportNameIndexRequest $request): LengthAwarePaginator
    {
        $page = $request->input('pagination.page', 1);
        $pageSize = $request->input('pagination.pageSize', 10);
        $term = $request->term;

        return SupportName
            ::when(
                $request->filled('sort'),
                fn($query) => $query->orderBy($request->input('sort.column'), $request->input('sort.direction'))
            )
            ->when($term, fn($query, $value) => $query->where('name', 'like', "%$value%"))
            ->paginate($pageSize, ['*'], 'page', $page);
    }

    /**
     * @param SupportNameRequest $request
     * @return JsonResponse
     */
    public function store(SupportNameRequest $request): JsonResponse
    {
        $supportName = SupportName::create($request->validated());

        return response()->json(['supportName' => $supportName]);
    }

    /**
     * @param SupportName $supportName
     * @param SupportNameRequest $request
     * @return JsonResponse
     */
    public function update(SupportName $supportName, SupportNameRequest $request): JsonResponse
    {
        $supportName->update($request->validated());

        return response()->json(['supportName' => $supportName]);
    }

    /**
     * @param SupportName $supportName
     * @return JsonResponse
     * @throws Exception
     */
    public function destroy(SupportName $supportName): JsonResponse
    {
        $supportName->delete();

        return response()->json(null, Response::HTTP_NO_CONTENT);
    }
}
