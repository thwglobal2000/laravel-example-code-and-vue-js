<?php

namespace App\Modules\Admin\SupportCategories\Controllers;

use App\Http\Controllers\Controller;
use App\Models\SupportCategory\SupportCategory;
use App\Modules\Admin\SupportCategories\Requests\SupportCategoryIndexRequest;
use App\Modules\Admin\SupportCategories\Requests\SupportCategoryRequest;
use Exception;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class SupportCategoryController extends Controller
{
    /**
     * @param SupportCategoryIndexRequest $request
     * @return LengthAwarePaginator
     * @noinspection PhpUndefinedFieldInspection
     * @noinspection DuplicatedCode
     */
    public function index(SupportCategoryIndexRequest $request): LengthAwarePaginator
    {
        $page = $request->input('pagination.page', 1);
        $pageSize = $request->input('pagination.pageSize', 10);
        $term = $request->term;

        return SupportCategory
            ::when(
                $request->filled('sort'),
                fn($query) => $query->orderBy($request->input('sort.column'), $request->input('sort.direction'))
            )
            ->when($term, fn($query, $value) => $query->where('name', 'like', "%$value%"))
            ->paginate($pageSize, ['*'], 'page', $page);
    }

    /**
     * @param SupportCategoryRequest $request
     * @return JsonResponse
     */
    public function store(SupportCategoryRequest $request): JsonResponse
    {
        $supportCategory = SupportCategory::create($request->validated());

        return response()->json(['supportCategory' => $supportCategory]);
    }

    /**
     * @param SupportCategory $supportCategory
     * @param SupportCategoryRequest $request
     * @return JsonResponse
     */
    public function update(SupportCategory $supportCategory, SupportCategoryRequest $request): JsonResponse
    {
        $supportCategory->update($request->validated());

        return response()->json(['supportCategory' => $supportCategory]);
    }

    /**
     * @param SupportCategory $supportCategory
     * @return JsonResponse
     * @throws Exception
     */
    public function destroy(SupportCategory $supportCategory): JsonResponse
    {
        $supportCategory->delete();

        return response()->json(null, Response::HTTP_NO_CONTENT);
    }
}
