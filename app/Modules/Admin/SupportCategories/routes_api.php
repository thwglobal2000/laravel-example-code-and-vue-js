<?php

Route::prefix('support-categories')->middleware(['jwt', 'access.control'])->group(function () {
    Route::get('/', 'SupportCategoryController@index')->name('support-categories.index');
    Route::post('/', 'SupportCategoryController@store')->name('support-categories.store');
    Route::put('/{supportCategory}', 'SupportCategoryController@update')->name('support-categories.update');
    Route::delete('/{supportCategory}', 'SupportCategoryController@destroy')->name('support-categories.destroy');
});
