<?php

namespace App\Modules\Admin\PersonnelSpecializations\Controllers;

use App\Http\Controllers\Controller;
use App\Models\PersonnelSpecialization\PersonnelSpecialization;
use App\Modules\Admin\PersonnelSpecializations\Requests\PersonnelSpecializationIndexRequest;
use App\Modules\Admin\PersonnelSpecializations\Requests\PersonnelSpecializationRequest;
use Exception;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class PersonnelSpecializationController extends Controller
{
    /**
     * @param PersonnelSpecializationIndexRequest $request
     * @return LengthAwarePaginator
     * @noinspection PhpUndefinedFieldInspection
     */
    public function index(PersonnelSpecializationIndexRequest $request): LengthAwarePaginator
    {
        $page = $request->input('pagination.page', 1);
        $pageSize = $request->input('pagination.pageSize', 10);
        $term = $request->term;

        return PersonnelSpecialization::with('person:id,first_name,last_name,email')
            ->when(
                $request->filled('sort'),
                fn($query) => $query->orderBy($request->input('sort.column'), $request->input('sort.direction'))
            )
            ->when(
                $term,
                fn($query, $value) => $query->where('ahpra_registration_number', 'like', "%$value%")
            )
            ->paginate($pageSize, ['*'], 'page', $page);
    }

    /**
     * @param PersonnelSpecializationRequest $request
     * @return JsonResponse
     */
    public function store(PersonnelSpecializationRequest $request): JsonResponse
    {
        $data = $request->validated();
        $specialization = PersonnelSpecialization::create($data);

        return response()->json(['personnelSpecialization' => $specialization]);
    }

    /**
     * @param PersonnelSpecialization $specialization
     * @param PersonnelSpecializationRequest $request
     * @return JsonResponse
     */
    public function update(
        PersonnelSpecialization $specialization,
        PersonnelSpecializationRequest $request
    ): JsonResponse {
        $specialization->update($request->validated());

        return response()->json(['personnelSpecialization' => $specialization]);
    }

    /**
     * @param PersonnelSpecialization $specialization
     * @return JsonResponse
     * @throws Exception
     */
    public function destroy(PersonnelSpecialization $specialization): JsonResponse
    {
        $specialization->delete();

        return response()->json(null, Response::HTTP_NO_CONTENT);
    }
}
