<?php

namespace App\Modules\Admin\PersonnelSpecializations\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Specialization\Specialization;
use Illuminate\Http\JsonResponse;

class SpecializationController extends Controller
{
    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $specializations = Specialization::with('serviceType:id,service_name')
            ->orderBy('specializations.name')
            ->get();

        return response()->json(['data' => $specializations]);
    }
}
