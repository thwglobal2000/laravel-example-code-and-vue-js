<?php

namespace App\Modules\Admin\PersonnelSpecializations\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Contact\Contact;
use Illuminate\Http\JsonResponse;

class ContactController extends Controller
{
    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $contacts = Contact::orderBy('first_name')->get(['id', 'first_name', 'last_name']);

        return response()->json(['data' => $contacts]);
    }
}
