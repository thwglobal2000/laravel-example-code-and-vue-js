<?php

Route::prefix('personnel-specializations')->middleware(['jwt', 'access.control'])->group(function () {
    Route::get('/', 'PersonnelSpecializationController@index')->name('personnel-specializations.index');
    Route::post('/', 'PersonnelSpecializationController@store')->name('personnel-specializations.store');
    Route::put('/{specialization}', 'PersonnelSpecializationController@update')
        ->name('personnel-specializations.update');
    Route::delete('/{specialization}', 'PersonnelSpecializationController@destroy')
        ->name('personnel-specializations.destroy');
    Route::get('/specializations-list', 'SpecializationController@index')
        ->name('personnel-specializations.specializations-list');
    Route::get('/contacts-list', 'ContactController@index')->name('personnel-specializations.contacts-list');
});
