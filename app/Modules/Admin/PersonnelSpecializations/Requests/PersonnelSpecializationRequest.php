<?php

namespace App\Modules\Admin\PersonnelSpecializations\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PersonnelSpecializationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'ahpra_registration_number' => ['required', 'string', 'max:191'],
            'personnel_id' => ['required', 'exists:contacts,id'],
            'specialization_id' => ['required', 'exists:specializations,id'],
        ];
    }

    public function attributes(): array
    {
        return [
            'personnel_id' => 'person',
            'specialization_id' => 'specialization',
        ];
    }
}
