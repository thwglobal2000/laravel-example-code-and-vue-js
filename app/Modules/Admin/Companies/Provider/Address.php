<?php

namespace App\Modules\Admin\Companies\Provider;

use Faker\Provider\en_AU\Address as AUAddress;

class Address extends AUAddress
{
    public static function getStateList()
    {
        return array_combine(static::$stateAbbr, static::$state);
    }
}
