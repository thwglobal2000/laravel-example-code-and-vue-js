<?php

namespace App\Modules\Admin\Companies\Requests;

use App\Models\Company\Company;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class CompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'admin_id' => [Rule::requiredIf($this->company_type !== Company::COMPANY_PLAN_MANAGER)],
            'business_name' => ['required', 'string'],
            'url' => ['nullable', 'url'],
            'contact_name' => ['required', 'string'],
            'email' => ['required', 'email'],
            'mobile_phone_number' => ['required', 'phone:AU'],
            'phone_number' => ['nullable', 'phone:AU'],
            'address' => ['nullable', 'string'],
            'postcode' => ['nullable', 'string'],
            'country' => ['nullable', 'string'],

            'contacts' => ['array'],
            'contacts.*' => ['exists:contacts,id'],

            'service_types' => ['array'],
            'service_types.*' => ['exists:service_types,id'],

            'company_type' => ['required', Rule::in(array_keys(Company::COMPANY_TYPES))],
        ];
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     * @noinspection PhpUndefinedFieldInspection
     */
    public function prepareForValidation()
    {
        if ($this->filled('url')) {
            $url = $this->url;
            $this->merge([
                'url' => strpos($url, 'http') !== 0 ? "http://$url" : $url,
            ]);
        }
    }
}
