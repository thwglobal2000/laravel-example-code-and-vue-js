<?php

Route::group(['prefix' => 'companies', 'middleware' => ['jwt', 'access.control']], function () {
    Route::get('/', 'CompanyController@index')->name('companies.index');
    Route::post('/', 'CompanyController@store')->name('companies.store');
    Route::put('/{company}', 'CompanyController@update')->name('companies.update');
    Route::delete('/{company}', 'CompanyController@destroy')->name('companies.destroy');
    Route::get('/users', 'UserController@index')->name('companies.users-list');
    Route::get('/contacts', 'ContactController@index')->name('companies.contacts-list');
    Route::get('/contacts-list-service-provider', 'ContactController@getListServiceProvider')
        ->name('companies.contacts-list-service-provider');
    Route::get('/service-types', 'ServiceTypeController@index')->name('companies.service-types-list');
});
