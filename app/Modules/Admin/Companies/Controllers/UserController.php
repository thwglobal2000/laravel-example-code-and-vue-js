<?php

namespace App\Modules\Admin\Companies\Controllers;

use App\Http\Controllers\Controller;
use App\Models\User\User;
use Illuminate\Http\JsonResponse;

class UserController extends Controller
{
    public function index(): JsonResponse
    {
        $users = User::orderBy('name')->get(['id', 'name']);

        return response()->json(['data' => $users]);
    }
}
