<?php

namespace App\Modules\Admin\Companies\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Company\Company;
use App\Modules\Admin\Companies\Requests\CompanyIndexRequest;
use App\Modules\Admin\Companies\Requests\CompanyRequest;
use Exception;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class CompanyController extends Controller
{
    /**
     * @param CompanyIndexRequest $request
     * @return LengthAwarePaginator
     * @noinspection PhpUndefinedFieldInspection
     */
    public function index(CompanyIndexRequest $request): LengthAwarePaginator
    {
        $page = $request->input('pagination.page', 1);
        $pageSize = $request->input('pagination.pageSize', 10);
        $term = $request->term;

        return Company
            ::with('contacts:id', 'serviceTypes:id', 'visitTypes:id,company_id,visit_type')
            ->orderBy('companies.business_name')
            ->when(
                $request->filled('sort'),
                fn($query) => $query->orderBy($request->input('sort.column'), $request->input('sort.direction'))
            )
            ->when(
                $term,
                fn($query) => $query->where(fn($query) => $query
                    ->where('business_name', 'like', "%$term%"))
            )
            ->paginate($pageSize, ['*'], 'page', $page);
    }

    /**
     * @param CompanyRequest $request
     * @return JsonResponse
     * @noinspection PhpUndefinedFieldInspection
     */
    public function store(CompanyRequest $request): JsonResponse
    {
        $data = $request->except('contacts', 'service_types');

        $company = Company::create($data);
        $company->contacts()->sync($request->contacts);
        $company->serviceTypes()->sync($request->service_types);
        $company->syncModels('visitTypes', 'visit_type', $request->visit_types);

        $company->load('contacts:id');

        return response()->json(['company' => $company]);
    }

    /**
     * @param Company $company
     * @param CompanyRequest $request
     * @return JsonResponse
     * @noinspection PhpUndefinedFieldInspection
     */
    public function update(Company $company, CompanyRequest $request): JsonResponse
    {
        $data = $request->except('contacts', 'service_types');

        $company->update($data);
        $company->contacts()->sync($request->contacts);
        $company->serviceTypes()->sync($request->service_types);
        $company->syncModels('visitTypes', 'visit_type', $request->visit_types);

        $company->load('contacts:id');

        return response()->json(['company' => $company]);
    }

    /**
     * @param Company $company
     * @return JsonResponse
     * @throws Exception
     */
    public function destroy(Company $company): JsonResponse
    {
        $company->delete();

        return response()->json(null, Response::HTTP_NO_CONTENT);
    }
}
