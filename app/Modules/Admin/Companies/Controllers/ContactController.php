<?php

namespace App\Modules\Admin\Companies\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Contact\Contact;
use App\Models\Role\Role;
use Illuminate\Http\JsonResponse;

class ContactController extends Controller
{
    public function index(): array
    {
        $data = Contact
            ::orderBy('first_name')
            ->orderBy('last_name')
            ->get()
            ->map(fn($item) => $item->only(['id', 'full_name']));

        return compact('data');
    }

    public function getListServiceProvider(): array
    {
        $data = Contact
            ::orderBy('first_name')
            ->orderBy('last_name')
            ->with('user')
            ->whereHas('user.roles', function ($query) {
                $query->where('name', Role::ROLE_SERVICE_PROVIDER);
            })
            ->get()
            ->map(fn($item) => $item->only(['id', 'full_name']));

        return compact('data');
    }

}
