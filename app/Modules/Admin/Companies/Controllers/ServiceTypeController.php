<?php

namespace App\Modules\Admin\Companies\Controllers;

use App\Http\Controllers\Controller;
use App\Models\ServiceType\ServiceType;
use Illuminate\Http\JsonResponse;

class ServiceTypeController extends Controller
{
    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $serviceTypes = ServiceType
            ::orderBy('service_name')
            ->get(['id', 'service_name']);

        return response()->json(['data' => $serviceTypes]);
    }
}
