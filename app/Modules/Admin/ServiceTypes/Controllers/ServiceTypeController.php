<?php

namespace App\Modules\Admin\ServiceTypes\Controllers;

use App\Http\Controllers\Controller;
use App\Models\ServiceType\ServiceType;
use App\Modules\Admin\ServiceTypes\Requests\ServiceTypeIndexRequest;
use App\Modules\Admin\ServiceTypes\Requests\ServiceTypeRequest;
use Exception;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class ServiceTypeController extends Controller
{
    /**
     * @param ServiceTypeIndexRequest $request
     * @return LengthAwarePaginator
     * @noinspection PhpUndefinedFieldInspection
     */
    public function index(ServiceTypeIndexRequest $request): LengthAwarePaginator
    {
        $page = $request->input('pagination.page', 1);
        $pageSize = $request->input('pagination.pageSize', 10);
        $term = $request->term;

        return ServiceType
            ::when(
                $request->filled('sort'),
                fn($query) => $query->orderBy($request->input('sort.column'), $request->input('sort.direction'))
            )
            ->when(
                $term,
                fn($query, $value) => $query->where('service_name', 'like', "%$value%")
            )
            ->paginate($pageSize, ['*'], 'page', $page);
    }

    public function store(ServiceTypeRequest $request): JsonResponse
    {
        $serviceType = ServiceType::create($request->validated());

        return response()->json(['serviceType' => $serviceType], Response::HTTP_CREATED);
    }

    public function update(ServiceType $serviceType, ServiceTypeRequest $request): JsonResponse
    {
        $serviceType->update($request->validated());

        return response()->json(['serviceType' => $serviceType]);
    }

    /**
     * @param ServiceType $serviceType
     * @return JsonResponse
     * @throws Exception
     */
    public function destroy(ServiceType $serviceType): JsonResponse
    {
        $serviceType->delete();

        return response()->json(null, Response::HTTP_NO_CONTENT);
    }
}
