<?php

Route::prefix('service-types')->middleware(['jwt', 'access.control'])->group(function () {
    Route::get('/', 'ServiceTypeController@index')->name('service-types.index');
    Route::post('/', 'ServiceTypeController@store')->name('service-types.store');
    Route::put('/{serviceType}', 'ServiceTypeController@update')->name('service-types.update');
    Route::delete('/{serviceType}', 'ServiceTypeController@destroy')->name('service-types.destroy');
});
