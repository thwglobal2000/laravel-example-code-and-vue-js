<?php

namespace App\Modules\Admin\Specializations\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SpecializationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => ['required', 'string', 'max:191'],
            'code' => ['required', 'string', 'max:191'],
            'service_type_id' => ['required', 'exists:service_types,id'],
        ];
    }

    public function messages(): array
    {
        return [
            'service_type_id.required' => 'Service Type is required',
            'service_type_id.exists' => 'Service Type is not exists',
        ];
    }
}
