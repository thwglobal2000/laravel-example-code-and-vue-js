<?php

namespace App\Modules\Admin\Specializations\Controllers;

use App\Http\Controllers\Controller;
use App\Models\ServiceType\ServiceType;
use Illuminate\Http\JsonResponse;

class ServiceTypeController extends Controller
{
    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $serviceTypes = ServiceType::orderBy('service_name')->get();

        return response()->json(['data' => $serviceTypes]);
    }
}
