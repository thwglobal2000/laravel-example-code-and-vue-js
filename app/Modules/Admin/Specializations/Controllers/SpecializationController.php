<?php

namespace App\Modules\Admin\Specializations\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Specialization\Specialization;
use App\Modules\Admin\Specializations\Requests\SpecializationIndexRequest;
use App\Modules\Admin\Specializations\Requests\SpecializationRequest;
use Exception;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class SpecializationController extends Controller
{
    /**
     * @param SpecializationIndexRequest $request
     * @return LengthAwarePaginator
     * @noinspection PhpUndefinedFieldInspection
     */
    public function index(SpecializationIndexRequest $request): LengthAwarePaginator
    {
        $page = $request->input('pagination.page', 1);
        $pageSize = $request->input('pagination.pageSize', 10);
        $term = $request->term;

        return Specialization
            ::with('serviceType:id,service_name')
            ->orderBy('specializations.name')
            ->when(
                $request->filled('sort'),
                fn($query) => $query->orderBy($request->input('sort.column'), $request->input('sort.direction'))
            )
            ->when(
                $term,
                fn($query, $value) => $query->where(
                    fn($q) => $q->where('name', 'like', "%$value%")
                        ->orWhere('code', 'like', "%$value%")
                )
            )
            ->paginate($pageSize, ['*'], 'page', $page);
    }

    /**
     * @param SpecializationRequest $request
     * @return JsonResponse
     */
    public function store(SpecializationRequest $request): JsonResponse
    {
        $specialization = Specialization::create($request->validated());

        return response()->json(['specialization' => $specialization]);
    }

    /**
     * @param Specialization $specialization
     * @param SpecializationRequest $request
     * @return JsonResponse
     */
    public function update(Specialization $specialization, SpecializationRequest $request): JsonResponse
    {
        $specialization->update($request->validated());

        return response()->json(['specialization' => $specialization]);
    }

    /**
     * @param Specialization $specialization
     * @return JsonResponse
     * @throws Exception
     */
    public function destroy(Specialization $specialization): JsonResponse
    {
        $specialization->delete();

        return response()->json(null, Response::HTTP_NO_CONTENT);
    }
}
