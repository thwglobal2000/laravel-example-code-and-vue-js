<?php

Route::prefix('specializations')->middleware(['jwt', 'access.control'])->group(function () {
    Route::get('/', 'SpecializationController@index')->name('specializations.index');
    Route::post('/', 'SpecializationController@store')->name('specializations.store');
    Route::put('/{specialization}', 'SpecializationController@update')->name('specializations.update');
    Route::delete('/{specialization}', 'SpecializationController@destroy')->name('specializations.destroy');
    Route::get('/service-types-list', 'ServiceTypeController@index')->name('specializations.service-types-list');
});
