<?php

namespace App\Contracts;

interface DTOInterface
{
    /**
     * Return JSON
     *
     * @return mixed
     */
    public function __toString();

    /**
     * Restrict dynamic properties
     *
     * @param $name
     * @param $value
     */
    public function __set($name, $value);

    /**
     * Fill DTO via assoc array
     * Array keys must be equal property names
     *
     * @param array $arrayData
     */
    public function fill(array $arrayData);

    /**
     * Make DTO from keyed array
     *
     * @param $arrayData
     */
    public static function make($arrayData);
}
